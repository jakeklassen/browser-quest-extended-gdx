package com.bitknight.bqex;

import com.badlogic.gdx.Files.FileType;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class Main {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "Browser Quest Extended";
		cfg.useGL20 = true;
		//cfg.width = 1280;
		//cfg.height = 800;
		
		//HACK Set the display mode to some supported mode a few positions back from the
		// maximum supported resolution.
		int numDisplayModes = LwjglApplicationConfiguration.getDisplayModes().length;
		cfg.width = LwjglApplicationConfiguration.getDisplayModes()[numDisplayModes - 4].width;
		cfg.height = LwjglApplicationConfiguration.getDisplayModes()[numDisplayModes - 4].height;
		// When the window is active
		cfg.foregroundFPS = 60;
		cfg.fullscreen = false;
		cfg.forceExit = true;
		cfg.vSyncEnabled = true;
		cfg.resizable = false;
		cfg.addIcon("data/img/icon.png", FileType.Internal);
		
		new LwjglApplication(new MainGame(), cfg);
	}
}
