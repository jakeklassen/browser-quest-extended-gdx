package com.bitknight.components;

/**
 * Class representing a health component. HP - Health Points.
 * 
 * @author Jake Klassen
 * 
 */
public class HealthComponent {
  private int maxHP;
  private int currentHP;
  private boolean alive;

  public HealthComponent(int maxHP) {
    this.currentHP = maxHP;
    this.maxHP = maxHP;
    alive = true;
  }

  public boolean isDead() {
    return !alive;
  }
  
  public boolean isAlive() {
    return alive;
  }

  public void setAlive(boolean alive) {
    this.alive = alive;
  }

  public int getMaxHP() {
    return maxHP;
  }

  public void setMaxHealth(int maxHP) {
    this.maxHP = maxHP;
  }

  public int getCurrentHP() {
    return currentHP;
  }

  public void setCurrentHP(int currentHP) {
    this.currentHP = currentHP;
  }

  public void takeDamage(int amount) {
    currentHP -= amount;
    if (currentHP <= 0) {
      currentHP = 0;
      alive = false;
    }
  }

  public void heal(int amount) {
    currentHP += amount;
    if (currentHP >= maxHP)
      currentHP = maxHP;
  }
  
  public float getHealthAsPercentage() {
  	return currentHP / (float)maxHP;
  }
}
