package com.bitknight.bqex.screens;

import java.io.IOException;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics.DisplayMode;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.Texture.TextureWrap;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlReader.Element;
import com.bitknight.bqex.Game;
import com.bitknight.bqex.entities.Player;
import com.bitknight.bqex.entities.Player.Gender;
import com.bitknight.bqex.utils.AnimationKey;
import com.bitknight.bqex.utils.PlayerFileUtils;
import com.bitknight.sound.AudioResourceType;
import com.bitknight.sound.SoundManager;
import com.bitknight.utils.GameState;
import com.bitknight.utils.IScreen;

public class TitleScreen implements IScreen, InputProcessor {
  private TextureRegion region = null;
  
  private int windowY = 0, windowX = 0, playerCount = 3, logoSwap = 5;
  private float height, width, elapsedTime = 0;
	private long beginTime = 0;
  
	private Stage hudStage;
  private Skin skin;
  
  private Image iMenu, iLogo, iLittleWorkshop, iMozilla, iBitknight, iPlayerOne, iPlayerTwo, iPlayerThree, iPlayerNew;
  
  private String playerOneName = "", playerTwoName = "", playerThreeName = "";

  private String playerOneImage = "data/img/native/newchar.png";
  private String playerTwoImage = "data/img/native/emptychar.png";
  private String playerThreeImage = "data/img/native/emptychar.png";
  
  private TextButton playButton, deleteButton, soundButton, fullscreenButton, helpButton;
  private TextButton newGamePlay, newGameCancel, genderButton, newGame;
  private TextButton playerOne, playerTwo, playerThree, displayTF, selectOne, selectTwo, selectThree;
  private TextButton newBack, startButton, aboutButton, backButton;
  
	private Image bg, iNewGame;
	
  private boolean isFullOn = false, isKeyboardOn = false, isStartClosed = false;
  public static boolean isSoundOn = true;

  private Table menu, startMenu, menuAboutOrHelp, backTable, imageTable;
	private Table menuTable, logoTable, errorTable, textDisplayTable;
	private Table websiteTable, labelTable, playersTable, buttonsTable, toggleButtonsTable, selectionTable; 
	private Table newGameMenuTable, newGameTable, newPlayerTable, newImageTable, textFieldTable, newGameBackTable, newGameBehindTable;
	private Table newGameLogo;
	
	private TextField tf;
	private Label errorMsg;
	
	public TitleScreen() {
		width = Gdx.graphics.getWidth();
  	height = Gdx.graphics.getHeight();
  	
		
		PlayerFileUtils.CheckForXMLFile();
		CreateHUD();
		// Play music
    SoundManager.getInstance().play("forest.ogg", true, AudioResourceType.MUSIC);
		soundButton.toggle();
	}
	
	public Stage getStage() {
		return hudStage;
	}

	@Override
  public boolean keyDown(int keycode) {
	  return false;
  }

	@Override
  public boolean keyUp(int keycode) {
	  return false;
  }

	@Override
  public boolean keyTyped(char character) {
	  return false;
  }

	@Override
  public boolean touchDown(int screenX, int screenY, int pointer, int button) {
	  return false;
  }

	@Override
  public boolean touchUp(int screenX, int screenY, int pointer, int button) {
	  return false;
  }

	@Override
  public boolean touchDragged(int screenX, int screenY, int pointer) {
	  return false;
  }

	@Override
  public boolean mouseMoved(int screenX, int screenY) {
	  return false;
  }

	@Override
  public boolean scrolled(int amount) {
	  return false;
  }

	@Override
  public void update(float delta) {
		if( Gdx.input.isKeyPressed(Input.Keys.ESCAPE) ) {
	  	Gdx.app.exit();
	  }
		
		elapsedTime=(System.nanoTime()-beginTime)/1000000000.0f;
	  if(elapsedTime>0.15f){
	    beginTime = System.nanoTime();
	    if(logoSwap==0){
	    	++logoSwap;
	 
	  		logoTable.remove();
				
	  		Texture textureLogo = new Texture(Gdx.files.internal("data/logos/Browserquest-logo1.png"));
	  		textureLogo.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
	  		textureLogo.setWrap(TextureWrap.ClampToEdge, TextureWrap.ClampToEdge);
	  		region = new TextureRegion(textureLogo, 7,0, 282, 64);
	  		
	  		iLogo = new Image(region);
	  		iLogo.setScale(2);
				
	  		logoTable = new Table();
	  		hudStage.addActor(logoTable);
	      logoTable.setSize(width, height);
	      logoTable.setPosition(-iLogo.getWidth()/2, 0);
		    
	      logoTable.add(iLogo).bottom().right().padBottom(300);
	      logoTable.toBack();
	      menuTable.toBack();
	      bg.toBack();
	    }
	    else if(logoSwap==1){
	    	++logoSwap;
	    	logoTable.remove();
				
	  		Texture textureLogo = new Texture(Gdx.files.internal("data/logos/Browserquest-logo2.png"));
	  		textureLogo.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
	  		textureLogo.setWrap(TextureWrap.ClampToEdge, TextureWrap.ClampToEdge);
	  		region = new TextureRegion(textureLogo, 7,0, 282, 64);
	  		
	  		iLogo = new Image(region);
	  		iLogo.setScale(2);
				
	  		logoTable = new Table();
	  		hudStage.addActor(logoTable);
	      logoTable.setSize(width, height);
	      logoTable.setPosition(-iLogo.getWidth()/2, 0);
		    
	      logoTable.add(iLogo).bottom().right().padBottom(300);
	      logoTable.toBack();
	      menuTable.toBack();
	      bg.toBack();
	    }
	    else if(logoSwap==2){
	    	++logoSwap;
	    	logoTable.remove();
				
	  		Texture textureLogo = new Texture(Gdx.files.internal("data/logos/Browserquest-logo3.png"));
	  		textureLogo.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
	  		textureLogo.setWrap(TextureWrap.ClampToEdge, TextureWrap.ClampToEdge);
	  		region = new TextureRegion(textureLogo, 7,0, 282, 64);
	  		
	  		iLogo = new Image(region);
	  		iLogo.setScale(2);
				
	  		logoTable = new Table();
	  		hudStage.addActor(logoTable);
	      logoTable.setSize(width, height);
	      logoTable.setPosition(-iLogo.getWidth()/2, 0);
		    
	      logoTable.add(iLogo).bottom().right().padBottom(300);
	      logoTable.toBack();
	      menuTable.toBack();
	      bg.toBack();
	    }
	    else if(logoSwap==3){
	    	++logoSwap;
	    	logoTable.remove();
				
	  		Texture textureLogo = new Texture(Gdx.files.internal("data/logos/Browserquest-logo4.png"));
	  		textureLogo.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
	  		textureLogo.setWrap(TextureWrap.ClampToEdge, TextureWrap.ClampToEdge);
	  		region = new TextureRegion(textureLogo, 7,0, 282, 64);
	  		
	  		iLogo = new Image(region);
	  		iLogo.setScale(2);
				
	  		logoTable = new Table();
	  		hudStage.addActor(logoTable);
	      logoTable.setSize(width, height);
	      logoTable.setPosition(-iLogo.getWidth()/2, 0);
		    
	      logoTable.add(iLogo).bottom().right().padBottom(300);
	      logoTable.toBack();
	      menuTable.toBack();
	      bg.toBack();
	    }
	    else if(logoSwap==4){
	    	++logoSwap;
	    	logoTable.remove();
				
	  		Texture textureLogo = new Texture(Gdx.files.internal("data/logos/Browserquest-logo5.png"));
	  		textureLogo.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
	  		textureLogo.setWrap(TextureWrap.ClampToEdge, TextureWrap.ClampToEdge);
	  		region = new TextureRegion(textureLogo, 7,0, 282, 64);
	  		
	  		iLogo = new Image(region);
	  		iLogo.setScale(2);
				
	  		logoTable = new Table();
	  		hudStage.addActor(logoTable);
	      logoTable.setSize(width, height);
	      logoTable.setPosition(-iLogo.getWidth()/2, 0);
		    
	      logoTable.add(iLogo).bottom().right().padBottom(300);
	      logoTable.toBack();
	      menuTable.toBack();
	      bg.toBack();
	    }
	    else if(logoSwap==5){
	    	logoSwap=0;
	    	logoTable.remove();
				
	  		Texture textureLogo = new Texture(Gdx.files.internal("data/logos/Browserquest-logo6.png"));
	  		textureLogo.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
	  		textureLogo.setWrap(TextureWrap.ClampToEdge, TextureWrap.ClampToEdge);
	  		region = new TextureRegion(textureLogo, 7,0, 282, 64);
	  		
	  		iLogo = new Image(region);
	  		iLogo.setScale(2);
				
	  		logoTable = new Table();
	  		hudStage.addActor(logoTable);
	      logoTable.setSize(width, height);
	      logoTable.setPosition(-iLogo.getWidth()/2, 0);
		    
		    logoTable.add(iLogo).bottom().right().padBottom(300);
		    logoTable.toBack();
	      menuTable.toBack();
	      bg.toBack();
	    }
	  }
	  
	  displayTF.setText(tf.getText());
  }

	@Override
  public void render() {
		Gdx.gl.glClearColor(0, 0, 0, 1);
	  Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
	  
	  // Draw Hud
	  hudStage.draw();
	  update(1);
  }

	@Override
  public void resize(int width, int height) {
		this.width = width;
		this.height = height;

	  // Resize the Hud
	  hudStage.setViewport(width, height, true);
	  repositionUI();
  }

	@Override
  public void show() {
  }

	@Override
  public void hide() {
  }

	@Override
  public void pause() {
  }

	@Override
  public void resume() {
  }

	@Override
  public void dispose() {
		hudStage.dispose();
		skin.dispose();
  }
	
	
	public void CreateHUD() {
		isStartClosed=false;
		try {
			XmlReader reader = new XmlReader();
			Element root;
		
			root = reader.parse(Gdx.files.local("player.xml"));
			Array<Element> players = root.getChildrenByName("player");
			playerCount = root.getChildCount();
			for (Element child : players){
				int id = Integer.parseInt(child.getAttribute("id"));
				if(id == 1) {
					playerOneName = child.getAttribute("name");
					playerOneImage = child.getAttribute("image");
				}
				else if(id == 2) {
					playerTwoName = child.getAttribute("name");
					playerTwoImage = child.getAttribute("image");
				}
				else {
					playerThreeName = child.getAttribute("name");
					playerThreeImage = child.getAttribute("image");
				}
			}
		} 
		catch (IOException e) {}
		
		if(playerCount == 1)
		{
			playerTwoName = "New Game";
			playerTwoImage = "data/img/native/newchar.png";
			
			playerThreeName = "";
			playerThreeImage = "data/img/native/emptychar.png";
		}
		else if(playerCount == 2)
		{
			playerThreeName = "New Game";
			playerThreeImage = "data/img/native/newchar.png";
		}
		else if(playerCount == 0)
		{
			playerOneName = "New Game";
			playerOneImage = "data/img/native/newchar.png";
			
			playerTwoName = "";
			playerTwoImage = "data/img/native/emptychar.png";
			
			playerThreeName = "";
			playerThreeImage = "data/img/native/emptychar.png";
		}
		
		// Create the skin
		skin = new Skin();
    skin.addRegions(new TextureAtlas(Gdx.files.internal("data/ui/titlescreen/uiskins.atlas")));
    skin.load(Gdx.files.internal("data/ui/titlescreen/uiskin.json"));
		
		// Create the stage
		hudStage = new Stage();
		Gdx.input.setInputProcessor(hudStage);
		
		positionUI();
	}
  
  public void positionUI () {
  	//Set Up the tables
  	Texture textureBG = new Texture(Gdx.files.internal("data/img/wood.png"));
  	textureBG.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
  	textureBG.setWrap(TextureWrap.Repeat, TextureWrap.Repeat);
  	region = new TextureRegion(textureBG, 0,0, Gdx.graphics.getWidth(), Gdx.graphics.getWidth());
  	bg = new Image(region);
  	hudStage.addActor(bg);


  	//Set Up the tables
  	Texture textureLogo = new Texture(Gdx.files.internal("data/logos/Browserquest-logo.png"));
  	textureLogo.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
  	textureLogo.setWrap(TextureWrap.ClampToEdge, TextureWrap.ClampToEdge);
  	region = new TextureRegion(textureLogo, 7,0, 282, 64);
  	iLogo = new Image(region);
  	iLogo.setScale(Gdx.graphics.getWidth()/(Gdx.graphics.getHeight()+0.0f));

  	logoTable = new Table();
  	hudStage.addActor(logoTable);
  	logoTable.setSize(width, height);
  	logoTable.setPosition(-iLogo.getWidth()/2, 0);

  	Texture textureMenu = new Texture(Gdx.files.internal("data/img/menu.png"));
  	textureMenu.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
  	textureMenu.setWrap(TextureWrap.ClampToEdge, TextureWrap.ClampToEdge);
  	region = new TextureRegion(textureMenu, 5,0, 497, 256);
  	iMenu = new Image(region);
  	iMenu.setScale(2);

  	menuTable = new Table();
  	hudStage.addActor(menuTable);
  	menuTable.setSize(width, height);
  	menuTable.setPosition(-iMenu.getWidth()/2, 0);

  	selectionTable = new Table();
  	hudStage.addActor(selectionTable);
  	selectionTable.setSize(width, height);
  	selectionTable.setPosition(0, 55);

  	websiteTable = new Table();
  	hudStage.addActor(websiteTable);
  	websiteTable.setSize(width, height);
  	websiteTable.setPosition(0, 0);

  	textDisplayTable = new Table();
  	textDisplayTable.setSize(width, height);
  	textDisplayTable.setPosition(0, 0);

  	errorTable = new Table();
  	hudStage.addActor(errorTable);
  	errorTable.setSize(width, height);
  	errorTable.setPosition(0, 0);

  	labelTable = new Table();
  	hudStage.addActor(labelTable);
  	labelTable.setSize(width, height);
  	labelTable.setPosition(0, 0);

  	playersTable = new Table();
  	hudStage.addActor(playersTable);
  	playersTable.setSize(width, height);
  	playersTable.setPosition(0, 0);

  	buttonsTable = new Table();
  	hudStage.addActor(buttonsTable);
  	buttonsTable.setSize(width, height);
  	buttonsTable.setPosition(0, 0);

  	toggleButtonsTable = new Table();
  	hudStage.addActor(toggleButtonsTable);
  	toggleButtonsTable.setSize(width, height);
  	toggleButtonsTable.setPosition(0, 0);

  	newGameTable = new Table();
  	newGameTable.setSize(width, height);
  	newGameTable.setPosition(0, 0);

  	newGameBackTable = new Table();
  	newGameBackTable.setSize(width, height);
  	newGameBackTable.setPosition(0, 0);

  	newGameMenuTable = new Table();
  	newGameMenuTable.setSize(width, height);
  	newGameMenuTable.setPosition(0, 0);

  	newGameBehindTable = new Table();
  	newGameBehindTable.setSize(width, height);
  	newGameBehindTable.setPosition(0, 0);

  	newPlayerTable = new Table();
  	newPlayerTable.setSize(width, height);
  	newPlayerTable.setPosition(0, 0);

  	newImageTable = new Table();
  	newImageTable.setSize(width, height);
  	newImageTable.setPosition(0, 0);

  	Texture textureNG = new Texture(Gdx.files.internal("data/img/newgame.png"));
  	textureNG.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
  	textureNG.setWrap(TextureWrap.ClampToEdge, TextureWrap.ClampToEdge);
  	region = new TextureRegion(textureNG, 0,0, 80, 21);
  	iNewGame = new Image(region);
  	iNewGame.setTouchable(Touchable.disabled);
  	iNewGame.setScale(2);

  	newGameLogo = new Table();
  	newGameLogo.setSize(width, height);
  	newGameLogo.setPosition(-iNewGame.getWidth()/2, 0);

  	textFieldTable = new Table();
  	textFieldTable.setSize(width, height);
  	textFieldTable.setPosition(0, 0);

  	//Create the UI Items
  	Texture textureLittleWorkshop = new Texture(Gdx.files.internal("data/logos/littleworkshop.png"));
  	textureLittleWorkshop.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
  	textureLittleWorkshop.setWrap(TextureWrap.ClampToEdge, TextureWrap.ClampToEdge);
  	region = new TextureRegion(textureLittleWorkshop, 0,0, 256, 88);
  	iLittleWorkshop = new Image(region);

  	Texture textureMozilla = new Texture(Gdx.files.internal("data/logos/mozillapressed.png"));
  	textureMozilla.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
  	textureMozilla.setWrap(TextureWrap.ClampToEdge, TextureWrap.ClampToEdge);
  	region = new TextureRegion(textureMozilla, 0,0, 256, 88);
  	iMozilla = new Image(region);

  	Texture textureBitknight = new Texture(Gdx.files.internal("data/logos/bitknightpressed.png"));
  	textureBitknight.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
  	textureBitknight.setWrap(TextureWrap.ClampToEdge, TextureWrap.ClampToEdge);
  	region = new TextureRegion(textureBitknight, 0,0, 256, 88);
  	iBitknight = new Image(region);

  	Texture texture = new Texture(Gdx.files.internal(playerOneImage));
  	texture.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
  	texture.setWrap(TextureWrap.ClampToEdge, TextureWrap.ClampToEdge);
  	region = new TextureRegion(texture, 40,256, 48, 128);
  	iPlayerOne = new Image(region);
  	iPlayerOne.setTouchable(Touchable.disabled);

  	texture = new Texture(Gdx.files.internal(playerTwoImage));
  	texture.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
  	texture.setWrap(TextureWrap.ClampToEdge, TextureWrap.ClampToEdge);
  	region = new TextureRegion(texture, 40,256, 48, 128);
  	iPlayerTwo = new Image(region);
  	iPlayerTwo.setTouchable(Touchable.disabled);

  	texture = new Texture(Gdx.files.internal(playerThreeImage));
  	texture.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
  	texture.setWrap(TextureWrap.ClampToEdge, TextureWrap.ClampToEdge);
  	region = new TextureRegion(texture, 40,256, 48, 128);
  	iPlayerThree = new Image(region);
  	iPlayerThree.setTouchable(Touchable.disabled);

  	iPlayerOne.setScale(3);
  	iPlayerTwo.setScale(3);
  	iPlayerThree.setScale(3);

  	displayTF = new TextButton("", skin, "select");
  	displayTF.scale(2);
  	displayTF.setTouchable(Touchable.disabled);

  	errorMsg = new Label("", skin);
  	errorMsg.setColor(Color.RED);

  	playerOne = new TextButton(playerOneName, skin, "menu");
  	playerOne.setTouchable(Touchable.disabled);

  	playerTwo = new TextButton(playerTwoName, skin, "menu");
  	playerTwo.setTouchable(Touchable.disabled);

  	playerThree = new TextButton(playerThreeName, skin, "menu");
  	playerThree.setTouchable(Touchable.disabled);

  	selectOne = new TextButton("", skin, "select");

  	if(playerCount > 0)
  		selectTwo = new TextButton("", skin, "select");
  	else
  		selectTwo = new TextButton("", skin, "menu");

  	if(playerCount > 1)
  		selectThree = new TextButton("", skin, "select");
  	else
  		selectThree = new TextButton("", skin, "menu");

  	if(playerCount==0) {
  		selectTwo.setTouchable(Touchable.disabled);
  		selectThree.setTouchable(Touchable.disabled);
  	}
  	else if(playerCount==1) {
  		selectThree.setTouchable(Touchable.disabled);
  	}

  	playButton = new TextButton("Play", skin, "default");
  	deleteButton = new TextButton("Delete", skin, "default");

  	soundButton = new TextButton("Sound", skin, "toggle");
  	fullscreenButton = new TextButton("Fullscreen", skin, "toggle");
  	helpButton = new TextButton("Help", skin, "default");

  	newGame = new TextButton("", skin, "select");
  	newGame.setTouchable(Touchable.disabled);

  	TextButton newGameBehind = new TextButton("", skin, "menu");

  	TextButton selectNew = new TextButton("", skin, "select");
  	selectNew.toggle();
  	selectNew.setTouchable(Touchable.disabled);

  	texture = new Texture(Gdx.files.internal("data/img/native/clotharmor.png"));
  	texture.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
  	texture.setWrap(TextureWrap.ClampToEdge, TextureWrap.ClampToEdge);
  	region = new TextureRegion(texture, 40,256, 54, 128);
  	iPlayerNew = new Image(region);
  	iPlayerNew.setTouchable(Touchable.disabled);
  	iPlayerNew.setScale(3);

  	tf = new TextField("", skin);
  	tf.setMessageText("Enter Name...");

  	newGamePlay = new TextButton("Play", skin, "default");
  	newGameCancel = new TextButton("Cancel", skin, "default");
  	genderButton = new TextButton("Lord", skin, "gender");

  	newBack = new TextButton("", skin, "menu");

  	//Add UI Items to tables
  	menuTable.add(iMenu).bottom().right().padTop(200);
  	logoTable.add(iLogo).bottom().right().padTop(100);

  	if(Gdx.graphics.getHeight()==480) {
  		playersTable.add(iPlayerOne).bottom().padRight(62).padTop(360);
  		playersTable.add(iPlayerTwo).bottom().padLeft(62).padRight(62).padTop(360);
  		playersTable.add(iPlayerThree).bottom().padLeft(62).padTop(360);
  	}
  	else {
  		playersTable.add(iPlayerOne).bottom().padRight(62).padTop(400);
  		playersTable.add(iPlayerTwo).bottom().padRight(62).padLeft(62).padTop(400);
  		playersTable.add(iPlayerThree).bottom().padLeft(62).padTop(400);
  	}

  	labelTable.add(playerOne).bottom().right().width(150).height(Gdx.graphics.getHeight()/20).padRight(20).padBottom(50);
  	labelTable.add(playerTwo).bottom().right().width(150).height(Gdx.graphics.getHeight()/20).padRight(15).padBottom(50);
  	labelTable.add(playerThree).bottom().right().width(150).height(Gdx.graphics.getHeight()/20).padLeft(10).padBottom(50);

  	selectionTable.add(selectOne).bottom().right().width(150).height(140).padRight(15);
  	selectionTable.add(selectTwo).bottom().right().width(150).height(140).padLeft(10).padRight(15);
  	selectionTable.add(selectThree).bottom().right().width(150).height(140).padLeft(10);

  	if(Gdx.graphics.getHeight()!=480) {
  		websiteTable.add(iLittleWorkshop).bottom().padRight(60).padTop(500);
  		websiteTable.add(iMozilla).bottom().padLeft(60).padRight(60).padTop(500);
  		websiteTable.add(iBitknight).bottom().padLeft(60).padTop(500);
  	}

  	buttonsTable.add(playButton).bottom().right().width(200).height(40).padRight(20).padTop(150);
  	buttonsTable.add(deleteButton).bottom().right().width(200).height(40).padLeft(20).padTop(150);

  	if(Gdx.app.getType().equals(ApplicationType.Desktop)){
  		toggleButtonsTable.add(soundButton).bottom().right().width(140).height(40).padLeft(20).padRight(20).padTop(250);
  		toggleButtonsTable.add(fullscreenButton).bottom().right().width(140).height(40).padLeft(20).padRight(20).padTop(250);
  		toggleButtonsTable.add(helpButton).bottom().right().width(140).height(40).padLeft(20).padRight(20).padTop(250);
  	}
  	else {
  		textDisplayTable.add(displayTF).bottom().right().height(100).width(Gdx.graphics.getWidth()-50);
  		toggleButtonsTable.add(soundButton).bottom().right().width(200).height(40).padLeft(20).padRight(20).padTop(250);
  		toggleButtonsTable.add(helpButton).bottom().right().width(200).height(40).padLeft(20).padRight(20).padTop(250);
  	}

  	newGameBackTable.add(newBack).bottom().right().width(Gdx.graphics.getWidth()).height(Gdx.graphics.getHeight()).padBottom(10);
  	newGameMenuTable.add(newGame).bottom().right().width(300).height(300).padBottom(10);
  	newGameBehindTable.add(newGameBehind).bottom().right().width(300).height(300).padBottom(10);
  	newPlayerTable.add(selectNew).bottom().right().width(150).height(140).padBottom(60);
  	if(Gdx.graphics.getHeight()==480)
  		newImageTable.add(iPlayerNew).bottom().right().padTop(375);
  	else
  		newImageTable.add(iPlayerNew).bottom().right().padTop(470);

  	newGameTable.add(newGamePlay).bottom().right().width(125).height(35).padRight(10).padTop(240);
  	newGameTable.add(newGameCancel).bottom().right().width(125).height(35).padLeft(10);

  	textFieldTable.add(tf).bottom().right().width(180).padTop(150).padRight(5);
  	textFieldTable.add(genderButton).bottom().right().width(75).height(32).padTop(150).padLeft(5);
  	errorTable.add(errorMsg).bottom().right().padBottom(450);

  	newGameLogo.add(iNewGame).bottom().right().padBottom(225);

  	//Button Click Event Listeners
  	iLittleWorkshop.addListener(new ClickListener() {
  		@Override
  		public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
  			iLittleWorkshop.remove();
  			iMozilla.remove();
  			iBitknight.remove();

  			Texture textureLW = new Texture(Gdx.files.internal("data/logos/littleworkshoppressed.png"));
  			textureLW.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
  			textureLW.setWrap(TextureWrap.ClampToEdge, TextureWrap.ClampToEdge);
  			region = new TextureRegion(textureLW, 0,0, 256, 88);
  			iLittleWorkshop = new Image(region);

  			websiteTable = new Table();
  			hudStage.addActor(websiteTable);
  			websiteTable.setSize(width, height);
  			websiteTable.setPosition(0, 0);

  			websiteTable.add(iLittleWorkshop).bottom().padRight(60).padTop(500);
  			websiteTable.add(iMozilla).bottom().padLeft(60).padRight(60).padTop(500);
  			websiteTable.add(iBitknight).bottom().padLeft(60).padTop(500);
  			return super.touchDown(event, x, y, pointer, button);
  		}

  		@Override
  		public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
  			websiteTable.remove();

  			Texture textureLW = new Texture(Gdx.files.internal("data/logos/littleworkshop.png"));
  			textureLW.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
  			textureLW.setWrap(TextureWrap.ClampToEdge, TextureWrap.ClampToEdge);
  			region = new TextureRegion(textureLW, 0,0, 256, 88);
  			iLittleWorkshop = new Image(region);

  			websiteTable = new Table();
  			hudStage.addActor(websiteTable);
  			websiteTable.setSize(width, height);
  			websiteTable.setPosition(0, 0);

  			websiteTable.add(iLittleWorkshop).bottom().padRight(60).padTop(500);
  			websiteTable.add(iMozilla).bottom().padLeft(60).padRight(60).padTop(500);
  			websiteTable.add(iBitknight).bottom().padLeft(60).padTop(500);
  			Gdx.net.openURI("http://www.littleworkshop.fr/browserquest.html");
  			super.touchUp(event, x, y, pointer, button);
  		}
  	});

  	iMozilla.addListener(new ClickListener() {

  		@Override
  		public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
  			websiteTable.remove();
  			Texture textureMozilla = new Texture(Gdx.files.internal("data/logos/mozilla.png"));
  			textureMozilla.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
  			textureMozilla.setWrap(TextureWrap.ClampToEdge, TextureWrap.ClampToEdge);
  			region = new TextureRegion(textureMozilla, 0,0, 256, 88);
  			iMozilla = new Image(region);

  			websiteTable = new Table();
  			hudStage.addActor(websiteTable);
  			websiteTable.setSize(width, height);
  			websiteTable.setPosition(0, 0);

  			websiteTable.add(iLittleWorkshop).bottom().padRight(60).padTop(500);
  			websiteTable.add(iMozilla).bottom().padLeft(60).padRight(60).padTop(500);
  			websiteTable.add(iBitknight).bottom().padLeft(60).padTop(500);

  			return super.touchDown(event, x, y, pointer, button);
  		}

  		@Override
  		public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
  			websiteTable.remove();
  			Texture textureMozilla = new Texture(Gdx.files.internal("data/logos/mozillapressed.png"));
  			textureMozilla.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
  			textureMozilla.setWrap(TextureWrap.ClampToEdge, TextureWrap.ClampToEdge);
  			region = new TextureRegion(textureMozilla, 0,0, 256, 88);
  			iMozilla = new Image(region);

  			websiteTable = new Table();
  			hudStage.addActor(websiteTable);
  			websiteTable.setSize(width, height);
  			websiteTable.setPosition(0, 0);

  			websiteTable.add(iLittleWorkshop).bottom().padRight(60).padTop(500);
  			websiteTable.add(iMozilla).bottom().padLeft(60).padRight(60).padTop(500);
  			websiteTable.add(iBitknight).bottom().padLeft(60).padTop(500);
  			Gdx.net.openURI("http://browserquest.mozilla.org/");
  			super.touchUp(event, x, y, pointer, button);
  		}

  	});

  	iBitknight.addListener(new ClickListener() {
  		@Override
  		public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
  			websiteTable.remove();

  			Texture textureBitknight = new Texture(Gdx.files.internal("data/logos/bitknight.png"));
  			textureBitknight.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
  			textureBitknight.setWrap(TextureWrap.ClampToEdge, TextureWrap.ClampToEdge);
  			region = new TextureRegion(textureBitknight, 0,0, 256, 88);
  			iBitknight = new Image(region);

  			websiteTable = new Table();
  			hudStage.addActor(websiteTable);
  			websiteTable.setSize(width, height);
  			websiteTable.setPosition(0, 0);

  			websiteTable.add(iLittleWorkshop).bottom().padRight(60).padTop(500);
  			websiteTable.add(iMozilla).bottom().padLeft(60).padRight(60).padTop(500);
  			websiteTable.add(iBitknight).bottom().padLeft(60).padTop(500);
  			return super.touchDown(event, x, y, pointer, button);
  		}

  		@Override
  		public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
  			websiteTable.remove();

  			Texture textureBitknight = new Texture(Gdx.files.internal("data/logos/bitknightpressed.png"));
  			textureBitknight.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
  			textureBitknight.setWrap(TextureWrap.ClampToEdge, TextureWrap.ClampToEdge);
  			region = new TextureRegion(textureBitknight, 0,0, 256, 88);
  			iBitknight = new Image(region);

  			websiteTable = new Table();
  			hudStage.addActor(websiteTable);
  			websiteTable.setSize(width, height);
  			websiteTable.setPosition(0, 0);

  			websiteTable.add(iLittleWorkshop).bottom().padRight(60).padTop(500);
  			websiteTable.add(iMozilla).bottom().padLeft(60).padRight(60).padTop(500);
  			websiteTable.add(iBitknight).bottom().padLeft(60).padTop(500);
  			Gdx.net.openURI("http://www.bitknight.com/");
  			super.touchUp(event, x, y, pointer, button);
  		}	
  	});

  	fullscreenButton.addListener(new ClickListener() {
  		@Override
  		public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
  			if(isFullOn == false) {
  				isFullOn = true;
  				windowX = Gdx.graphics.getWidth();
  				windowY = Gdx.graphics.getHeight();
  				DisplayMode desktopDisplayMode = Gdx.graphics.getDesktopDisplayMode();
  				Gdx.graphics.setDisplayMode(desktopDisplayMode.width, desktopDisplayMode.height, true);
  			}
  			else {
  				isFullOn = false;
  				Gdx.graphics.setDisplayMode(windowX, windowY, false);
  			}
  			super.touchUp(event, x, y, pointer, button);
  		}
  	});

  	helpButton.addListener(new ClickListener() {
  		@Override
  		public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
  			menuAboutOrHelp = new Table();
  			hudStage.addActor(menuAboutOrHelp);
  			menuAboutOrHelp.setSize(width, height);
  			menuAboutOrHelp.setPosition(0, 0);

  			backTable = new Table();
  			hudStage.addActor(backTable);
  			backTable.setSize(width, height);
  			backTable.setPosition(0, 0);

  			Texture textureHelp = new Texture(Gdx.files.internal("data/img/help.png"));
  			textureHelp.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
  			textureHelp.setWrap(TextureWrap.Repeat, TextureWrap.Repeat);
  			region = new TextureRegion(textureHelp, 0,0, 512, 282);
  			Image iHelp = new Image(region);

  			backButton = new TextButton("Back", skin, "default");
  			backButton.addListener(new ClickListener() {

  				@Override
  				public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
  					backTable.remove();
  					menuAboutOrHelp.remove();
  					super.touchUp(event, x, y, pointer, button);
  				}

  			});
  			menuAboutOrHelp.add(iHelp).bottom().right().padTop(7);
  			backTable.add(backButton).bottom().right().height(40).width(200).padTop(275);
  			super.touchUp(event, x, y, pointer, button);
  		}
  	});

  	soundButton.addListener(new ClickListener() {
  		@Override
  		public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
  			errorMsg.setText("");
  			if(isSoundOn == false) {
  				isSoundOn = true;
  				// Play music
  				SoundManager.getInstance().play("forest.ogg", true, AudioResourceType.MUSIC);
  			}
  			else {
  				isSoundOn = false;
  				//stop music
  				SoundManager.getInstance().pauseMusic("forest.ogg");
  			}
  			super.touchUp(event, x, y, pointer, button);
  		}
  	});

  	selectOne.addListener(new ClickListener() {
  		@Override
  		public boolean touchDown(InputEvent event, float x, float y, int pointer,int button) {
  			errorMsg.setText("");
  			if(selectOne.isChecked()) {
  				selectOne.toggle();
  			}
  			if(selectTwo.isChecked()) {
  				selectTwo.toggle();
  			}
  			if(selectThree.isChecked()) {
  				selectThree.toggle();
  			}
  			if(playerCount==0) {
  				hudStage.addActor(newGameBackTable);
  				hudStage.addActor(newGameBehindTable);
  				hudStage.addActor(newGameMenuTable);
  				hudStage.addActor(newPlayerTable);
  				hudStage.addActor(newImageTable);
  				hudStage.addActor(textFieldTable);
  				hudStage.addActor(newGameTable);
  				hudStage.addActor(newGameLogo);
  			}
  			return super.touchDown(event, x, y, pointer, button);
  		}
  	});

  	selectTwo.addListener(new ClickListener() {
  		@Override
  		public boolean touchDown(InputEvent event, float x, float y, int pointer,int button) {
  			if(playerCount > 0) {
  				errorMsg.setText("");

  				if(selectTwo.isChecked()) {
  					selectTwo.toggle();
  				}
  				if(selectOne.isChecked()) {
  					selectOne.toggle();
  				}
  				if(selectThree.isChecked()) {
  					selectThree.toggle();
  				}
  				if(playerCount==1) {
  					hudStage.addActor(newGameBackTable);
  					hudStage.addActor(newGameBehindTable);
  					hudStage.addActor(newGameMenuTable);
  					hudStage.addActor(newPlayerTable);
  					hudStage.addActor(newImageTable);
  					hudStage.addActor(textFieldTable);
  					hudStage.addActor(newGameTable);
  					hudStage.addActor(newGameLogo);
  				}
  			}
  			else
  			{
  				selectTwo.toggle();
  				if(selectOne.isChecked()==false) {
  					selectOne.toggle();
  				}
  			}
  			return super.touchDown(event, x, y, pointer, button);
  		}
  	});

  	selectThree.addListener(new ClickListener() {
  		@Override
  		public boolean touchDown(InputEvent event, float x, float y, int pointer,int button) {
  			if(playerCount > 1) {
  				errorMsg.setText("");

  				if(selectThree.isChecked()) {
  					selectThree.toggle();
  				}
  				if(selectOne.isChecked()) {
  					selectOne.toggle();
  				}
  				if(selectTwo.isChecked()) {
  					selectTwo.toggle();
  				}
  				if(playerCount==2) {
  					hudStage.addActor(newGameBackTable);
  					hudStage.addActor(newGameBehindTable);
  					hudStage.addActor(newGameMenuTable);
  					hudStage.addActor(newPlayerTable);
  					hudStage.addActor(newImageTable);
  					hudStage.addActor(textFieldTable);
  					hudStage.addActor(newGameTable);
  					hudStage.addActor(newGameLogo);
  				}
  			}
  			else
  			{
  				selectThree.toggle();
  			}

  			return super.touchDown(event, x, y, pointer, button);
  		}

  	});

  	newGameCancel.addListener(new ClickListener() {
  		@Override
  		public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
  			errorMsg.setText("");
  			newGameBehindTable.remove();
  			newGameBackTable.remove();
  			newGameMenuTable.remove();
  			newPlayerTable.remove();
  			newImageTable.remove();
  			textFieldTable.remove();
  			newGameTable.remove();
  			newGameLogo.remove();
  			super.touchUp(event, x, y, pointer, button);
  		}
  	});

  	genderButton.addListener(new ClickListener() {
  		@Override
  		public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
  			if(genderButton.getText().toString().equals("Lord")) {
  				genderButton.setText("Lady");

  				newImageTable.remove();
  				Texture texture = new Texture(Gdx.files.internal("data/img/native/clotharmor_female.png"));
  				texture.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
  				texture.setWrap(TextureWrap.ClampToEdge, TextureWrap.ClampToEdge);
  				region = new TextureRegion(texture, 40,256, 54, 128);
  				iPlayerNew = new Image(region);
  				iPlayerNew.setTouchable(Touchable.disabled);
  				iPlayerNew.setScale(3);

  				newImageTable = new Table();
  				hudStage.addActor(newImageTable);
  				newImageTable.setSize(width, height);
  				newImageTable.setPosition(0, 0);

  				if(Gdx.graphics.getHeight()==480)
  					newImageTable.add(iPlayerNew).bottom().right().padTop(375);
  				else
  					newImageTable.add(iPlayerNew).bottom().right().padTop(470);
  			}
  			else {
  				genderButton.setText("Lord");

  				newImageTable.remove();
  				Texture texture = new Texture(Gdx.files.internal("data/img/native/clotharmor.png"));
  				texture.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
  				texture.setWrap(TextureWrap.ClampToEdge, TextureWrap.ClampToEdge);
  				region = new TextureRegion(texture, 40,256, 54, 128);
  				iPlayerNew = new Image(region);
  				iPlayerNew.setTouchable(Touchable.disabled);
  				iPlayerNew.setScale(3);

  				newImageTable = new Table();
  				hudStage.addActor(newImageTable);
  				newImageTable.setSize(width, height);
  				newImageTable.setPosition(0, 0);


  				if(Gdx.graphics.getHeight()==480)
  					newImageTable.add(iPlayerNew).bottom().right().padTop(375);
  				else
  					newImageTable.add(iPlayerNew).bottom().right().padTop(470);
  			}
  			super.touchUp(event, x, y, pointer, button);
  		}
  	});

  	tf.addListener(new ClickListener() {
  		@Override
  		public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
  			if(Gdx.app.getType().equals(ApplicationType.Desktop)==false){
  				isKeyboardOn = true;
  				textDisplayTable.remove();
  				textDisplayTable = new Table();
  				hudStage.addActor(textDisplayTable);
  				textDisplayTable.setSize(width, height);
  				textDisplayTable.setPosition(0, (Gdx.graphics.getHeight()/2)-50);
  				textDisplayTable.add(displayTF).bottom().right().height(100).width(Gdx.graphics.getWidth());
  			}
  			return super.touchDown(event, x, y, pointer, button);
  		}

  	});

  	newBack.addListener(new ClickListener() {
  		@Override
  		public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
  			if(isKeyboardOn==true && Gdx.app.getType().equals(ApplicationType.Desktop)==false)
  			{
  				isKeyboardOn = false;
  				Gdx.input.setOnscreenKeyboardVisible(false);
  				textDisplayTable.remove();
  			}
  			else
  			{
  				errorMsg.setText("");
  				newGameBehindTable.remove();
  				newGameBackTable.remove();
  				newGameMenuTable.remove();
  				newPlayerTable.remove();
  				newImageTable.remove();
  				textFieldTable.remove();
  				newGameTable.remove();
  				newGameLogo.remove();
  			}

  			super.touchUp(event, x, y, pointer, button);
  		}
  	});

  	playButton.addListener(new ClickListener() {
  		@Override
  		public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
  			errorMsg.setText("");
  			if(playerCount==0){
  				hudStage.addActor(newGameBackTable);
  				hudStage.addActor(newGameBehindTable);
  				hudStage.addActor(newGameMenuTable);
  				hudStage.addActor(newPlayerTable);
  				hudStage.addActor(newImageTable);
  				hudStage.addActor(textFieldTable);
  				hudStage.addActor(newGameTable);
  				hudStage.addActor(newGameLogo);
  			}
  			else if(playerCount==1) {
  				if(selectTwo.isChecked()) {
  					hudStage.addActor(newGameBackTable);
  					hudStage.addActor(newGameBehindTable);
  					hudStage.addActor(newGameMenuTable);
  					hudStage.addActor(newPlayerTable);
  					hudStage.addActor(newImageTable);
  					hudStage.addActor(textFieldTable);
  					hudStage.addActor(newGameTable);
  					hudStage.addActor(newGameLogo);
  				}
  				else {
  					// Start the game
  					CreateHUD();
  					loadPlayerFromFile(1);
  				}
  			}
  			else if(playerCount==2) {
  				if(selectThree.isChecked()) {
  					hudStage.addActor(newGameBackTable);
  					hudStage.addActor(newGameBehindTable);
  					hudStage.addActor(newGameMenuTable);
  					hudStage.addActor(newPlayerTable);
  					hudStage.addActor(newImageTable);
  					hudStage.addActor(textFieldTable);
  					hudStage.addActor(newGameTable);
  					hudStage.addActor(newGameLogo);
  				}
  				else {
  					// Start the game
  					if(selectOne.isChecked()) {
  						CreateHUD();
  						loadPlayerFromFile(1);
  					}
  					else
  					{
  						CreateHUD();
  						loadPlayerFromFile(2);
  					}

  				}
  			}
  			else {
  				// Start the game
  				if(selectOne.isChecked()) {
  					CreateHUD();
  					loadPlayerFromFile(1);
  				}
  				else if(selectTwo.isChecked()) {
  					CreateHUD();
  					loadPlayerFromFile(2);
  				}
  				else
  				{
  					CreateHUD();
  					loadPlayerFromFile(3);
  				}
  			}
  			super.touchUp(event, x, y, pointer, button);
  		}
  	});

  	deleteButton.addListener(new ClickListener() {
  		@Override
  		public void touchUp(InputEvent event, float x, float y, int pointer,int button) {
  			if(playerCount==0) {
  				errorMsg.setText("Cannot delete that item.");
  			}
  			else if(playerCount==1) {
  				if(selectTwo.isChecked())
  					errorMsg.setText("Cannot delete that item.");
  				else {
  					--playerCount;
  					PlayerFileUtils.DeleteFromFile(1);
  					selectionTable.remove();
  					selectionTable = new Table();
  					hudStage.addActor(selectionTable);
  					selectionTable.toBack();
  					menuTable.toBack();
  					bg.toBack();
  					selectionTable.setSize(width, height);
  					selectionTable.setPosition(0, 55);
  					selectTwo = new TextButton("", skin, "menu");
  					selectTwo.setTouchable(Touchable.disabled);

  					selectionTable.add(selectOne).bottom().right().width(150).height(140).padRight(15);
  					selectionTable.add(selectTwo).bottom().right().width(150).height(140).padLeft(10).padRight(15);
  					selectionTable.add(selectThree).bottom().right().width(150).height(140).padLeft(10);

  					playerOneName = "New Game";
  					playerOneImage = "data/img/native/newchar.png";

  					playerTwoName = "";
  					playerTwoImage = "data/img/native/emptychar.png";

  					playersTable.remove();
  					playersTable = new Table();
  					hudStage.addActor(playersTable);
  					playersTable.setSize(width, height);
  					playersTable.setPosition(0, 0);

  					Texture texture = new Texture(Gdx.files.internal(playerOneImage));
  					texture.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
  					texture.setWrap(TextureWrap.ClampToEdge, TextureWrap.ClampToEdge);
  					region = new TextureRegion(texture, 40,256, 48, 128);
  					iPlayerOne = new Image(region);
  					iPlayerOne.setTouchable(Touchable.disabled);

  					texture = new Texture(Gdx.files.internal(playerTwoImage));
  					texture.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
  					texture.setWrap(TextureWrap.ClampToEdge, TextureWrap.ClampToEdge);
  					region = new TextureRegion(texture, 40,256, 48, 128);
  					iPlayerTwo = new Image(region);
  					iPlayerTwo.setTouchable(Touchable.disabled);

  					iPlayerOne.setScale(3);
  					iPlayerTwo.setScale(3);

  					playerOne.setText(playerOneName);
  					playerTwo.setText(playerTwoName);


  					if(Gdx.graphics.getHeight()==480) {
  						playersTable.add(iPlayerOne).bottom().padRight(62).padTop(360);
  						playersTable.add(iPlayerTwo).bottom().padLeft(62).padRight(62).padTop(360);
  						playersTable.add(iPlayerThree).bottom().padLeft(62).padTop(360);
  					}
  					else {
  						playersTable.add(iPlayerOne).bottom().padRight(62).padTop(400);
  						playersTable.add(iPlayerTwo).bottom().padRight(62).padLeft(62).padTop(400);
  						playersTable.add(iPlayerThree).bottom().padLeft(62).padTop(400);
  					}
  				}
  			}
  			else if(playerCount==2) {
  				if(selectThree.isChecked())
  					errorMsg.setText("Cannot delete that item.");
  				else {
  					--playerCount;
  					selectionTable.remove();
  					selectionTable = new Table();
  					hudStage.addActor(selectionTable);
  					selectionTable.toBack();
  					menuTable.toBack();
  					bg.toBack();
  					selectionTable.setSize(width, height);
  					selectionTable.setPosition(0, 55);
  					selectThree = new TextButton("", skin, "menu");
  					selectThree.setTouchable(Touchable.disabled);

  					selectionTable.add(selectOne).bottom().right().width(150).height(140).padRight(15);
  					selectionTable.add(selectTwo).bottom().right().width(150).height(140).padLeft(10).padRight(15);
  					selectionTable.add(selectThree).bottom().right().width(150).height(140).padLeft(10);

  					if(selectOne.isChecked()) {
  						PlayerFileUtils.DeleteFromFile(1);
  						PlayerFileUtils.UpdateId(2);
  						playerOneName = playerTwoName;
  						playerOneImage = playerTwoImage;

  						playerTwoName = "New Game";
  						playerTwoImage = "data/img/native/newchar.png";

  						playerThreeName = "";
  						playerThreeImage = "data/img/native/emptychar.png";

  						playersTable.remove();
  						playersTable = new Table();
  						hudStage.addActor(playersTable);
  						playersTable.setSize(width, height);
  						playersTable.setPosition(0, 0);

  						Texture texture = new Texture(Gdx.files.internal(playerOneImage));
  						texture.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
  						texture.setWrap(TextureWrap.ClampToEdge, TextureWrap.ClampToEdge);
  						region = new TextureRegion(texture, 40,256, 48, 128);
  						iPlayerOne = new Image(region);
  						iPlayerOne.setTouchable(Touchable.disabled);

  						texture = new Texture(Gdx.files.internal(playerTwoImage));
  						texture.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
  						texture.setWrap(TextureWrap.ClampToEdge, TextureWrap.ClampToEdge);
  						region = new TextureRegion(texture, 40,256, 48, 128);
  						iPlayerTwo = new Image(region);
  						iPlayerTwo.setTouchable(Touchable.disabled);

  						texture = new Texture(Gdx.files.internal(playerThreeImage));
  						texture.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
  						texture.setWrap(TextureWrap.ClampToEdge, TextureWrap.ClampToEdge);
  						region = new TextureRegion(texture, 40,256, 48, 128);
  						iPlayerThree = new Image(region);
  						iPlayerThree.setTouchable(Touchable.disabled);

  						iPlayerOne.setScale(3);
  						iPlayerTwo.setScale(3);
  						iPlayerThree.setScale(3);

  						playerOne.setText(playerOneName);
  						playerTwo.setText(playerTwoName);
  						playerThree.setText(playerThreeName);

  						if(Gdx.graphics.getHeight()==480) {
  							playersTable.add(iPlayerOne).bottom().padRight(62).padTop(360);
  							playersTable.add(iPlayerTwo).bottom().padLeft(62).padRight(62).padTop(360);
  							playersTable.add(iPlayerThree).bottom().padLeft(62).padTop(360);
  						}
  						else {
  							playersTable.add(iPlayerOne).bottom().padRight(62).padTop(400);
  							playersTable.add(iPlayerTwo).bottom().padRight(62).padLeft(62).padTop(400);
  							playersTable.add(iPlayerThree).bottom().padLeft(62).padTop(400);
  						}
  					}
  					else {
  						PlayerFileUtils.DeleteFromFile(2);
  						playerTwoName = "New Game";
  						playerTwoImage = "data/img/native/newchar.png";

  						playerThreeName = "";
  						playerThreeImage = "data/img/native/emptychar.png";

  						playersTable.remove();
  						playersTable = new Table();
  						hudStage.addActor(playersTable);
  						playersTable.setSize(width, height);
  						playersTable.setPosition(0, 0);

  						Texture texture = new Texture(Gdx.files.internal(playerTwoImage));
  						texture.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
  						texture.setWrap(TextureWrap.ClampToEdge, TextureWrap.ClampToEdge);
  						region = new TextureRegion(texture, 40,256, 48, 128);
  						iPlayerTwo = new Image(region);
  						iPlayerTwo.setTouchable(Touchable.disabled);

  						texture = new Texture(Gdx.files.internal(playerThreeImage));
  						texture.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
  						texture.setWrap(TextureWrap.ClampToEdge, TextureWrap.ClampToEdge);
  						region = new TextureRegion(texture, 40,256, 48, 128);
  						iPlayerThree = new Image(region);
  						iPlayerThree.setTouchable(Touchable.disabled);

  						iPlayerTwo.setScale(3);
  						iPlayerThree.setScale(3);

  						playerOne.setText(playerOneName);
  						playerTwo.setText(playerTwoName);
  						playerThree.setText(playerThreeName);

  						if(Gdx.graphics.getHeight()==480) {
  							playersTable.add(iPlayerOne).bottom().padRight(62).padTop(360);
  							playersTable.add(iPlayerTwo).bottom().padLeft(62).padRight(62).padTop(360);
  							playersTable.add(iPlayerThree).bottom().padLeft(62).padTop(360);
  						}
  						else {
  							playersTable.add(iPlayerOne).bottom().padRight(62).padTop(400);
  							playersTable.add(iPlayerTwo).bottom().padRight(62).padLeft(62).padTop(400);
  							playersTable.add(iPlayerThree).bottom().padLeft(62).padTop(400);
  						}
  					}
  				}
  			}
  			else {
  				--playerCount;
  				if(selectOne.isChecked()) {
  					PlayerFileUtils.DeleteFromFile(1);
  					PlayerFileUtils.UpdateId(2);
  					PlayerFileUtils.UpdateId(3);
  					playerOneName = playerTwoName;
  					playerOneImage = playerTwoImage;

  					playerTwoName = playerThreeName;
  					playerTwoImage = playerThreeImage;

  					playerThreeName = "New Game";
  					playerThreeImage = "data/img/native/newchar.png";

  					playersTable.remove();
  					playersTable = new Table();
  					hudStage.addActor(playersTable);
  					playersTable.setSize(width, height);
  					playersTable.setPosition(0, 0);

  					Texture texture = new Texture(Gdx.files.internal(playerOneImage));
  					texture.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
  					texture.setWrap(TextureWrap.ClampToEdge, TextureWrap.ClampToEdge);
  					region = new TextureRegion(texture, 40,256, 48, 128);
  					iPlayerOne = new Image(region);
  					iPlayerOne.setTouchable(Touchable.disabled);

  					texture = new Texture(Gdx.files.internal(playerTwoImage));
  					texture.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
  					texture.setWrap(TextureWrap.ClampToEdge, TextureWrap.ClampToEdge);
  					region = new TextureRegion(texture, 40,256, 48, 128);
  					iPlayerTwo = new Image(region);
  					iPlayerTwo.setTouchable(Touchable.disabled);

  					texture = new Texture(Gdx.files.internal(playerThreeImage));
  					texture.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
  					texture.setWrap(TextureWrap.ClampToEdge, TextureWrap.ClampToEdge);
  					region = new TextureRegion(texture, 40,256, 48, 128);
  					iPlayerThree = new Image(region);
  					iPlayerThree.setTouchable(Touchable.disabled);

  					iPlayerOne.setScale(3);
  					iPlayerTwo.setScale(3);
  					iPlayerThree.setScale(3);

  					playerOne.setText(playerOneName);
  					playerTwo.setText(playerTwoName);
  					playerThree.setText(playerThreeName);

  					if(Gdx.graphics.getHeight()==480) {
  						playersTable.add(iPlayerOne).bottom().padRight(62).padTop(360);
  						playersTable.add(iPlayerTwo).bottom().padLeft(62).padRight(62).padTop(360);
  						playersTable.add(iPlayerThree).bottom().padLeft(62).padTop(360);
  					}
  					else {
  						playersTable.add(iPlayerOne).bottom().padRight(62).padTop(400);
  						playersTable.add(iPlayerTwo).bottom().padRight(62).padLeft(62).padTop(400);
  						playersTable.add(iPlayerThree).bottom().padLeft(62).padTop(400);
  					}
  				}
  				else if(selectTwo.isChecked()) {
  					PlayerFileUtils.DeleteFromFile(2);
  					PlayerFileUtils.UpdateId(3);
  					playerTwoName = playerThreeName;
  					playerTwoImage = playerThreeImage;

  					playerThreeName = "New Game";
  					playerThreeImage = "data/img/native/newchar.png";

  					playersTable.remove();
  					playersTable = new Table();
  					hudStage.addActor(playersTable);
  					playersTable.setSize(width, height);
  					playersTable.setPosition(0, 0);

  					Texture texture = new Texture(Gdx.files.internal(playerTwoImage));
  					texture.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
  					texture.setWrap(TextureWrap.ClampToEdge, TextureWrap.ClampToEdge);
  					region = new TextureRegion(texture, 40,256, 48, 128);
  					iPlayerTwo = new Image(region);
  					iPlayerTwo.setTouchable(Touchable.disabled);

  					texture = new Texture(Gdx.files.internal(playerThreeImage));
  					texture.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
  					texture.setWrap(TextureWrap.ClampToEdge, TextureWrap.ClampToEdge);
  					region = new TextureRegion(texture, 40,256, 48, 128);
  					iPlayerThree = new Image(region);
  					iPlayerThree.setTouchable(Touchable.disabled);

  					iPlayerTwo.setScale(3);
  					iPlayerThree.setScale(3);

  					playerOne.setText(playerOneName);
  					playerTwo.setText(playerTwoName);
  					playerThree.setText(playerThreeName);

  					if(Gdx.graphics.getHeight()==480) {
  						playersTable.add(iPlayerOne).bottom().padRight(62).padTop(360);
  						playersTable.add(iPlayerTwo).bottom().padLeft(62).padRight(62).padTop(360);
  						playersTable.add(iPlayerThree).bottom().padLeft(62).padTop(360);
  					}
  					else {
  						playersTable.add(iPlayerOne).bottom().padRight(62).padTop(400);
  						playersTable.add(iPlayerTwo).bottom().padRight(62).padLeft(62).padTop(400);
  						playersTable.add(iPlayerThree).bottom().padLeft(62).padTop(400);
  					}
  				}
  				else {
  					PlayerFileUtils.DeleteFromFile(3);
  					playerThreeName = "New Game";
  					playerThreeImage = "data/img/native/newchar.png";

  					playersTable.remove();
  					playersTable = new Table();
  					hudStage.addActor(playersTable);
  					playersTable.setSize(width, height);
  					playersTable.setPosition(0, 0);

  					Texture texture = new Texture(Gdx.files.internal(playerThreeImage));
  					texture.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
  					texture.setWrap(TextureWrap.ClampToEdge, TextureWrap.ClampToEdge);
  					region = new TextureRegion(texture, 40,256, 48, 128);
  					iPlayerThree = new Image(region);
  					iPlayerThree.setTouchable(Touchable.disabled);

  					iPlayerThree.setScale(3);

  					playerOne.setText(playerOneName);
  					playerTwo.setText(playerTwoName);
  					playerThree.setText(playerThreeName);

  					if(Gdx.graphics.getHeight()==480) {
  						playersTable.add(iPlayerOne).bottom().padRight(62).padTop(360);
  						playersTable.add(iPlayerTwo).bottom().padLeft(62).padRight(62).padTop(360);
  						playersTable.add(iPlayerThree).bottom().padLeft(62).padTop(360);
  					}
  					else {
  						playersTable.add(iPlayerOne).bottom().padRight(62).padTop(400);
  						playersTable.add(iPlayerTwo).bottom().padRight(62).padLeft(62).padTop(400);
  						playersTable.add(iPlayerThree).bottom().padLeft(62).padTop(400);
  					}
  				}
  			}
  			super.touchUp(event, x, y, pointer, button);
  		}
  	});

  	newGamePlay.addListener(new ClickListener() {

  		@Override
  		public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
  			if(tf.getText().equals("")) {
  				errorMsg.setText("Error: No name entered. Please try again.");
  			}
  			else if(tf.getText().length() > 12) {
  				errorMsg.setText("Error: Name is too long. Please enter 12 or less characters.");
  			}
  			else if(tf.getText().equals(playerOneName)) {
  				errorMsg.setText("Error: Player name already taken. Please try again.");
  			}
  			else if(tf.getText().equals(playerTwoName)) {
  				errorMsg.setText("Error: Player name already taken. Please try again.");
  			}
  			else if(tf.getText().equals(playerThreeName)) {
  				errorMsg.setText("Error: Player name already taken. Please try again.");
  			}
  			else {
  				Player.Gender gender = Player.Gender.MALE;
  				if(genderButton.isChecked())
  					gender = Player.Gender.FEMALE;

  				// Save
  				int selectedId = -1;
  				if(selectOne.isChecked())
  					selectedId = 1;
  				else if(selectTwo.isChecked())
  					selectedId = 2;
  				else
  					selectedId = 3;
  				
  				// Create the player
          createNewPlayer(gender, selectedId, tf.getText());
  				
  				PlayerFileUtils.WriteToFile(selectedId, gender, tf.getText());

  				CreateHUD();
  				
  				// Start the game
  				startGame();
  			}
  			super.touchUp(event, x, y, pointer, button);
  		}

  	});

  	if(isStartClosed==false) {
  		menu = new Table();
  		hudStage.addActor(menu);
  		menu.setSize(width, height);
  		menu.setPosition(0, 0);

  		imageTable = new Table();
  		hudStage.addActor(imageTable);
  		imageTable.setSize(width, height);
  		imageTable.setPosition(0, 0);

  		startMenu = new Table();
  		hudStage.addActor(startMenu);
  		startMenu.setSize(width, height);
  		startMenu.setPosition(0, 0);

  		Texture textureMenuArea = new Texture(Gdx.files.internal("data/img/menusquare.png"));
  		textureMenuArea.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
  		textureMenuArea.setWrap(TextureWrap.Repeat, TextureWrap.Repeat);
  		region = new TextureRegion(textureMenuArea, 0,0, 512, 300);
  		Image iMenuArea = new Image(region);

  		Texture textureImageToken = new Texture(Gdx.files.internal("data/img/swords.png"));
  		textureImageToken.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
  		textureImageToken.setWrap(TextureWrap.ClampToEdge, TextureWrap.ClampToEdge);
  		region = new TextureRegion(textureImageToken, 0,0, 256, 256);
  		Image iToken = new Image(region);

  		startButton = new TextButton("Start", skin, "default");
  		aboutButton = new TextButton("About", skin, "default");

  		startMenu.add(startButton).bottom().right().height(50).width(200).padRight(20).padTop(25);
  		startMenu.add(aboutButton).bottom().right().height(50).width(200).padLeft(20).padTop(25);
  		menu.add(iMenuArea).bottom().right().padTop(25);
  		imageTable.add(iToken).bottom().right().padTop(25);

  		startButton.addListener(new ClickListener() {
  			@Override
  			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
  				isStartClosed = true;
  				startMenu.remove();
  				menu.remove();
  				imageTable.remove();
  				super.touchUp(event, x, y, pointer, button);
  			}
  		});

  		aboutButton.addListener(new ClickListener() {
  			@Override
  			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
  				menuAboutOrHelp = new Table();
  				hudStage.addActor(menuAboutOrHelp);
  				menuAboutOrHelp.setSize(width, height);
  				menuAboutOrHelp.setPosition(0, 0);

  				backTable = new Table();
  				hudStage.addActor(backTable);
  				backTable.setSize(width, height);
  				backTable.setPosition(0, 0);

  				Texture textureAbout = new Texture(Gdx.files.internal("data/img/aboutus.png"));
  				textureAbout.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
  				textureAbout.setWrap(TextureWrap.Repeat, TextureWrap.Repeat);
  				region = new TextureRegion(textureAbout, 0,0, 512, 275);
  				Image iAbout = new Image(region);

  				backButton = new TextButton("Back", skin, "default");
  				backButton.addListener(new ClickListener() {

  					@Override
  					public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
  						backTable.remove();
  						menuAboutOrHelp.remove();
  						super.touchUp(event, x, y, pointer, button);
  					}

  				});
  				menuAboutOrHelp.add(iAbout).bottom().right();
  				backTable.add(backButton).bottom().right().height(40).width(200).padTop(275);
  				super.touchUp(event, x, y, pointer, button);
  			}
  		});
  }
  
  // Draw the hud
	hudStage.draw();
	
	//Select the first player and their gender
	selectOne.toggle();
}
  /**
   * Reposition UI items to center on application resize
   * @author Kyle
   */
  public void repositionUI () {
  	bg.remove();
  	menu.remove();
  	startMenu.remove();
		logoTable.remove();
		menuTable.remove();
		imageTable.remove();
    selectionTable.remove();
		websiteTable.remove();
    labelTable.remove();
    playersTable.remove();
    buttonsTable.remove();
    toggleButtonsTable.remove();
    errorTable.remove();
    newGameTable.remove();
    newGameBackTable.remove();
    newGameMenuTable.remove();
    newGameBehindTable.remove();
    newPlayerTable.remove();
    newImageTable.remove();
    newGameLogo.remove();
    textFieldTable.remove();
    positionUI();
    
    if(isFullOn) {
    	fullscreenButton.toggle();
    }
  }
  
  /**
   * Create a new player
   * @param gender
   */
  private void createNewPlayer(Player.Gender gender, int id, String name) {
  	if (Game.player != null && Game.entities.getEntities().contains(Game.player))
  		Game.entities.remove(Game.player);
  	
  	Game.player = new Player(gender);
  	Game.player.setId(id);
  	Game.player.setName(name);
    Game.player.setScale(new Vector2(2, 2));
    Game.player.setCurrentAnimation(AnimationKey.IDLE_SOUTH);
    Game.player.setDirection("south");
    Game.player.setPosition(99, 28);
    Game.player.setDesination(99, 28);
  }
  
  /**
   * Used to construct the player and start the game.
   */
  private void startGame() {
    Game.state = GameState.PLAYING;
    SoundManager.getInstance().stopMusic("forest.ogg");
  }
  
  /**
   * Used to load the player and start the game.
   * Image Attributes					= @param armor @param weapon
   * Position Attributes			=	@param x @param y
   * Strength Attributes 			= @param str @param strCurGoal @param strPrevGoal @param strProgress
   * Intelligence Attributes 	= @param intel @param intelCurGoal @param intelPrevGoal @param intelProgress
   * Dexterity Attributes			= @param dex @param dexCurGoal @param dexPrevGoal @param dexProgress
   * Vitality Attributes			= @param vit @param vitCurGoal @param vitPrevGoal @param vitProgress
   * Health Attributes				= @param maxHP @param curHP
   */
  private void startGame(int id, String gender, String name, String armor, String weapon, 
  		String x, String y, String str, String strCurGoal, String strPrevGoal, String strProgress, 
  		String intel, String intelCurGoal, String intelPrevGoal, String intelProgress,
  		String dex, String dexCurGoal, String dexPrevGoal, String dexProgress, 
  		String vit, String vitCurGoal, String vitPrevGoal, String vitProgress, 
  		String maxHP, String curHP ) {
    
  	if (Game.player != null && Game.entities.getEntities().contains(Game.player))
  		Game.entities.remove(Game.player);
  	
  	Gender g;
  	if(gender.equals("male"))
  		g = Gender.MALE;
  	else
  		g = Gender.FEMALE;
  	
    Game.player = new Player(g, armor, weapon, 
    		Integer.parseInt(str), Integer.parseInt(strCurGoal), Integer.parseInt(strPrevGoal), Integer.parseInt(strProgress),
    		Integer.parseInt(intel), Integer.parseInt(intelCurGoal), Integer.parseInt(intelPrevGoal), Integer.parseInt(intelProgress),
    		Integer.parseInt(dex), Integer.parseInt(dexCurGoal), Integer.parseInt(dexPrevGoal), Integer.parseInt(dexProgress),
    		Integer.parseInt(vit), Integer.parseInt(vitCurGoal), Integer.parseInt(vitPrevGoal), Integer.parseInt(vitProgress),
    		Integer.parseInt(maxHP), Integer.parseInt(curHP));
    Game.player.setId(id);
    Game.player.setName(name);
    Game.player.setScale(new Vector2(2, 2));
    Game.player.setCurrentAnimation(AnimationKey.IDLE_SOUTH);
    Game.player.setDirection("south");
    Game.player.setPosition(Float.valueOf(x), Float.valueOf(y));
    Game.player.setDesination(Float.valueOf(x), Float.valueOf(y));
    
    Game.state = GameState.PLAYING;
    SoundManager.getInstance().stopMusic("forest.ogg");
  }
  /**
   * Load player and start game
   */
  private void loadPlayerFromFile(int selectedPlayer) {
  	try {
			XmlReader reader = new XmlReader();
			Element root;
			root = reader.parse(Gdx.files.local("player.xml"));
			Array<Element> players = root.getChildrenByName("player");
			for (Element child : players){
				int id = Integer.parseInt(child.getAttribute("id"));
				if(id == selectedPlayer) {
					startGame(id, child.getAttribute("gender"), child.getAttribute("name"), 
						child.getAttribute("image"), child.getAttribute("weapon"), child.getAttribute("x"), child.getAttribute("y"),
						child.getAttribute("str"), child.getAttribute("strCurGoal"), child.getAttribute("strPrevGoal"), child.getAttribute("strProgress"),
						child.getAttribute("int"), child.getAttribute("intCurGoal"), child.getAttribute("intPrevGoal"), child.getAttribute("intProgress"),
						child.getAttribute("dex"), child.getAttribute("dexCurGoal"), child.getAttribute("dexPrevGoal"), child.getAttribute("dexProgress"),
						child.getAttribute("vit"), child.getAttribute("vitCurGoal"), child.getAttribute("vitPrevGoal"), child.getAttribute("vitProgress"),
						child.getAttribute("maxHP"), child.getAttribute("curHP"));
				}
			}
		} 
		catch (IOException e) {
			
		}
  }
  
  public void playMusic() {
  	if(isSoundOn) {
  		if (!soundButton.isChecked())
  			soundButton.toggle();
    }
    SoundManager.getInstance().play("forest.ogg", true, AudioResourceType.MUSIC);
  }
  
  public void pauseMusic() {
    SoundManager.getInstance().pauseMusic("forest.ogg");
  }
}
