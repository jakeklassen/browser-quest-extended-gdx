package com.bitknight.bqex.screens;

import java.util.List;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.TiledMapTile.BlendMode;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.maps.tiled.tiles.AnimatedTiledMapTile;
import com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;
import com.bitknight.bqex.Game;
import com.bitknight.bqex.Game.AppType;
import com.bitknight.bqex.entities.Enemy;
import com.bitknight.bqex.entities.EntityType;
import com.bitknight.bqex.entities.IDrawable;
import com.bitknight.bqex.entities.ITarget;
import com.bitknight.bqex.items.Loot;
import com.bitknight.bqex.items.Weapon;
import com.bitknight.bqex.utils.PlayerFileUtils;
import com.bitknight.bqex.utils.maps.TileCollisionMap;
import com.bitknight.sound.AudioResourceType;
import com.bitknight.sound.SoundManager;
import com.bitknight.utils.GameState;
import com.bitknight.utils.IScreen;
import com.bitknight.utils.MultipleVirtualViewportBuilder;
import com.bitknight.utils.OrthographicCameraWithVirtualViewport;
import com.bitknight.utils.VirtualViewport;
import com.bitknight.utils.pathfinding.IPathFinder;
import com.bitknight.utils.pathfinding.JumpPointSearch;
import com.bitknight.utils.pathfinding.Path;
import com.esotericsoftware.tablelayout.BaseTableLayout.Debug;

/**
 * TODO Refactor, refactor, refactor.
 */

/**
 * In game screen.
 * 
 * @author Jake Klassen
 *
 */
public class GameScreen implements IScreen, InputProcessor {
  public static OrthographicCameraWithVirtualViewport hudCamera;
  public static OrthographicCameraWithVirtualViewport camera;
  // Creates virtual viewports
  private MultipleVirtualViewportBuilder multipleVirtualViewportBuilder;
  private SpriteBatch spriteBatch;
  private TiledMap map;
  private TiledMapTileLayer displayLayer, collision;
  // Store the indexes of layers we render without interaction
  // required.
  private int[] backgroundLayers;
  private int[] foregroundLayers;
  private OrthogonalTiledMapRenderer mapRenderer;
  // Used for visual debugging
  private BitmapFont font;
  private ShapeRenderer shapeRenderer;

  // Need to know the size of the playable area of the map that has focus,
  // for camera bounds.
  private int PLAYABLE_MAP_WIDTH = 100;
  private int PLAYABLE_MAP_HEIGHT = 100;
  // Used to create virtual viewports
  private final int MIN_SUPPORTED_WIDTH = 640;
  private final int MIN_SUPPORTED_HEIGHT = 480;
  private final int MAX_SUPPORTED_WIDTH = 1920;
  private final int MAX_SUPPORTED_HEIGHT = 1200;
  // Used to determine map camera bounds
  private float horizontalTiles = 0;
  private float verticalTiles = 0;
  private final float zoomFactor = 48f;
  private int hoverTileX = 0;
  private int hoverTileY = 0;

  private Cell cell;
  private Cell destinationCell;
  private Vector2 lastClickedPos;
  private TiledMapTile canMoveToTile;
  private Vector3 mouseWorldPosition = Vector3.Zero;
  private TiledMapTile cannotMoveToTile;
  private AnimatedTiledMapTile animatedStopTile;
  private AnimatedTiledMapTile animatedGoTile;

  private Texture mapTileSheet;

  // Path finding members
  private IPathFinder finder;
  private Path path;
  private TileCollisionMap tileCollisionMap;

  float height, width, deviceZoomFactor = 1;

  // Track who the player touched
  private ITarget target = null;
  private Texture cursorHandTexture, cursorSwordTexture, 
          cursorLootTexture, cursorTalkTexture, cursorTexture = null;
  
	//Hud Variables
	Stage hudStage;
	TextureRegion barTexture, healthBarTexture, manaBarTexture, 
	  weaponTexture, armourTexture, messagePaneTexture, buttonPaneTexture,
	  speechButtonTexture, healthStatusTexture, manaStatusTexture, itemSlotsTexture;
	Image healthStatusImage, manaStatusImage, healthBarImage, manaBarImage,
	  messagePaneImage, buttonPaneImage, itemSlotsImage, panelImage,
	    panelLeftImage, panelTopLeftImage, panelTopImage, panelTopRightImage,
	    panelRightImage, panelBottomRightImage, panelBottomImage, panelBottomLeftImage,
	    currentWeaponImage, currentArmorImage;
	Button speechButton, characterButton, soundButton, aboutButton,
      menuAboutButton, menuSaveButton, menuExitButton, menuResumeButton,
      closeAboutButton, titlescreenButton;
	Label statusLabel, aboutLabel;
	Skin skin;
	Table barsTable, barsStatusTable, buttonsTable, menuTable, menuButtonTable,
	  aboutTable, aboutButtonTable, itemsTable;
	float healthBarSize;
	 
	public static InputMultiplexer inputMultiplexer;
	
	public static boolean isMusicPaused = false;

  /**
   * Setup the game
   */
  public GameScreen() {
  	width = Gdx.graphics.getWidth();
  	height = Gdx.graphics.getHeight();

  	// Setup the viewport and cameras
  	multipleVirtualViewportBuilder = new MultipleVirtualViewportBuilder(
  			MIN_SUPPORTED_WIDTH, MIN_SUPPORTED_HEIGHT, MAX_SUPPORTED_WIDTH,
  			MAX_SUPPORTED_HEIGHT);
  	VirtualViewport virtualViewport = multipleVirtualViewportBuilder
  			.getVirtualViewport(width, height);
  	camera = new OrthographicCameraWithVirtualViewport(virtualViewport);
  	hudCamera = new OrthographicCameraWithVirtualViewport(virtualViewport);

  	// Hide the cursor on the desktop
  	//Gdx.input.setCursorCatched(true)

  	// Setup the animated tiles
  	Array<StaticTiledMapTile> tileArray;
  	// Start position on the sheet
  	int startX = 192;
  	int startY = 1568;

  	mapTileSheet = new Texture(Gdx.files.internal("data/maps/tilesheet.png"));
  	mapTileSheet.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
  	TextureRegion region = null;
  	// We are trying to load two strips of 4 frames, 8 total
  	for (int i = 0; i < 2; ++i) {
  		tileArray = new Array<StaticTiledMapTile>(4);
  		for (int j = 0; j < 4; ++j) {
  			region = new TextureRegion(mapTileSheet, startX, startY, 16, 16);
  			tileArray.add(new StaticTiledMapTile(region));
  			startX += 16;
  		}

  		if (i == 0) {
  			animatedStopTile = new AnimatedTiledMapTile(1 / 10f, tileArray);
  		} else {
  			animatedGoTile = new AnimatedTiledMapTile(1 / 10f, tileArray);
  		}
  	}

  	// Load the map
  	map = new TmxMapLoader().load("data/maps/base.tmx");
  	// Prepare the layers for batching.
  	// We need to know the count before and after the display layer.
  	// Skip any layer with a value of skip = true
  	int bgCount = 0, fgCount = 0;
  	boolean foundDisplay = false;
  	for (MapLayer m : map.getLayers()) {
  		if (m.getProperties().get("skip") != null && 
  				m.getProperties().get("skip").toString().equalsIgnoreCase("true"))
  			continue;

  		if (m.getName().toString().equalsIgnoreCase("display")) {
  			foundDisplay = true;
  			continue;
  		}

  		if (foundDisplay)
  			++fgCount;
  		else
  			++bgCount;
  	}
  	backgroundLayers = new int[bgCount];
  	foregroundLayers = new int[fgCount];
  	for (int i = 0; i < bgCount; ++i)
  		backgroundLayers[i] = i;

  	for (int i = map.getLayers().getCount() - fgCount, j = 0; i < map.getLayers().getCount(); ++i, ++j)
  		foregroundLayers[j] = i;

  	// Get some map layers for reference
  	displayLayer = (TiledMapTileLayer) map.getLayers().get("display");
  	collision = (TiledMapTileLayer) map.getLayers().get("collision");

  	// Setup the two tiles that show movable and not movable sprites
  	canMoveToTile = map.getTileSets().getTileSet(0).getTile(1959);
  	canMoveToTile.setBlendMode(BlendMode.ALPHA);
  	cannotMoveToTile = map.getTileSets().getTileSet(0).getTile(1958);
  	cannotMoveToTile.setBlendMode(BlendMode.ALPHA);

  	// Setup graphical cells
  	cell = new Cell();
  	cell.setTile(canMoveToTile);
  	destinationCell = new Cell();
  	destinationCell.setTile(animatedGoTile);

  	// Track last clicked position in terms of cell units.
  	lastClickedPos = Vector2.Zero;
  	mapRenderer = new OrthogonalTiledMapRenderer(map, 1 / 16f);
  	spriteBatch = new SpriteBatch();
  	shapeRenderer = new ShapeRenderer();

  	font = new BitmapFont(Gdx.files.internal("data/consolas.fnt"), false);
  	font.setScale(1f);

  	// From the map
  	loadEnemies();

  	// Setting up the path finding
  	tileCollisionMap = new TileCollisionMap((TiledMapTileLayer) map.getLayers().get("collision"));
  	finder = new JumpPointSearch((TiledMapTileLayer) map.getLayers().get("collision"), true);

  	cursorHandTexture = new Texture(Gdx.files.internal("data/img/native/hand.png"));
  	cursorTalkTexture = new Texture(Gdx.files.internal("data/img/native/talk.png"));
  	cursorLootTexture = new Texture(Gdx.files.internal("data/img/native/loot.png"));
  	cursorSwordTexture = new Texture(Gdx.files.internal("data/img/native/sword.png"));
  	cursorTexture = cursorHandTexture;

  	Game.entities.add(
  			new Loot("item-darksword1.png", "Dark Sword", new Vector2(1, 1), 
  					new Vector2(52, 43), new Weapon("darksword1.png", 5, new Vector2(52, 43)))
  			);

  	/*
  	 * Setup the UI
  	 */
  	skin = new Skin();

  	// Create the atlas of texture regions and add to the skin resource pool
  	skin.addRegions(new TextureAtlas(Gdx.files.internal("data/ui/hud/uiskins.atlas")));

  	// Load styles from json file
  	skin.load(Gdx.files.internal("data/ui/hud/uiskin.json"));

  	// Create the stage
  	hudStage = new Stage();
  	hudStage.setViewport(width, height, false);

  	// Table to hold the status of the health and mana bars
  	barsStatusTable = new Table();
  	hudStage.addActor(barsStatusTable);
  	barsStatusTable.setSize(width, height);
  	barsStatusTable.setPosition(0, 0);

  	// Health bar status
  	healthStatusTexture = new TextureRegion(skin.getRegion("health-status"));
  	healthStatusImage = new Image(healthStatusTexture);
  	barsStatusTable.add(healthStatusImage).top().left().height(18).width(174).padTop(9).padLeft(29);

  	// Store the size of the health status image
  	healthBarSize = 174f;

  	barsStatusTable.row();

  	// Mana bar status
  	manaStatusTexture = new TextureRegion(skin.getRegion("mana-status"));
  	manaStatusImage = new Image(manaStatusTexture);
  	barsStatusTable.add(manaStatusImage).expand().top().left().height(18).width(174).padTop(3).padLeft(44);
  	
  	barsStatusTable.row();  	
  	
  	// Table to hold the status bars
  	barsTable = new Table();
  	hudStage.addActor(barsTable);
  	barsTable.setSize(width, height);
  	barsTable.setPosition(0, 0);

  	// Health bar texture
  	healthBarTexture = new TextureRegion(skin.getRegion("health-bar"));
  	healthBarImage = new Image(healthBarTexture);
  	barsTable.add(healthBarImage).top().left().height(26).width(202).padTop(5).padLeft(5);

  	barsTable.row();

  	// Mana bar texture
  	manaBarTexture = new TextureRegion(skin.getRegion("mana-bar"));
  	manaBarImage = new Image(manaBarTexture);
  	barsTable.add(manaBarImage).expand().top().left().height(26).width(202).padLeft(20).padTop(-5);

  	// Place new row at bottom of screen
  	barsTable.row();

  	// Message pane
  	messagePaneTexture = new TextureRegion(skin.getRegion("message-pane"));
  	messagePaneImage = new Image(messagePaneTexture);        
  	barsTable.add(messagePaneImage).expand().bottom().fillX().height(36);

  	// Button pane
  	buttonPaneTexture = new TextureRegion(skin.getRegion("button-pane"));
  	buttonPaneImage = new Image(buttonPaneTexture);
  	barsTable.add(buttonPaneImage).bottom().right().fillX().width(129).height(36);

  	// ButtonsTable
  	buttonsTable = new Table();
  	hudStage.addActor(buttonsTable);
  	buttonsTable.setSize(width, height);
  	buttonsTable.setPosition(0, 0);

  	// Item Slots
  	itemSlotsTexture = new TextureRegion(skin.getRegion("item-slots"));
  	itemSlotsImage = new Image(itemSlotsTexture);
  	buttonsTable.add(itemSlotsImage).bottom().left().padLeft(4).height(36).width(70);

  	// Status Label
  	statusLabel = new Label("", skin);
  	statusLabel.setAlignment(Align.center);
  	statusLabel.setFontScale(0.8f);
  	buttonsTable.add(statusLabel).bottom().padBottom(3).expandX();

  	// Speech Button
  	speechButton = new Button(skin, "speechButton");
  	buttonsTable.add(speechButton).expandY().bottom().right().width(30).height(30).padLeft(2).padRight(2).padBottom(2);
  	speechButton.addListener(new ButtonHandler());

  	// Character Button
  	characterButton = new Button(skin, "characterButton");
  	buttonsTable.add(characterButton).bottom().right().width(30).height(30).padRight(2).padBottom(2);
  	characterButton.addListener(new ButtonHandler());

  	// Sound Button
  	soundButton = new Button(skin, "soundButton");
  	soundButton.toggle();
  	buttonsTable.add(soundButton).bottom().right().width(30).height(30).padRight(2).padBottom(2);
  	soundButton.addListener(new ButtonHandler());

  	// About Button
  	aboutButton = new Button(skin, "aboutButton");
  	buttonsTable.add(aboutButton).bottom().right().width(30).height(30).padRight(2).padBottom(2);
  	aboutButton.addListener(new ButtonHandler());

  	// Items Table
  	itemsTable = new Table();
  	hudStage.addActor(itemsTable);

  	// Show weapon
  	
   	// Show armor
   	currentArmorImage = new Image(Game.getUI().getArmor());
   	barsStatusTable.add(currentArmorImage).bottom().left().height(32).padBottom(2);

  	// Create Menu Panel
  	menuTable = new Table();
  	hudStage.addActor(menuTable);
  	menuTable.setPosition(width / 2, height / 2);

  	// Panel pieces
  	panelImage = new Image(new TextureRegion(skin.getRegion("panel")));
  	panelLeftImage = new Image(new TextureRegion(skin.getRegion("panel-border-left")));
  	panelTopLeftImage = new Image(new TextureRegion(skin.getRegion("panel-border-top-left")));
  	panelTopImage = new Image(new TextureRegion(skin.getRegion("panel-border-top")));
  	panelTopRightImage = new Image(new TextureRegion(skin.getRegion("panel-border-top-right")));
  	panelRightImage = new Image(new TextureRegion(skin.getRegion("panel-border-right")));
  	panelBottomRightImage = new Image(new TextureRegion(skin.getRegion("panel-border-bottom-right")));
  	panelBottomImage = new Image(new TextureRegion(skin.getRegion("panel-border-bottom")));
  	panelBottomLeftImage = new Image(new TextureRegion(skin.getRegion("panel-border-bottom-left")));

  	// Add pieces to the panel
  	menuTable.add(panelTopLeftImage).width(8).height(6);
  	menuTable.add(panelTopImage).width(180).height(6);
  	menuTable.add(panelTopRightImage).width(8).height(6);
  	menuTable.row();
  	menuTable.add(panelLeftImage).width(8).height(220);
  	menuTable.add(panelImage).width(180).height(220);
  	menuTable.add(panelRightImage).width(8).height(220);
  	menuTable.row();
  	menuTable.add(panelBottomLeftImage).width(8).height(6);
  	menuTable.add(panelBottomImage).width(180).height(6);
  	menuTable.add(panelBottomRightImage).width(8).height(6);

  	// Create Menu Button Table
  	menuButtonTable = new Table();
  	hudStage.addActor(menuButtonTable);
  	menuButtonTable.setPosition(width / 2, height / 2);

  	// Create Menu buttons
  	// Menu About Button
  	menuAboutButton = new Button(skin, "menuAbout");
  	menuButtonTable.add(menuAboutButton).width(150).height(35).padTop(5).padBottom(5);
  	menuButtonTable.row();
  	menuAboutButton.addListener(new ButtonHandler());


  	// Menu Save Button
  	menuSaveButton = new Button(skin, "menuSave");
  	menuButtonTable.add(menuSaveButton).width(150).height(35).padTop(5).padBottom(5);
  	menuButtonTable.row();
  	menuSaveButton.addListener(new ButtonHandler());

  	// Titlescreen Button
  	titlescreenButton = new Button(skin, "titlescreenButton");
  	menuButtonTable.add(titlescreenButton).width(150).height(35).padTop(5).padBottom(5);
  	menuButtonTable.row();
  	titlescreenButton.addListener(new ButtonHandler());

  	// Menu Exit Button
  	menuExitButton = new Button(skin, "menuExit");
  	menuButtonTable.add(menuExitButton).width(150).height(35).padTop(5).padBottom(5);
  	menuButtonTable.row();
  	menuExitButton.addListener(new ButtonHandler());

  	//Menu Resume Button
  	menuResumeButton = new Button(skin, "menuResume");
  	menuButtonTable.add(menuResumeButton).width(150).height(35).padTop(5).padBottom(5);
  	menuResumeButton.addListener(new ButtonHandler());

  	menuTable.setVisible(false);
  	menuButtonTable.setVisible(false);

  	// About Table
  	aboutTable = new Table();
  	hudStage.addActor(aboutTable);
  	aboutTable.setPosition(width / 2, height / 2);

  	// Panel pieces
  	panelImage = new Image(new TextureRegion(skin.getRegion("panel")));
  	panelLeftImage = new Image(new TextureRegion(skin.getRegion("panel-border-left")));
  	panelTopLeftImage = new Image(new TextureRegion(skin.getRegion("panel-border-top-left")));
  	panelTopImage = new Image(new TextureRegion(skin.getRegion("panel-border-top")));
  	panelTopRightImage = new Image(new TextureRegion(skin.getRegion("panel-border-top-right")));
  	panelRightImage = new Image(new TextureRegion(skin.getRegion("panel-border-right")));
  	panelBottomRightImage = new Image(new TextureRegion(skin.getRegion("panel-border-bottom-right")));
  	panelBottomImage = new Image(new TextureRegion(skin.getRegion("panel-border-bottom")));
  	panelBottomLeftImage = new Image(new TextureRegion(skin.getRegion("panel-border-bottom-left")));

  	// About Panel
  	aboutTable.add(panelTopLeftImage).width(8).height(6);
  	aboutTable.add(panelTopImage).width(300).height(6);
  	aboutTable.add(panelTopRightImage).width(8).height(6);
  	aboutTable.row();
  	aboutTable.add(panelLeftImage).width(8).height(320);
  	aboutTable.add(panelImage).width(300).height(320);
  	aboutTable.add(panelRightImage).width(8).height(320);
  	aboutTable.row();
  	aboutTable.add(panelBottomLeftImage).width(8).height(6);
  	aboutTable.add(panelBottomImage).width(300).height(6);
  	aboutTable.add(panelBottomRightImage).width(8).height(6);

  	// About Button Table
  	aboutButtonTable = new Table();
  	hudStage.addActor(aboutButtonTable);
  	aboutButtonTable.setPosition(width / 2, height / 2 - 130);

  	// Add buttons and labels to table
  	String aboutMessage = "What is BrowserQuest?" +
  			"\nBrowserQuest is an open source role-playing game built with " +
  			"HTML5 and Javascript. This enhanced version has been rebuilt " +
  			"in native Java using LibGDX so it is compatible with Windows, " +
  			"Android, Linux, and Mac. The improved pathfinding and the new" +
  			"multi-directional movement as well as gender selection will " +
  			"provide players with more control over their characters. " +
  			"Now go out and kill some ogres and goblins" +
  			"\nWho are the BitKnights?" +
  			"\nOur team is comprised of Jake Klassen, John Klassen, Ryan " +
  			"MacEacheron, and Kyle Dymock";
  	aboutLabel = new Label(aboutMessage, skin);
  	aboutButtonTable.add(aboutLabel).width(300).height(20);
  	aboutLabel.setWrap(true);
  	aboutLabel.setFontScale(0.5f);

  	aboutButtonTable.row();

  	// Close About Button
  	closeAboutButton = new Button(skin, "aboutClose");
  	aboutButtonTable.add(closeAboutButton).width(150).height(35);
  	closeAboutButton.addListener(new ButtonHandler());

  	// Hide the about panel
  	aboutTable.setVisible(false);
  	aboutButtonTable.setVisible(false);

  	// Handle input from HUD then game
  	inputMultiplexer = new InputMultiplexer(this);
  	inputMultiplexer.addProcessor(0, hudStage);
  	//Gdx.input.setInputProcessor(inputMultiplexer);
  }
  
  @Override
  public void update(float delta) {
    Game.entities.update(delta);
    camera.update();
    
    // TODO Should be removed when no longer debugging
    if (Gdx.input.isKeyPressed(Input.Keys.NUMPAD_2)) {
      camera.zoom += 0.1f * delta;
    }
    if (Gdx.input.isKeyPressed(Input.Keys.NUMPAD_8)) {
      camera.zoom -= 0.1f * delta;
    }

    // Move the camera to the player
    float lerp = 0.1f;
    camera.position.x += (Game.player.getPosition().x + 1 - camera.position.x)
        * lerp;
    camera.position.y += (Game.player.getPosition().y + 1 - camera.position.y)
        * lerp;
    camera.position.x = adjustPosition(camera.position.x, zoomFactor);
    camera.position.y = adjustPosition(camera.position.y, zoomFactor);

    // Bound the camera to map edges.
    // Position is the center of the camera.
    if (camera.position.x > PLAYABLE_MAP_WIDTH - horizontalTiles / 2) {
      camera.position.x = PLAYABLE_MAP_WIDTH - horizontalTiles / 2;
    }
    if (camera.position.x < horizontalTiles / 2) {
      camera.position.x = horizontalTiles / 2;
    }

    if (camera.position.y < verticalTiles / 2) {
      camera.position.y = verticalTiles / 2;
    }
    if (camera.position.y > PLAYABLE_MAP_HEIGHT - verticalTiles / 2) {
      camera.position.y = PLAYABLE_MAP_HEIGHT - verticalTiles / 2;
    }

    camera.update();

    // Clear the last cell for mouse hover
    if (displayLayer.getCell(hoverTileX, hoverTileY) != destinationCell)
      displayLayer.setCell(hoverTileX, hoverTileY, null);

    // Convert screen coordinates to world coordinates
    mouseWorldPosition = new Vector3(Gdx.input.getX(0),
        Gdx.input.getY(0), 0);
    camera.unproject(mouseWorldPosition);
    hoverTileX = (int) (mouseWorldPosition.x);
    hoverTileY = (int) (mouseWorldPosition.y);

    if (Game.getAppType() != AppType.MOBILE) {
      if (collision.getCell(hoverTileX, hoverTileY) != null) {
        Cell tmpCell = collision.getCell(hoverTileX, hoverTileY);
        TiledMapTile tile = tmpCell.getTile();
        if (tile != null) {
          cell.setTile(cannotMoveToTile);
        } else {
          cell.setTile(canMoveToTile);
        }
      } else {
        cell.setTile(canMoveToTile);
      }
      
      if (displayLayer.getCell(hoverTileX, hoverTileY) != destinationCell)
        displayLayer.setCell(hoverTileX, hoverTileY, cell);
    }

    // Remove the animated move to tile if the player reaches their destination
    if (Game.player.getPosition().equals(Game.player.getDestination())) {
      displayLayer.setCell((int) (Game.player.getPosition().x),
          (int) (Game.player.getPosition().y), null);
    }
    
    // HUD update
    barsStatusTable.getCell(healthStatusImage).width(healthBarSize * 
    		(Game.player.getHealthComponent().getHealthAsPercentage()));
    barsStatusTable.invalidate();
  }

  @Override
  public void render() {
    Gdx.gl.glClearColor(0, 0, 0, 1);
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

    // Map rendering code
    mapRenderer.setView(camera);
    mapRenderer.render(backgroundLayers);

    // How to handle on the mobile?
    mapRenderer.getSpriteBatch().begin();
    mapRenderer.renderTileLayer(displayLayer);
    mapRenderer.getSpriteBatch().end();

    mapRenderer.getSpriteBatch().begin();
    Game.entities.render(mapRenderer.getSpriteBatch());
    mapRenderer.getSpriteBatch().end();
    
    mapRenderer.render(foregroundLayers);


    spriteBatch.setProjectionMatrix(hudCamera.combined);
    spriteBatch.begin();
    font.setColor(Color.WHITE);
    
    Game.getTweenBuilder().render(spriteBatch);

    /*
     * spriteBatch.draw(cursorHandTexture, Gdx.input.getX() - 2,
     * Gdx.graphics.getHeight() - Gdx.input.getY() - 40, 0, 0, 14, 14, 3, 3, 0,
     * 0, 0, 14, 14, false, false);
     */
    spriteBatch.end();
    
    // Render Hud
    hudStage.draw();
    
    // Cursor above HUD
    spriteBatch.begin();
    // Draw mouse cursor - non-mobile
    if (Game.getAppType() != AppType.MOBILE) {
      spriteBatch.draw(cursorTexture, Gdx.input.getX() - 2,
          Gdx.graphics.getHeight() - Gdx.input.getY() - 40, 0, 0, 14, 14,
          3, 3, 0, 0, 0, 14, 14, false, false);
    }
    spriteBatch.end();
  }

  @Override
  public void resize(int width, int height) {
    this.width = width;
    this.height = height;

    // Reset the cameras
    VirtualViewport virtualViewport = multipleVirtualViewportBuilder.getVirtualViewport(width, height);
    camera.setVirtualViewport(virtualViewport);
    camera.position.set(0f, 0f, 0f);
    camera.zoom = 1 / zoomFactor;
    camera.updateViewport();

    hudCamera.setVirtualViewport(virtualViewport);
    hudCamera.position.set(0f, 0f, 0f);
    hudCamera.updateViewport();

    // Store how many horizontal and vertical tiles are visible at once
    verticalTiles = 1 / zoomFactor * camera.virtualViewport.getVirtualHeight();
    horizontalTiles = 1 / zoomFactor * camera.virtualViewport.getVirtualWidth();
    
    // HUD Resizing
    // Get the dimensions of the screen
    float deviceWidthIn, deviceHeightIn;
    deviceWidthIn = Gdx.graphics.getWidth() / Gdx.graphics.getPpiX();
    deviceHeightIn = Gdx.graphics.getHeight() / Gdx.graphics.getPpiY();
    
    float deviceDiagnoalIn = (float)Math.sqrt((deviceWidthIn * deviceWidthIn) + (deviceHeightIn * deviceHeightIn));
    if(deviceDiagnoalIn < 11 && Gdx.app.getType().equals(ApplicationType.Android) && deviceZoomFactor == 1f) {
      deviceZoomFactor = 2f;
      float menuZoomFactor = 1.5f;
      
      // Scale the width of each cell in each table
      com.esotericsoftware.tablelayout.Cell<?> cell;
      cell = barsStatusTable.getCell(healthStatusImage);
      cell.width(cell.getMaxWidth() * deviceZoomFactor);
      cell.height(cell.getMaxHeight() * deviceZoomFactor);
      cell.padLeft(cell.getPadLeft() * deviceZoomFactor);
      cell.padTop(cell.getPadTop() * deviceZoomFactor);
      // Store the size of the health status image
      healthBarSize = cell.getMaxWidth();
      
      // Resize Bars Status Table
      cell = barsStatusTable.getCell(manaStatusImage);
      cell.width(cell.getMaxWidth() * deviceZoomFactor);
      cell.height(cell.getMaxHeight() * deviceZoomFactor);
      cell.padLeft(cell.getPadLeft() * deviceZoomFactor);
      cell.padTop(cell.getPadTop() * deviceZoomFactor);
      barsStatusTable.invalidate();
      
      // Resize Bars Table
      cell = barsTable.getCell(healthBarImage);
      cell.width(cell.getMaxWidth() * deviceZoomFactor);
      cell.height(cell.getMaxHeight() * deviceZoomFactor);
      cell.padLeft(cell.getPadLeft() * deviceZoomFactor);
      cell.padTop(cell.getPadTop() * deviceZoomFactor);
      cell = barsTable.getCell(manaBarImage);
      cell.width(cell.getMaxWidth() * deviceZoomFactor);
      cell.height(cell.getMaxHeight() * deviceZoomFactor);
      cell.padLeft(cell.getPadLeft() * deviceZoomFactor);
      cell.padTop(cell.getPadTop() * deviceZoomFactor);
      cell = barsTable.getCell(messagePaneImage);
      cell.height(cell.getMaxHeight() * deviceZoomFactor);
      cell = barsTable.getCell(buttonPaneImage);
      cell.width(cell.getMaxWidth() * deviceZoomFactor);
      cell.height(cell.getMaxHeight() * deviceZoomFactor);
      barsTable.invalidate();
      
      // Resize the Status Label
      statusLabel.setFontScale(1.6f);
      buttonsTable.getCell(statusLabel).padBottom(10);
      
      // Resize Button Table
      cell = buttonsTable.getCell(itemSlotsImage);
      cell.width(cell.getMaxWidth() * deviceZoomFactor);
      cell.height(cell.getMaxHeight() * deviceZoomFactor);
      cell.padLeft(cell.getPadLeft() * deviceZoomFactor);
      cell = buttonsTable.getCell(speechButton);
      cell.width(cell.getMaxWidth() * deviceZoomFactor);
      cell.height(cell.getMaxHeight() * deviceZoomFactor);
      cell.padRight(cell.getPadRight() * deviceZoomFactor);
      cell.padBottom(cell.getPadBottom() * deviceZoomFactor);
      cell.padLeft(cell.getPadLeft() * deviceZoomFactor);
      cell = buttonsTable.getCell(characterButton);
      cell.width(cell.getMaxWidth() * deviceZoomFactor);
      cell.height(cell.getMaxHeight() * deviceZoomFactor);
      cell.padRight(cell.getPadRight() * deviceZoomFactor);
      cell.padBottom(cell.getPadBottom() * deviceZoomFactor);
      cell = buttonsTable.getCell(soundButton);
      cell.width(cell.getMaxWidth() * deviceZoomFactor);
      cell.height(cell.getMaxHeight() * deviceZoomFactor);
      cell.padRight(cell.getPadRight() * deviceZoomFactor);
      cell.padBottom(cell.getPadBottom() * deviceZoomFactor);
      cell = buttonsTable.getCell(aboutButton);
      cell.width(cell.getMaxWidth() * deviceZoomFactor);
      cell.height(cell.getMaxHeight() * deviceZoomFactor);
      cell.padRight(cell.getPadRight() * deviceZoomFactor);
      cell.padBottom(cell.getPadBottom() * deviceZoomFactor);
      buttonsTable.invalidate();
      
      // Resize Menus
      List<com.esotericsoftware.tablelayout.Cell> cells = menuTable.getCells();
      cell = cells.get(1);
      cell.width(cell.getMaxWidth() * menuZoomFactor);
      cell = cells.get(3);
      cell.height(cell.getMaxHeight() * menuZoomFactor);
      cell = cells.get(4);
      cell.height(cell.getMaxHeight() * menuZoomFactor);
      cell.width(cell.getMaxWidth() * menuZoomFactor);
      cell = cells.get(5);
      cell.height(cell.getMaxHeight() * menuZoomFactor);
      cell = cells.get(7);
      cell.width(cell.getMaxWidth() * menuZoomFactor);
      menuTable.invalidate();
      
      cell = aboutTable.getCell(panelTopImage);
      cell.width(cell.getMaxWidth() * menuZoomFactor);
      cell = aboutTable.getCell(panelRightImage);
      cell.height(cell.getMaxHeight() * menuZoomFactor);
      cell = aboutTable.getCell(panelBottomImage);
      cell.width(cell.getMaxWidth() * menuZoomFactor);
      cell = aboutTable.getCell(panelLeftImage);
      cell.height(cell.getMaxHeight() * menuZoomFactor);
      cell = aboutTable.getCell(panelImage);
      cell.height(cell.getMaxHeight() * menuZoomFactor);
      cell.width(cell.getMaxWidth() * menuZoomFactor);
      aboutTable.invalidate();
      
      // Resize Menu Buttons and Font
      cell = menuButtonTable.getCell(menuAboutButton);
      cell.width(cell.getMaxWidth() * menuZoomFactor);
      cell.height(cell.getMaxHeight() * menuZoomFactor);
      cell = menuButtonTable.getCell(menuSaveButton);
      cell.width(cell.getMaxWidth() * menuZoomFactor);
      cell.height(cell.getMaxHeight() * menuZoomFactor);
      cell = menuButtonTable.getCell(titlescreenButton);
      cell.width(cell.getMaxWidth() * menuZoomFactor);
      cell.height(cell.getMaxHeight() * menuZoomFactor);
      cell = menuButtonTable.getCell(menuExitButton);
      cell.width(cell.getMaxWidth() * menuZoomFactor);
      cell.height(cell.getMaxHeight() * menuZoomFactor);
      cell = menuButtonTable.getCell(menuResumeButton);
      cell.width(cell.getMaxWidth() * menuZoomFactor);
      cell.height(cell.getMaxHeight() * menuZoomFactor);
      menuButtonTable.invalidate();
      
      cell = aboutButtonTable.getCell(closeAboutButton);
      cell.width(cell.getMaxWidth() * menuZoomFactor);
      cell.height(cell.getMaxHeight() * menuZoomFactor);
      cell = aboutButtonTable.getCell(aboutLabel);
      cell.width(cell.getMaxWidth() * menuZoomFactor);
      aboutLabel.setFontScale(0.8f);
      aboutButtonTable.invalidate();
    }
    
    // Resizing the screen
    barsTable.setSize(virtualViewport.getWidth(), virtualViewport.getHeight());
    barsStatusTable.setSize(virtualViewport.getWidth(), virtualViewport.getHeight());
    buttonsTable.setSize(virtualViewport.getWidth(), virtualViewport.getHeight());
    itemsTable.setSize(virtualViewport.getWidth(), virtualViewport.getHeight());
    
    menuTable.setPosition(virtualViewport.getWidth() / 2, virtualViewport.getHeight() / 2);
    menuButtonTable.setPosition(virtualViewport.getWidth() / 2, virtualViewport.getHeight() / 2);
    aboutTable.setPosition(virtualViewport.getWidth() / 2, virtualViewport.getHeight() / 2);
    if(deviceZoomFactor == 1f) {
      aboutButtonTable.setPosition(virtualViewport.getWidth() / 2, virtualViewport.getHeight() / 2 - 130);
    }
    else {
      aboutButtonTable.setPosition(virtualViewport.getWidth() / 2, virtualViewport.getHeight() / 2 - 20);
    }
    hudStage.setViewport(virtualViewport.getWidth(), virtualViewport.getHeight(), false);
    
    barsTable.invalidate();
    barsStatusTable.invalidate();
    buttonsTable.invalidate();
    itemsTable.invalidate();
    menuTable.invalidate();
    menuButtonTable.invalidate();
    aboutTable.invalidate();
    aboutButtonTable.invalidate();
  }

  @Override
  public void show() {
    // TODO Auto-generated method stub

  }

  @Override
  public void hide() {
    // TODO Auto-generated method stub

  }

  @Override
  public void pause() {
  	PlayerFileUtils.WriteToFile(Game.player);
  }

  @Override
  public void resume() {
    // TODO Auto-generated method stub
  }

  @Override
  public void dispose() {
    spriteBatch.dispose();
    mapTileSheet.dispose();
    cursorHandTexture.dispose();
    cursorSwordTexture.dispose();
    cursorLootTexture.dispose();
    cursorTalkTexture.dispose();
    font.dispose();
    hudStage.dispose();
    skin.dispose();
  }
  
  // HUD handlers
  
  public class ButtonHandler extends ChangeListener {

    @Override
    public void changed(ChangeEvent event, Actor actor) {
      // TODO Auto-generated method stub
      if(actor.equals(speechButton)) {
        
      }
      else if(actor.equals(characterButton)) {
        
      }
      else if(actor.equals(soundButton)) {
        if (isMusicPaused)
          playMusic();
        else
          pauseMusic();
        
        isMusicPaused = !isMusicPaused;
      }
      else if(actor.equals(aboutButton)) {
        if(aboutButton.isChecked()) {
          menuTable.setVisible(true);
          menuButtonTable.setVisible(true);
        }
        else
        {
          menuTable.setVisible(false);
          menuButtonTable.setVisible(false);
          aboutTable.setVisible(false);
          aboutButtonTable.setVisible(false);
        }
      }
      else if(actor.equals(menuResumeButton)) {
        menuTable.setVisible(false);
        menuButtonTable.setVisible(false);
        aboutButton.setChecked(false);
      }
      else if(actor.equals(menuAboutButton)) {
        aboutTable.setVisible(true);
        aboutButtonTable.setVisible(true);
      }
      else if(actor.equals(menuSaveButton)) {
        PlayerFileUtils.WriteToFile(Game.player);
        statusLabel.setText("Game saved.");
        
        // Clear the message in 2 seconds
        float delay = 2; // seconds

        Timer.schedule(new Task(){
          @Override
          public void run() {
              statusLabel.setText("");
          }
        }, delay);
      }
      else if(actor.equals(menuExitButton)) {
        Gdx.app.exit();
      }
      else if(actor.equals(titlescreenButton)) {
      	PlayerFileUtils.WriteToFile(Game.player);
        Game.state = GameState.TITLE_SCREEN;
      }
      else if(actor.equals(closeAboutButton)) {
        aboutTable.setVisible(false);
        aboutButtonTable.setVisible(false);
      }
    }
    
  }

  @Override
  public boolean keyDown(int keycode) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public boolean keyUp(int keycode) {
    if (keycode  == Input.Keys.ESCAPE) {
      Gdx.app.exit();
    }

    return false;
  }

  @Override
  public boolean keyTyped(char character) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public boolean touchDown(int screenX, int screenY, int pointer, int button) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public boolean touchUp(int screenX, int screenY, int pointer, int button) {
    if (Game.player.isDead())
      return true;
    
    // Ignore anything in the hud region along the bottom
    if ( Gdx.input.getY(0) >= Gdx.graphics.getHeight() - barsTable.getCell(messagePaneImage).getMaxHeight())
    	return true;
    
    TiledMapTileLayer collision = (TiledMapTileLayer) map.getLayers().get("collision"),
        display = (TiledMapTileLayer) map.getLayers().get("display");

    // Check if we are over an entity of some sort
    for (IDrawable entity : Game.entities.getEntities()) {
      switch (entity.getType()) {
      case PLAYER:
      	break;
      case LOOT:
      	Loot loot = (Loot) entity;
      	if (loot != null) {
      		if (loot.getHitBox().contains(mouseWorldPosition.x, mouseWorldPosition.y)) {
      			ITarget t = (ITarget) entity;
      			if (t != null) {
      				target = t;

      				// If the player is not on the item, look for it
              if (Math.abs(target.getPosition().dst2(Game.player.getPosition())) >= 1) {               
                path = finder.findPath(Game.player, (int) Game.player.getPosition().x,
                    (int) Game.player.getPosition().y, (int) t.getPosition().x, (int) t.getPosition().y);
                if(path != null) {
                  clearLastPathNodeCell();
                  Game.player.setPath(path);
                  Game.player.setTarget(t);
                  display.setCell((int)t.getPosition().x, (int)t.getPosition().y, destinationCell);
                  clearLastClickedCell();
                  return true;
                }
              }
      			}
      		}
      	}
      	break;
      case ENEMY:
      	// Enemy check
        Enemy e = (Enemy) entity;
        if (e != null) {
          if (e.getHitBox().contains(mouseWorldPosition.x, mouseWorldPosition.y)) {
            ITarget t = (ITarget)e;
            if( t != null ) {
              target = t;
              Vector2 targetVec = Game.player.getPosition().cpy();
              // If the player is more than one cell away or on a diagonal from
              // the enemy, look for a new position.
              if (Math.abs(target.getPosition().dst2(Game.player.getPosition())) > 1 ||
                  (Game.player.getPosition().x != target.getPosition().x && 
                      Game.player.getPosition().y != target.getPosition().y)) {
                // Get the closest free cell around the target
                Array<Vector2> vecs = new Array<Vector2>();
                // clockwise starting with north
                vecs.add(new Vector2(target.getPosition().x, target.getPosition().y + 1));
                vecs.add(new Vector2(target.getPosition().x + 1, target.getPosition().y));
                vecs.add(new Vector2(target.getPosition().x, target.getPosition().y - 1));
                vecs.add(new Vector2(target.getPosition().x - 1, target.getPosition().y));

                float dst = Float.MAX_VALUE;
                // loop through vectors
                for(Vector2 v : vecs) {
                  Cell tmpCell = collision.getCell((int)v.x, (int)v.y);
                  if (tmpCell == null) {
                    if (Math.abs(v.dst(Game.player.getPosition())) < dst) {
                      dst = Math.abs(v.dst(Game.player.getPosition()));
                      targetVec = v;
                    }
                  }
                }
              }

              path = finder.findPath(Game.player, (int) Game.player.getPosition().x,
                  (int) Game.player.getPosition().y, (int) targetVec.x, (int) targetVec.y);
              if(path != null) {
                clearLastPathNodeCell();
                Game.player.setPath(path);
                Game.player.setTarget(t);
                display.setCell((int)targetVec.x, (int)targetVec.y, destinationCell);
                clearLastClickedCell();
                return true;
              }
            }
          }
        }
      	break;
      case NPC:
      	break;
      	
      	default:
      	  Game.player.setTarget(null);
      	  break;
      }
    }
    
    // Once user lifts finger or mouse button, set the target tile to the
    // animated move to tile. Only happens if that tile is not a collision tile.
    Cell tmpCell = collision.getCell(hoverTileX, hoverTileY);
    if (tmpCell == null || tmpCell.getTile() != null) {
      if (tmpCell == null || !tmpCell.getTile().getProperties().get("type").toString().equalsIgnoreCase("collision")) {
        clearLastPathNodeCell();
        clearLastClickedCell();

        display.setCell(hoverTileX, hoverTileY, destinationCell);

        // Don't search for a path if player is clicking where they are
        // standing. Don't search for a path beyond the bounds of the map.
        if (((int) Game.player.getPosition().x == hoverTileX && 
            (int) Game.player.getPosition().y == hoverTileY) || 
            hoverTileX < 0 ||
            hoverTileX >= tileCollisionMap.getWidthInTiles() ||
            hoverTileY < 0 ||
            hoverTileY >= tileCollisionMap.getHeightInTiles())
          return true;

        // Clear player target
        Game.player.setTarget(null);

        // Get new player path
        path = finder.findPath(Game.player, (int) Game.player.getPosition().x,
            (int) Game.player.getPosition().y, hoverTileX, hoverTileY);

        // If null, the number of moves exceeds the max search distance, or no
        // path was found.
        if (path != null)
          Game.player.setPath(path);
      }
    }

    return true;
  }

  @Override
  public boolean touchDragged(int screenX, int screenY, int pointer) {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public boolean mouseMoved(int screenX, int screenY) {   
    boolean cursorResolved = false;
    // Determine cursor texture 
    for (IDrawable entity : Game.entities.getEntities()) {
      if (cursorResolved)
        break;
      
      if (entity.getType() == EntityType.ENEMY) {
        Enemy e = (Enemy) entity;
        if (e != null) {
          if (e.getHitBox().contains(mouseWorldPosition.x, mouseWorldPosition.y)) {
            cursorTexture = cursorSwordTexture;
            cursorResolved = true;
          }
        }
      }
      else if (entity.getType() == EntityType.LOOT) {
        Loot l = (Loot) entity;
        if (l != null) {
          if (l.getHitBox().contains(mouseWorldPosition.x, mouseWorldPosition.y)) {
            cursorTexture = cursorLootTexture;
            cursorResolved = true;
          }
        }
      }
    }
    
    if (!cursorResolved) {
      cursorTexture = cursorHandTexture;
    }
    
    return true;
  }

  @Override
  public boolean scrolled(int amount) {
    // TODO Auto-generated method stub
    return false;
  }

  // Methods
  
  private void clearLastPathNodeCell() {
    TiledMapTileLayer display = (TiledMapTileLayer) map.getLayers().get("display");
    // Clear destination if we had one
    if (Game.player.getPath() != null) {
      display.setCell((int) Game.player.getPath().last().getX(), 
          (int) Game.player.getPath().last().getY(), null);
    }
  }
  
  private void clearLastClickedCell() {
    TiledMapTileLayer display = (TiledMapTileLayer) map.getLayers().get("display");
    // Get rid of the last destination cell if the player didn't reach it
    // and clicked somewhere else.
    if (lastClickedPos.x != hoverTileX || lastClickedPos.y != hoverTileY) {
      display.setCell((int) lastClickedPos.x, (int) lastClickedPos.y, null);
      lastClickedPos = new Vector2(hoverTileX, hoverTileY);
    }
  }
  
  /**
   * Loop through entities layer looking for enemies;
   */
  private void loadEnemies() {
  	TiledMapTileLayer entities = (TiledMapTileLayer) map.getLayers().get("entities");
  	Cell tmpCell = null;
  	for (int x = 0; x < entities.getWidth(); ++x) {
  		for (int y = 0; y < entities.getHeight(); ++y) {
  			tmpCell = entities.getCell(x, y);
  			if (tmpCell != null && tmpCell.getTile() != null)
  				if (!tmpCell.getTile().getProperties().get("type").toString().equals(null))
  					if (tmpCell.getTile().getProperties().get("type").toString().equalsIgnoreCase("enemy")) 
  						if (tmpCell.getTile().getProperties().get("name").toString().equalsIgnoreCase("goblin"))
  							new Enemy(new Vector2(x, y));
  		}
  	}
  }
  

  // Helpers
  
  public void playMusic() {
    SoundManager.getInstance().play("village.ogg", true, AudioResourceType.MUSIC);
  }
  
  public void pauseMusic() {
    SoundManager.getInstance().pauseMusic("village.ogg");
  }
  
  /**
   * Used to ensure we don't rest the camera at a large or repeating fraction.
   * 
   * @param pos The initial position
   * @param factor  The acceptable value, e.g. 0.25
   * @return Adjusted value
   */
  private float adjustPosition(float pos, float factor) {
    final float limit = 1 / factor;
    float fraction = pos - (int) pos, base = (int) pos;

    return base + (int) (fraction / limit) * limit;
  }
  
}
