package com.bitknight.bqex;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class UI {
  
  public UI() {
    this.health = 0;
    this.maxHealth = 0;
    this.mana = 100;
    this.maxMana = 100;
    this.weaponTexture = null;
    this.armorTexture = null;
    this.healthPots = 10;
  }
  
  private int health, maxHealth, mana, maxMana, healthPots;
  private String weaponFileName;
  private String armorFileName;
  private TextureRegion weaponTexture;
  private TextureRegion armorTexture;
  
  public int getHealth() {
    return health;
  }
  
  public void setHealth(int health) {
    this.health = health;
  }
  
  public int getMaxHealth() {
    return maxHealth;
  }
  
  public void setMaxHealth(int maxHealth) {
    this.maxHealth = maxHealth;
  }
  
  public int getMaxMana() {
    return maxMana;
  }
  
  public void setMaxMana(int maxMana) {
    this.maxMana = maxMana;
  }
  
  public int getMana() {
    return mana;
  }
  
  public void setMana(int mana) {
    this.mana = mana;
  }
  
  public TextureRegion getWeapon() {
    return weaponTexture;
  }
  
  public void setWeapon(String weaponFileName) {
    this.weaponFileName = weaponFileName;
    
    // Set weaponTexture texture region
  }
  
  public TextureRegion getArmor() {
  	return new TextureRegion(Game.assetManager.get("data/img/native/item-clotharmor.png", Texture.class),
    		16, 16);
  }
  
  public void setArmor(String armorFileName) {
  	this.armorFileName = armorFileName;
  	
  	// Set armorTexture texture region
  }
  
  public int getHealthPots() {
    return healthPots;
  }
  
  public void setHealthPots(int healthPots) {
    this.healthPots = healthPots;
  }
  
}
