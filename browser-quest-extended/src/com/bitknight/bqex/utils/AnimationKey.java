package com.bitknight.bqex.utils;

public enum AnimationKey {
	EAST(0), WEST(1), SOUTH(2), NORTH(3), 
	IDLE_SOUTH(4), IDLE_NORTH(5), IDLE_EAST(6), IDLE_WEST(7), 
	ATTACK_SOUTH(8), ATTACK_NORTH(9), ATTACK_EAST(10), ATTACK_WEST(11), 
	DEATH(12);
	
	private final int value;

	AnimationKey(int value) {
    this.value = value;
  }

  public int getValue() {
    return value;
  }
	
}
