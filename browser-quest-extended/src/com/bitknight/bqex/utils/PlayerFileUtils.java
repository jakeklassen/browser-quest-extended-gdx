package com.bitknight.bqex.utils;

import java.io.IOException;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlReader.Element;
import com.bitknight.bqex.entities.Player;

/**
 * Used to save/load/created players
 * @author Jake Klassen
 * @author Kyle Dymock
 *
 */
public class PlayerFileUtils {

	/**
   * In Android Check to make sure xml file exists, if not create it
   * @author Kyle
   */
  public static void CheckForXMLFile() {
		boolean doesFileExist = Gdx.files.local("player.xml").exists();
		if(doesFileExist==false) {
			FileHandle from = Gdx.files.internal("data/player.xml");
			from.copyTo(Gdx.files.local("player.xml"));
		}
	}
  
  /**
   * Change the player id after a delete
   * @author Kyle
   * @param selectedPlayer
   */
  public static void UpdateId(int selectedPlayer) {
		try {
			XmlReader reader = new XmlReader();
			Element root;
			root = reader.parse(Gdx.files.local("player.xml"));
			
			Array<Element> players = root.getChildrenByName("player");
			for (Element child : players){
				int id = Integer.parseInt(child.getAttribute("id"));
				if(id == selectedPlayer) {
					child.setAttribute("id", Integer.toString(selectedPlayer-1));
				}
			}

			String xml = "<?xml version=\"1.0\"?>\r\n"  + root.toString();
			FileHandle file = Gdx.files.local("player.xml");
			file.writeString(xml, false);
		} 
		catch (IOException e) {}
	}
  
  /**
   * Remove the character from an XML file
   * @author Kyle
   * @param selectedPlayer (which player to be deleted)
   */
  public static void DeleteFromFile(int selectedPlayer) {
		try {
			XmlReader reader = new XmlReader();
			Element root;
			root = reader.parse(Gdx.files.local("player.xml"));
			root.removeChild(selectedPlayer-1);

			String xml = "<?xml version=\"1.0\"?>\r\n"  + root.toString();
			FileHandle file = Gdx.files.local("player.xml");
			file.writeString(xml, false);
		} 
		catch (IOException e) {}
	}
  
  /**
   *Write the new character to an XML file
   *@author Kyle
   */
  public static void WriteToFile(int selectedPlayerId, Player.Gender gender, String name) {
		try {
			XmlReader reader = new XmlReader();
			Element root, newChild;
			root = reader.parse(Gdx.files.local("player.xml"));
			newChild = new Element("player", root);
			
			newChild.setAttribute("weapon", "sword1.png");
			
			newChild.setAttribute("curHP", "13");
			newChild.setAttribute("maxHP", "13");
			newChild.setAttribute("curMP", "13");
			newChild.setAttribute("maxMP", "13");
			
			newChild.setAttribute("str", "5");
			newChild.setAttribute("strProgress", "0");
			newChild.setAttribute("strCurGoal", "50");
			newChild.setAttribute("strPrevGoal", "0");
			
			newChild.setAttribute("int", "5");
			newChild.setAttribute("intProgress", "0");
			newChild.setAttribute("intCurGoal", "50");
			newChild.setAttribute("intPrevGoal", "0");
			
			newChild.setAttribute("dex", "5");
			newChild.setAttribute("dexProgress", "0");
			newChild.setAttribute("dexCurGoal", "50");
			newChild.setAttribute("dexPrevGoal", "0");
			
			newChild.setAttribute("vit", "5");
			newChild.setAttribute("vitProgress", "0");
			newChild.setAttribute("vitCurGoal", "50");
			newChild.setAttribute("vitPrevGoal", "0");
			
			newChild.setAttribute("x", "90");
			newChild.setAttribute("y", "28");
			
			newChild.setAttribute("id", String.valueOf(selectedPlayerId));
			
			newChild.setAttribute("name", name);

			if(gender == Player.Gender.MALE) {
				newChild.setAttribute("gender", "male");
				newChild.setAttribute("image", "data/img/native/clotharmor.png");
			} 
			else {
				newChild.setAttribute("gender", "female");
				newChild.setAttribute("image", "data/img/native/clotharmor_female.png");			
		  }
			
			root.addChild(newChild);
			String xml = "<?xml version=\"1.0\"?>\r\n"  + root.toString();
			FileHandle file = Gdx.files.local("player.xml");
			file.writeString(xml, false);
		} 
		catch (IOException e) {}
	}
  
  /**
   * Write a specific player to file.
   * 
   * @author Jake Klassen
   * @param player
   */
  public static void WriteToFile(Player player) {
  	try {
			XmlReader reader = new XmlReader();
			Element root;
			root = reader.parse(Gdx.files.local("player.xml"));
			Array<Element> players = root.getChildrenByName("player");
			for (Element child : players){
				int id = Integer.parseInt(child.getAttribute("id"));
				if(id == player.getId()) {
					child.setAttribute("weapon", player.getWeapon().getTextureFileName());
					
					child.setAttribute("curHP", String.valueOf(player.getHealthComponent().getCurrentHP()));
					child.setAttribute("maxHP", String.valueOf(player.getHealthComponent().getMaxHP()));
					//TODO Change when MP is actually implemented
					child.setAttribute("curMP", String.valueOf(0));
					child.setAttribute("maxMP", String.valueOf(0));
					
					child.setAttribute("str", String.valueOf(player.strength.getValue()));
					child.setAttribute("strProgress", String.valueOf(player.strength.getStatProgress()));
					child.setAttribute("strCurGoal", String.valueOf(player.strength.getCurrentGoal()));
					child.setAttribute("strPrevGoal", String.valueOf(player.strength.getPreviousGoal()));
					
					child.setAttribute("int", String.valueOf(player.intelligence.getValue()));
					child.setAttribute("intProgress", String.valueOf(player.intelligence.getStatProgress()));
					child.setAttribute("intCurGoal", String.valueOf(player.intelligence.getCurrentGoal()));
					child.setAttribute("intPrevGoal", String.valueOf(player.intelligence.getPreviousGoal()));
					
					child.setAttribute("dex", String.valueOf(player.dexterity.getValue()));
					child.setAttribute("dexProgress", String.valueOf(player.dexterity.getStatProgress()));
					child.setAttribute("dexCurGoal", String.valueOf(player.dexterity.getCurrentGoal()));
					child.setAttribute("dexPrevGoal", String.valueOf(player.dexterity.getPreviousGoal()));
					
					child.setAttribute("vit", String.valueOf(player.vitality.getValue()));
					child.setAttribute("vitProgress", String.valueOf(player.vitality.getStatProgress()));
					child.setAttribute("vitCurGoal", String.valueOf(player.vitality.getCurrentGoal()));
					child.setAttribute("vitPrevGoal", String.valueOf(player.vitality.getPreviousGoal()));
					
					child.setAttribute("x", String.valueOf((int)player.getPosition().x));
					child.setAttribute("y", String.valueOf((int)player.getPosition().y));
					
					child.setAttribute("name", player.getName());

					if(player.getGender() == Player.Gender.MALE) {
						child.setAttribute("gender", "male");
						child.setAttribute("image", "data/img/native/clotharmor.png");
					} 
					else {
						child.setAttribute("gender", "female");
						child.setAttribute("image", "data/img/native/clotharmor_female.png");			
				  }
					
					String xml = "<?xml version=\"1.0\"?>\r\n"  + root.toString();
					FileHandle file = Gdx.files.local("player.xml");
					file.writeString(xml, false);
				}
			}
		} 
		catch (IOException e) {
			
		}
  }
}
