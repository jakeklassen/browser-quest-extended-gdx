package com.bitknight.bqex.utils;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Used to represent and message.
 * 
 * @author Jake Klassen
 *
 */
public class Message {
  private BitmapFont font;
  private String message;
  private float x;
  private float y;
  private Color color;
  private float scale;

  public Message (BitmapFont font, String message, float x, float y, Color color, 
      float scale) {
    this.font = font;
    this.message = message;
    this.x = x;
    this.y = y;
    this.color = color;
    this.scale = scale;

    font.setScale(this.scale);
    font.setColor(this.color);
  }

  public void dispose() {
    if (font != null)
      font.dispose();
  }

  public BitmapFont getFont() {
    return font;
  }

  public void setFont(BitmapFont font) {
    this.font = font;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public float getX() {
    return x;
  }

  public void setX(float x) {
    this.x = x;
  }

  public float getY() {
    return y;
  }

  public void setY(float y) {
    this.y = y;
  }

  public void setPosition(float x, float y) {
    this.x = x;
    this.y = y;
  }

  public Color getColor() {
    return color;
  }

  public void setColor(Color color) {
    this.color = color;
  }

  public float getScale() {
    return scale;
  }

  public void setScale(float scale) {
    this.scale = scale;
  }

  public void render(SpriteBatch sb) {
    font.setColor(color);
    font.setScale(scale);
    font.draw(sb, message, x, y);
  }

}
