package com.bitknight.bqex.utils.maps;

import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.bitknight.utils.pathfinding.IMover;
import com.bitknight.utils.pathfinding.ITileBasedMap;

/**
 * Representation for the collision layer to work with the path finding
 * code.
 * @author Jake Klasse
 *
 */
public class TileCollisionMap implements ITileBasedMap {
	// These are the costs of tiles set in the Tiled Map editor.
	// PASSABLE refers to an empty tile.
	public final int PASSABLE = 0;
	public final int ENTRANCE = 1;
	public final int BLOCKED = 10;
	
	private TiledMapTileLayer layer;
	private boolean[][] visited;
	private int[][] terrain;
	
	/**
	 * Contructs a collision map for path finding from a Tiled map layer.
	 * @param layer Tiled map tile layer that represents a collision layer.
	 */
	public TileCollisionMap(TiledMapTileLayer layer) {
		this.layer = layer;
		this.visited = new boolean[layer.getWidth()][layer.getHeight()];
		this.terrain = new int[layer.getWidth()][layer.getHeight()];
		
		// Traverse the tiled map tile layer and if anything is in a
		// cell, retrieve its cost.
		for (int x = 0; x < layer.getWidth(); ++x) {
			for (int y = 0; y < layer.getHeight(); ++y) {
				Cell c = layer.getCell(x, y);
				if( c != null ) {
					int cost = Integer.parseInt(c.getTile().getProperties().get("cost", String.class));
					
					switch(cost) {
					case PASSABLE:
						terrain[x][y] = PASSABLE;
						break;
					case ENTRANCE:
						terrain[x][y] = ENTRANCE;
						break;
					case BLOCKED:
						terrain[x][y] = BLOCKED;
						break;
					default:
						terrain[x][y] = BLOCKED;	
					}
				}
			}
		}
	}

	@Override
  public int getWidthInTiles() {
	  return this.layer.getWidth();
  }

	@Override
  public int getHeightInTiles() {
	  return this.layer.getHeight();
  }

	@Override
  public void pathFinderVisited(int x, int y) {
	  visited[x][y] = true;
  }
	
	/**
	 * Clear the array marking which tiles have been visted by the path 
	 * finder.
	 */
	public void clearVisited() {
		for (int x = 0; x < getWidthInTiles(); x++) {
			for (int y = 0; y < getHeightInTiles(); y++) {
				visited[x][y] = false;
			}
		}
	}

	@Override
  public boolean blocked(IMover mover, int x, int y) {
	  if( terrain[x][y] == BLOCKED  )
	  	return true;
		
	  // Some passable type
	  return false;
  }

	@Override
  public float getCost(IMover mover, int sx, int sy, int tx, int ty) {
		Cell cell = layer.getCell(tx, ty);
		if( cell == null )
			return 0;
		TiledMapTile tile = cell.getTile();
		if( tile == null )
			return 0;
		
	  return Integer.parseInt(tile.getProperties().get("cost").toString());
	}
	
}
