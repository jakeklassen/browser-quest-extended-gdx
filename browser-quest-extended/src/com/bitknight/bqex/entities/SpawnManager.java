package com.bitknight.bqex.entities;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

/**
 * Entity to manage and spawn enemies.
 * 
 * @author Jake Klassen
 * 
 */
public class SpawnManager {
  private Array<Enemy> enemies;
  private int maxEnemyCount;
  private Vector2 position;
  private float accumulator;
  private float timeToSpawn;

  public SpawnManager(Vector2 position, float timeToSpawn, int maxEnemyCount) {
    this.position = position;
    this.timeToSpawn = timeToSpawn;
    this.maxEnemyCount = maxEnemyCount;
    enemies = new Array<Enemy>();
  }

  public int getMaxEnemyCount() {
    return maxEnemyCount;
  }
  
  public void setMaxEnemyCount(int maxEnemyCount) {
    this.maxEnemyCount = maxEnemyCount;
  }
  
  public Vector2 getPosition() {
    return position;
  }
  
  public void setPosition(Vector2 position) {
    this.position = position;
  }
  
  public void setPosition(float x, float y) {
    position.x = x;
    position.y = y;
  }
  
  /**
   * Time to generate new enemies.
   * 
   * @return
   */
  public float getTimeToSpawn() {
    return timeToSpawn;
  }
  
  public void setTimeToSpawn(float timeToSpawn) {
    this.timeToSpawn = timeToSpawn;
  }
  
  public void removeEnemy(Enemy enemy) {
    if( enemies.contains(enemy, true) ) {
      enemies.removeValue(enemy, true);
    }
  }
  
  /**
   * Update internal timer. If we need to generate new enemies.
   * 
   * @param dt
   *          delta time
   */
  public void update(float dt) {
    if (enemies.size != maxEnemyCount) {
      accumulator += dt;
      if (accumulator >= timeToSpawn) {
        enemies.add(new Enemy(position));
      }
    }
  }
}
