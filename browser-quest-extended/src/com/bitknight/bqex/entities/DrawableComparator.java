package com.bitknight.bqex.entities;

import java.util.Comparator;

/**
 * Class containing comparators for IDrawable implementors.
 * 
 * @author Jake Klassen
 *
 */
public class DrawableComparator {

  /**
   * Comparator for Vector2 positions.
   * Cap x positions to whole integers because we don't care 
   * about partial x positions.
   */
  public static Comparator<IDrawable> PositionComparator = new Comparator<IDrawable>() {
    @Override
    public int compare(IDrawable o1, IDrawable o2) {
    	if (o1.getPosition().y > o2.getPosition().y) {
        return -1;
      }
      else if (o1.getPosition().y < o2.getPosition().y) {
        return 1;
      }
      else {
      	if (o1 instanceof Player) {
      		return 1;
      	}
      	else if (o2 instanceof Player)
      		return -1;
      	
        return 0;
      }
    }
  };
}
