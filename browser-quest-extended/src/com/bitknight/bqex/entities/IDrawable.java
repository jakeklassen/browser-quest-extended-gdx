package com.bitknight.bqex.entities;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Disposable;

/**
 * Tagging interface to sort a collection of IDrawables by
 * position.
 * 
 * @author Jake Klassen
 *
 */
public interface IDrawable extends Disposable {
	public Vector2 getPosition();
	public void setPosition(Vector2 position);
	public void update(float dt);
	public void render(SpriteBatch sb);
	public EntityType getType();
	public Rectangle getHitBox();
}
