package com.bitknight.bqex.entities;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.bitknight.bqex.Game;
import com.bitknight.bqex.screens.GameScreen;
import com.bitknight.bqex.utils.AnimationKey;
import com.bitknight.components.HealthComponent;
import com.bitknight.graphics.Animation;
import com.bitknight.sound.AudioResourceType;
import com.bitknight.sound.SoundManager;
import com.bitknight.utils.Direction;
import com.bitknight.utils.Stat;
import com.bitknight.utils.StatMultipliers;
import com.bitknight.utils.pathfinding.IMover;

/**
 * Base enemy class implementation.
 * 
 * @author Jake Klassen Created: 10-22-2013
 * 
 */
public class Enemy implements ITarget, IMover, IDrawable {
  private HealthComponent hc = new HealthComponent(10);
  public Stat strength = new Stat(StatMultipliers.STRENGTH_MULTIPLIER);
  public Stat intelligence = new Stat(StatMultipliers.INTELLIGENCE_MULTIPLIER);
  public Stat dexterity = new Stat(StatMultipliers.DEXTERITY_MULTIPLIER);
  public Stat vitality = new Stat(StatMultipliers.VITALITY_MULTIPLIER);
  public int atk = 2;
  public int def = 1;

  private Map<AnimationKey, Animation> animations;
  private Animation currentAnimation;
  private Texture texture;
  private Vector2 position;
  private Vector2 scale;
  private Vector2 maxVelocity;
  private Vector2 velocity;
  private Vector2 destination;
  private float accumulator;
  // How long to wait before generating next random move
  private float timeToMove;
  // How close to the target point can the enemy be before
  // we snap to that location.
  private final float positionTolerance = 0.05f;
  //Start at something other than zero for first click.
	private float directionAngle = -1f;
	private String direction;
	private Player player = null;

  public Enemy() {
    this(new Vector2(0, 0));
  }

  public Enemy(Vector2 position) {
    animations = new HashMap<AnimationKey, Animation>();
    accumulator = 0;
    timeToMove = 2;
    maxVelocity = new Vector2(4, 4);
    velocity = new Vector2(0, 0);
    destination = new Vector2(position.x, position.y);
    scale = new Vector2(2, 2);
    this.position = position;
    texture = new Texture(Gdx.files.internal("data/img/native/goblin.png"));
    direction = "south";

    // Set stat properties
    strength.setValue(5);
    strength.setgrowthMultiplier(StatMultipliers.STRENGTH_MULTIPLIER);
    strength.setCurrentGoal(50);
    strength.setPreviousGoal(0);
    
    intelligence.setValue(5);
    intelligence.setgrowthMultiplier(StatMultipliers.INTELLIGENCE_MULTIPLIER);
    intelligence.setCurrentGoal(50);
    intelligence.setPreviousGoal(0);
    
    dexterity.setValue(5);
    dexterity.setgrowthMultiplier(StatMultipliers.DEXTERITY_MULTIPLIER);
    dexterity.setCurrentGoal(50);
    dexterity.setPreviousGoal(0);
    
    vitality.setValue(5);
    vitality.setgrowthMultiplier(StatMultipliers.VITALITY_MULTIPLIER);
    vitality.setCurrentGoal(50);
    vitality.setPreviousGoal(0);
    
    // Setup animations
    initializeAnimations();
    setCurrentAnimation(AnimationKey.IDLE_SOUTH);
    
    Game.entities.add(this);
  }

  public void dispose() {
    texture.dispose();
  }

  public Vector2 getPosition() {
    return position;
  }

  public EntityType getType() {
    return EntityType.ENEMY;
  }

  public void setPosition(Vector2 position) {
    this.position = position;
  }

  public void setPosition(float x, float y) {
    position.x = x;
    position.y = y;
  }
  
  public String getDirection() {
    return direction;
  }

  public void setDirection(String direction) {
    this.direction = direction;
  }

  /**
   * Get rectangle 1x1 relative to the enemy
   * 
   * @return Rectangle
   */
  @Override
  public Rectangle getHitBox() {
    return new Rectangle(position.x, position.y, 1, 1);
  }

  public void setCurrentAnimation(AnimationKey key) {
    currentAnimation = animations.get(key);
  }

  public void addAnimation(AnimationKey key, Animation animation) {
    animations.put(key, animation);
  }

  public boolean isAlive() {
    return hc.isAlive();
  }

  public boolean isDead() {
    return hc.isDead();
  }

  public void hit(int amount, Player who) {
  	player = who;
    hc.takeDamage(amount);
    
    Vector3 posProjected = new Vector3(position.x + 0.25f, position.y + 2, 0);
    GameScreen.camera.project(posProjected);
    Game.getTweenBuilder().messageTween("-" + String.valueOf(amount), 
        posProjected.x, posProjected.y, posProjected.x, posProjected.y + 25, 
        1f, new Color(1, 0, 0, 1), new Color(1, 0, 0, 1), 1f);
    
    if(hc.isDead()) {
      currentAnimation = animations.get(AnimationKey.DEATH);
      SoundManager.getInstance().play("kill1.ogg", false, AudioResourceType.SFX);
    }
  }
  
  /**
   * Used to look in a specific direction.
   * 
   * @param target Where to look
   */
  private void lookAt(Vector2 target) {
    Vector2 dstVector = new Vector2(target.x, target.y);
    Vector2 posVector = new Vector2(position.x, position.y);
    posVector.sub(dstVector);

    if (posVector.angle() != directionAngle) {
      directionAngle = posVector.angle();

      // Determine which direction player is facing so we can determine which
      // sprite we should use when at rest.
      if (posVector.angle() <= Direction.SOUTH_WEST
          || posVector.angle() >= Direction.NORTH_WEST) {
        direction = "west";
      } else if (posVector.angle() > Direction.SOUTH_WEST
          && posVector.angle() < Direction.SOUTH_EAST) {
        direction = "south";
      } else if (posVector.angle() >= Direction.SOUTH_EAST
          && posVector.angle() <= Direction.NORTH_EAST) {
        direction = "east";
      } else {
        direction = "north";
      }
    }
  }
  
  /**
   * If the player is not moving, determine their idle animation.
   */
  private void setIdle() {
    if (velocity != Vector2.Zero)
      return;

    // No other nodes to go to
    // Which direction should we idle in?
    if (direction.equalsIgnoreCase("north")) {
      setCurrentAnimation(AnimationKey.IDLE_NORTH);
    } else if (direction.equalsIgnoreCase("east")) {
      setCurrentAnimation(AnimationKey.IDLE_EAST);
    } else if (direction.equalsIgnoreCase("south")) {
      setCurrentAnimation(AnimationKey.IDLE_SOUTH);
    } else {
      setCurrentAnimation(AnimationKey.IDLE_WEST);
    }
  }
  
  private void attack() {
    if(player == null)
      return;
    
    if (direction.equalsIgnoreCase("north")) {
      if(currentAnimation != animations.get(AnimationKey.ATTACK_NORTH))
        setCurrentAnimation(AnimationKey.ATTACK_NORTH);
    } else if (direction.equalsIgnoreCase("east")) {
      if(currentAnimation != animations.get(AnimationKey.ATTACK_EAST))
        setCurrentAnimation(AnimationKey.ATTACK_EAST);
    } else if (direction.equalsIgnoreCase("south")) {
      if(currentAnimation != animations.get(AnimationKey.ATTACK_SOUTH))
        setCurrentAnimation(AnimationKey.ATTACK_SOUTH);
    } else {
      if(currentAnimation != animations.get(AnimationKey.ATTACK_WEST))
        setCurrentAnimation(AnimationKey.ATTACK_WEST);
    }
  }

  /**
   * Setup enemy animations.
   */
  private void initializeAnimations() {
    texture = new Texture(Gdx.files.internal("data/img/native/goblin.png"));
    Array<TextureRegion> regions = new Array<TextureRegion>();
    // Idle South
    regions = new Array<TextureRegion>();
    regions.add(new TextureRegion(texture, 0, 208, 26, 26));
    regions.add(new TextureRegion(texture, 26, 208, 26, 26));
    addAnimation(AnimationKey.IDLE_SOUTH, new Animation(1 / 2f, regions,
        new int[] { 0, 1 }, position, scale, true));

    // Idle North
    regions = new Array<TextureRegion>();
    regions.add(new TextureRegion(texture, 0, 130, 26, 26));
    regions.add(new TextureRegion(texture, 26, 130, 26, 26));
    addAnimation(AnimationKey.IDLE_NORTH, new Animation(1 / 2f, regions,
        new int[] { 0, 1 }, position, scale, true));

    // Idle East
    regions = new Array<TextureRegion>();
    regions.add(new TextureRegion(texture, 0, 52, 26, 26));
    regions.add(new TextureRegion(texture, 26, 52, 26, 26));
    addAnimation(AnimationKey.IDLE_EAST, new Animation(1 / 2f, regions,
        new int[] { 0, 1 }, position, scale, true));

    // Idle West
    regions = new Array<TextureRegion>();
    regions.add(new TextureRegion(texture, 0, 52, 26, 26));
    regions.get(0).flip(true, false);
    regions.add(new TextureRegion(texture, 26, 52, 26, 26));
    regions.get(1).flip(true, false);
    addAnimation(AnimationKey.IDLE_WEST, new Animation(1 / 2f, regions,
        new int[] { 0, 1 }, position, scale, true));

    // East
    regions = new Array<TextureRegion>();
    regions.add(new TextureRegion(texture, 0, 26, 26, 26));
    regions.add(new TextureRegion(texture, 26, 26, 26, 26));
    regions.add(new TextureRegion(texture, 52, 26, 26, 26));
    addAnimation(AnimationKey.EAST, new Animation(1 / 8f, regions,
        new int[] { 0, 1, 2 }, position, scale, true));

    // West
    regions = new Array<TextureRegion>();
    regions.add(new TextureRegion(texture, 0, 26, 26, 26));
    regions.add(new TextureRegion(texture, 26, 26, 26, 26));
    regions.add(new TextureRegion(texture, 52, 26, 26, 26));
    regions.get(0).flip(true, false);
    regions.get(1).flip(true, false);
    regions.get(2).flip(true, false);
    addAnimation(AnimationKey.WEST, new Animation(1 / 8f, regions,
        new int[] { 0, 1, 2 }, position, scale, true));

    // South
    regions = new Array<TextureRegion>();
    regions.add(new TextureRegion(texture, 0, 182, 26, 26));
    regions.add(new TextureRegion(texture, 26, 182, 26, 26));
    regions.add(new TextureRegion(texture, 52, 182, 26, 26));
    regions.add(new TextureRegion(texture, 88, 182, 26, 26));
    addAnimation(AnimationKey.SOUTH, new Animation(1 / 8f, regions,
        new int[] { 0, 1, 2, 3 }, position, scale, true));

    // North
    regions = new Array<TextureRegion>();
    regions.add(new TextureRegion(texture, 0, 104, 26, 26));
    regions.add(new TextureRegion(texture, 26, 104, 26, 26));
    regions.add(new TextureRegion(texture, 52, 104, 26, 26));
    regions.add(new TextureRegion(texture, 88, 104, 26, 26));
    addAnimation(AnimationKey.NORTH, new Animation(1 / 8f, regions,
        new int[] { 0, 1, 2, 3 }, position, scale, true));

    // Attack North
    regions = new Array<TextureRegion>();
    regions.add(new TextureRegion(texture, 0, 78, 26, 26));
    regions.add(new TextureRegion(texture, 26, 78, 26, 26));
    regions.add(new TextureRegion(texture, 52, 78, 26, 26));
    addAnimation(AnimationKey.ATTACK_NORTH, new Animation(1 / 5f, regions,
        new int[] { 0, 1, 2 }, position, scale, true));

    // Attack East
    regions = new Array<TextureRegion>();
    regions.add(new TextureRegion(texture, 0, 0, 26, 26));
    regions.add(new TextureRegion(texture, 26, 0, 26, 26));
    regions.add(new TextureRegion(texture, 52, 0, 26, 26));
    addAnimation(AnimationKey.ATTACK_EAST, new Animation(1 / 5f, regions,
        new int[] { 0, 1, 2 }, position, scale, true));

    // Attack South
    regions = new Array<TextureRegion>();
    regions.add(new TextureRegion(texture, 0, 156, 26, 26));
    regions.add(new TextureRegion(texture, 26, 156, 26, 26));
    regions.add(new TextureRegion(texture, 52, 156, 26, 26));
    addAnimation(AnimationKey.ATTACK_SOUTH, new Animation(1 / 5f, regions,
        new int[] { 0, 1, 2 }, position, scale, true));

    // Attack West
    regions = new Array<TextureRegion>();
    regions.add(new TextureRegion(texture, 0, 0, 26, 26));
    regions.add(new TextureRegion(texture, 26, 0, 26, 26));
    regions.add(new TextureRegion(texture, 52, 0, 26, 26));
    regions.get(0).flip(true, false);
    regions.get(1).flip(true, false);
    regions.get(2).flip(true, false);
    addAnimation(AnimationKey.ATTACK_WEST, new Animation(1 / 5f, regions,
        new int[] { 0, 1, 2 }, position, scale, true));

    // Death
    texture = new Texture(Gdx.files.internal("data/img/native/death.png"));
    regions = new Array<TextureRegion>();
    regions.add(new TextureRegion(texture, 0, 0, 24, 24));
    regions.add(new TextureRegion(texture, 24, 0, 24, 24));
    regions.add(new TextureRegion(texture, 48, 0, 24, 24));
    regions.add(new TextureRegion(texture, 72, 0, 24, 24));
    regions.add(new TextureRegion(texture, 96, 0, 24, 24));
    regions.add(new TextureRegion(texture, 120, 0, 24, 24));
    addAnimation(AnimationKey.DEATH, new Animation(1 / 8f, regions, 
        new int[] { 0, 1, 2, 3, 4, 5 }, position, scale, false));
  }

  private static boolean hit = false;
  public void update(float dt) {
  	if (isAlive() && player != null) {
  		lookAt(player.getPosition());
  		if (position.dst(player.getPosition()) <= 1) {
  			if(player.isAlive()) {
          attack();
          if(currentAnimation.getProgress() >= 0.5f) {
            if(!hit) {
            	// Calculate the damage to the player
            	int damage = (int)(atk + (strength.getValue() / 2)) - player.def;
              player.hit(damage, this);
              SoundManager.getInstance().play(
              		"hurt.ogg", 
              		false, 
              		AudioResourceType.SFX
              	);
            }
            hit = true;
          }
          else {
            hit = false;
          }
        }
        else {
          player = null;
          // Reset the attack animation
          currentAnimation.setCurrentIdx(0);
        }
  		}
  		else {
  			setIdle();
  		}
  	}
  	else {
  		if (player == null)
  			setIdle();
  	}
  	
    accumulator += dt;
    // Generate new destination
    if (accumulator >= timeToMove) {
      accumulator = 0;
      destination.x = position.x;
      // setCurrentAnimation(AnimationKey.DEATH);
    }

    // Are we close enough to snap position to destination?
    if (Math.abs(destination.cpy().sub(position).len()) <= positionTolerance * 2) {
      position.x = destination.x;
      position.y = destination.y;

      velocity = Vector2.Zero;
      //setCurrentAnimation(AnimationKey.IDLE_WEST);
    } else {
      // Move the player
      velocity = destination.cpy().sub(position).nor().scl(maxVelocity);
      position.add(velocity.cpy().scl(dt));
    }

    // Update animation
    currentAnimation.setPos(new Vector2(position.x - 0.5f, position.y));
    currentAnimation.update(dt);
    
    if(isDead() && currentAnimation.getProgress() >= 1) {
      dispose();
      Game.entities.remove(this);
    }
  }

  public void render(SpriteBatch spriteBatch) {
    currentAnimation.render(spriteBatch);
  }
}
