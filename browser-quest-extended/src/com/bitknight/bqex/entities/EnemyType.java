package com.bitknight.bqex.entities;

/**
 * Used to differentiate enemies
 * 
 * @author Jake Klassen
 * 
 */
public enum EnemyType {
  GOBLIN(0), OGRE(1), BAT(2);

  private final int value;

  EnemyType(int value) {
    this.value = value;
  }

  public int getValue() {
    return value;
  }
}
