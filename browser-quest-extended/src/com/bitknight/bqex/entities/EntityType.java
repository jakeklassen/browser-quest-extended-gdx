package com.bitknight.bqex.entities;

/**
 * Container to differentiate entities;
 * 
 * @author Jake Klassen
 * 
 */
public enum EntityType {
  PLAYER(0), NPC(1), ENEMY(2), LOOT(3);

  private final int value;

  EntityType(int value) {
    this.value = value;
  }

  public int getValue() {
    return value;
  }
}
