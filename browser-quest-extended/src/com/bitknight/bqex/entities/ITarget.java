package com.bitknight.bqex.entities;

import com.badlogic.gdx.math.Vector2;

/**
 * Tagging interface to working with player touch/click and hover
 * on the desktop.
 * 
 * @author Jake Klassen
 *
 */
public interface ITarget {
  public Vector2 getPosition();
  public void setPosition(Vector2 position);
  public EntityType getType();
}
