package com.bitknight.bqex.entities;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.bitknight.bqex.Game;
import com.bitknight.bqex.items.Loot;
import com.bitknight.bqex.items.Weapon;
import com.bitknight.bqex.screens.GameScreen;
import com.bitknight.bqex.utils.AnimationKey;
import com.bitknight.components.HealthComponent;
import com.bitknight.graphics.Animation;
import com.bitknight.sound.AudioResourceType;
import com.bitknight.sound.SoundManager;
import com.bitknight.utils.Direction;
import com.bitknight.utils.Stat;
import com.bitknight.utils.StatMultipliers;
import com.bitknight.utils.pathfinding.IMover;
import com.bitknight.utils.pathfinding.Path;

/**
 * Player implementation.
 * TODO - Needs refactoring.
 * 
 * @author Jake Klassen
 * 
 */
public class Player implements IMover, IDrawable {
  public static enum Gender { MALE, FEMALE };
  
  // Identifies player for saving/loading
  private int id;
  
  private HealthComponent hc = new HealthComponent(15);
  public Stat strength = new Stat(StatMultipliers.STRENGTH_MULTIPLIER);
  public Stat intelligence = new Stat(StatMultipliers.INTELLIGENCE_MULTIPLIER);
  public Stat dexterity = new Stat(StatMultipliers.DEXTERITY_MULTIPLIER);
  public Stat vitality = new Stat(StatMultipliers.VITALITY_MULTIPLIER);
  public int def = 1;

  // Named animations
  private Map<AnimationKey, Animation> animations;
  private Animation currentAnimation;
  private Texture texture;

  // Path to follow
  private Path path;
  // Where we are in the path
  private int step;

  // How close to the target point can the player be before
  // we snap to that location.
  //TODO Find a better way to do this, not great at low FPS.
  private final float positionTolerance = 0.05f;

  // Offset used in x and y given a tile cell position.
  // These offsets are only used in the rendering of the player.
  private final float xOffset = 0.5f;
  private final float yOffset = 0.2f;
  private Vector2 position;
  private Vector2 velocity;
  private Vector2 maxVelocity;
  private Vector2 destination;
  private Vector2 scale;
  private Vector2 spawnPoint;
  // Start at something other than zero for first click.
  private float directionAngle = -1f;
  private String direction;

  // Used for tracking
  private ITarget target;
  // Used to avoid repeated casting
  private Enemy targetEnemy = null;
  private Random random = new Random();

  private Gender gender;
  private String name;
  
  // Weapon
  private Weapon currentWeapon = null;
  
  // C'tor
  public Player(Gender gender) {
    this.gender = gender;
    animations = new HashMap<AnimationKey, Animation>();
    position = new Vector2();
    destination = new Vector2();
    velocity = new Vector2(0, 0);
    maxVelocity = new Vector2(5, 5);
    path = null;
    step = 0;
    target = null;
    scale = new Vector2(1, 1);
    spawnPoint = new Vector2(23, 10);
    
    hc = new HealthComponent(13);
    initUI();
    
    // Setup stats
    strength.setValue(5);
    strength.setCurrentGoal(50);
    strength.setPreviousGoal(0);
    strength.setShortName("STR");
    strength.setFullName("Strength");
    strength.setOwner(this);
    
    intelligence.setValue(5);
    intelligence.setCurrentGoal(50);
    intelligence.setPreviousGoal(0);
    intelligence.setShortName("INT");
    intelligence.setFullName("Intelligence");
    intelligence.setOwner(this);
    
    dexterity.setValue(5);
    dexterity.setCurrentGoal(50);
    dexterity.setPreviousGoal(0);
    dexterity.setShortName("DEX");
    dexterity.setFullName("Dexterity");
    dexterity.setOwner(this);
    
    vitality.setValue(5);
    vitality.setCurrentGoal(50);
    vitality.setPreviousGoal(0);
    vitality.setShortName("VIT");
    vitality.setFullName("Vitality");
    vitality.setOwner(this);
    
    if(this.gender==Gender.MALE)
    	initializeAnimations("data/img/native/clotharmor.png");
    else
    	initializeAnimations("data/img/native/clotharmor_female.png");
    
    setCurrentAnimation(AnimationKey.SOUTH);
    setWeapon(new Weapon("sword1.png", 3, position.cpy()));
    
    // Init ui
    Game.getUI().setMaxHealth(hc.getMaxHP());
    
    Game.entities.add(this);
  }

  public Player(Gender gender, String armor, String weapon, 
  		Integer str, Integer strCurGoal, Integer strPrevGoal, Integer strProgress, 
  		Integer intel, Integer intelCurGoal, Integer intelPrevGoal, Integer intelProgress,  
  		Integer dex, Integer dexCurGoal, Integer dexPrevGoal, Integer dexProgress, 
  		Integer vit, Integer vitCurGoal, Integer vitPrevGoal, Integer vitProgress, 
			Integer maxHP, Integer curHP) {
  	this.gender = gender;
    animations = new HashMap<AnimationKey, Animation>();
    position = new Vector2();
    destination = new Vector2();
    velocity = new Vector2(0, 0);
    maxVelocity = new Vector2(5, 5);
    path = null;
    step = 0;
    target = null;
    scale = new Vector2(1, 1);
    spawnPoint = new Vector2(23, 10);
    
    hc = new HealthComponent(maxHP);
    hc.takeDamage((maxHP-curHP));
    initUI();
    
    // Setup stats
    strength.setValue(str);
    strength.setCurrentGoal(strCurGoal);
    strength.setPreviousGoal(strPrevGoal);
    strength.setShortName("STR");
    strength.setFullName("Strength");
    strength.setOwner(this);
    
    intelligence.setValue(intel);
    intelligence.setCurrentGoal(intelCurGoal);
    intelligence.setPreviousGoal(intelPrevGoal);
    intelligence.setShortName("INT");
    intelligence.setFullName("Intelligence");
    intelligence.setOwner(this);
    
    dexterity.setValue(dex);
    dexterity.setCurrentGoal(dexCurGoal);
    dexterity.setPreviousGoal(dexPrevGoal);
    dexterity.setShortName("DEX");
    dexterity.setFullName("Dexterity");
    dexterity.setOwner(this);
    
    vitality.setValue(vit);
    vitality.setCurrentGoal(vitCurGoal);
    vitality.setPreviousGoal(vitPrevGoal);
    vitality.setShortName("VIT");
    vitality.setFullName("Vitality");
    vitality.setOwner(this);
    
    initializeAnimations(armor);
    setCurrentAnimation(AnimationKey.SOUTH);
    if (weapon.equalsIgnoreCase("darksword1.png"))
    	setWeapon(new Weapon(weapon, 5, position.cpy()));
    else
    	setWeapon(new Weapon(weapon, 3, position.cpy()));
    
    // Init ui
    Game.getUI().setMaxHealth(hc.getMaxHP());
    
    Game.entities.add(this);
	}

	public void dispose() {
  	texture.dispose();
  	if (currentWeapon != null)
  		currentWeapon.dispose();
  }

  public void setId(int id) {
  	this.id = id;
  }
  
  public int getId() {
  	return id;
  }
  
  public EntityType getType() {
    return EntityType.PLAYER;
  }

  public void setCurrentAnimation(AnimationKey key) {
  	if (currentAnimation != null)	
  		currentAnimation.reset();
    
  	currentAnimation = animations.get(key);
    
  	if (currentWeapon != null) {
    	currentWeapon.getAnimation().reset();
    	currentWeapon.setAnimation(key);
    }
  }

  public void addAnimation(AnimationKey key, Animation animation) {
    animations.put(key, animation);
  }

  public Vector2 getPosition() {
    return new Vector2(position.x, position.y);
  }

  public void setPosition(Vector2 position) {
    this.position = position;
  }

  public void setPosition(float x, float y) {
    position.x = x;
    position.y = y;
  }

  public String getDirection() {
    return direction;
  }

  public void setDirection(String direction) {
    this.direction = direction;
  }
  
  public void setScale(Vector2 scale) {
    this.scale = scale;
    for(Animation a : animations.values()) {
      a.setScale(scale);
    }
  }
  
  public Vector2 getScale() {
    return scale;
  }
  
  public void setSpawnPoint(Vector2 spawnPoint) {
  	this.spawnPoint = spawnPoint;
  }
  
  public void setSpawnPoint(float x, float y) {
  	if (spawnPoint != null) {
  		spawnPoint.x = x;
  		spawnPoint.y = y;
  	}
  	else {
  		spawnPoint = new Vector2(x, y);
  	}
  }
  
  public Vector2 getSpawnPoint() {
  	return spawnPoint;
  }
  
  public HealthComponent getHealthComponent() {
  	return hc;
  }
  
  public ITarget getTarget() {
    return target;
  }
  
  public void setTarget(ITarget target) {
    this.target = target;
    if (target == null) {
      targetEnemy = null;
    	return;
    }
    
    if (this.target.getType() == EntityType.ENEMY) {
    	targetEnemy = (Enemy) this.target;
    }
  }
  
  public Gender getGender() {
    return gender;
  }
  
  public void setGender(Gender gender) {
    this.gender = gender;
  }
  
  public String getName() {
    return name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public Weapon getWeapon() {
  	return currentWeapon;
  }
  
  @Override
  public Rectangle getHitBox() {
		return new Rectangle(position.x, position.y, 1, 1);
  }

  /**
   * Velocity as a directional array in terms of x and y.
   * 
   * @return Integer array of [-x/+x/0, -y/+y/0]
   */
  public int[] getVelocityAsDirection() {
    if (velocity == null || velocity == Vector2.Zero)
      return new int[] { 0, 0 };

    if (velocity.x == 0 && velocity.y > 0)
      return new int[] { 0, 1 };
    else if (velocity.x > 0 && velocity.y > 0)
      return new int[] { 1, 1 };
    else if (velocity.x > 0 && velocity.y == 0)
      return new int[] { 1, 0 };
    else if (velocity.x > 0 && velocity.y < 0)
      return new int[] { 1, -1 };
    else if (velocity.x == 0 && velocity.y < 0)
      return new int[] { 0, -1 };
    else if (velocity.x < 0 && velocity.y < 0)
      return new int[] { -1, -1 };
    else if (velocity.x < 0 && velocity.y == 0)
      return new int[] { -1, 0 };
    else
      return new int[] { -1, -1 };
  }

  public Vector2 getVelocity() {
    return velocity;
  }

  public void setVelocity(Vector2 velocity) {
    this.velocity = velocity;
  }

  public void setVelocity(float x, float y) {
    velocity.x = x;
    velocity.y = y;
  }

  public Vector2 getMaxVelocity() {
    return maxVelocity;
  }

  public void setMaxVelocity(Vector2 maxVelocity) {
    this.maxVelocity = maxVelocity;
  }

  public void setMaxVelocity(float x, float y) {
    maxVelocity.x = x;
    maxVelocity.y = y;
  }

  public Vector2 getDestination() {
    return destination;
  }

  public void setDestination(Vector2 destination) {
    this.destination = destination;
  }

  public void setDesination(float x, float y) {
    destination.x = x;
    destination.y = y;
  }

  public void setPath(Path path) {
    if (path == null || path.getLength() == 1)
      return;

    this.path = path;
    step = 1;
    setDesination(path.getStep(step).getX(), path.getStep(step).getY());
    updateDirection();
  }
  
  public Path getPath() {
    return path;
  }

  /**
   * Update the current animation while the player is moving.
   */
  private void updateCurrentWalkAnimation() {
    // Walk animation to match the direction player is facing
    if (direction.equalsIgnoreCase("north")
        && currentAnimation != animations.get(AnimationKey.NORTH)) {
      setCurrentAnimation(AnimationKey.NORTH);
      currentWeapon.setAnimation(AnimationKey.NORTH);
    } else if (direction.equalsIgnoreCase("east")
        && currentAnimation != animations.get(AnimationKey.EAST)) {
      setCurrentAnimation(AnimationKey.EAST);
      currentWeapon.setAnimation(AnimationKey.EAST);
    } else if (direction.equalsIgnoreCase("south")
        && currentAnimation != animations.get(AnimationKey.SOUTH)) {
      setCurrentAnimation(AnimationKey.SOUTH);
      currentWeapon.setAnimation(AnimationKey.SOUTH);
    } else if (direction.equalsIgnoreCase("west")
        && currentAnimation != animations.get(AnimationKey.WEST)) {
      setCurrentAnimation(AnimationKey.WEST);
      currentWeapon.setAnimation(AnimationKey.WEST);
    }
  }

  /**
   * Based on the players position and destination determine the direction they
   * are facing.
   */
  private void updateDirection() {
    Vector2 dstVector = new Vector2(destination.x, destination.y);
    Vector2 posVector = new Vector2(position.x, position.y);
    posVector.sub(dstVector);

    if (posVector.angle() != directionAngle) {
      directionAngle = posVector.angle();

      // Determine which direction player is facing so we can determine which
      // sprite we should use when at rest.
      if (posVector.angle() <= Direction.SOUTH_WEST
          || posVector.angle() >= Direction.NORTH_WEST) {
        direction = "west";
      } else if (posVector.angle() > Direction.SOUTH_WEST
          && posVector.angle() < Direction.SOUTH_EAST) {
        direction = "south";
      } else if (posVector.angle() >= Direction.SOUTH_EAST
          && posVector.angle() <= Direction.NORTH_EAST) {
        direction = "east";
      } else {
        direction = "north";
      }
    }

    updateCurrentWalkAnimation();
  }
  
  /**
   * Used to look in a specific direction.
   * 
   * @param target Where to look
   */
  private void lookAt(Vector2 target) {
    Vector2 dstVector = new Vector2(target.x, target.y);
    Vector2 posVector = new Vector2(position.x, position.y);
    posVector.sub(dstVector);

    if (posVector.angle() != directionAngle) {
      directionAngle = posVector.angle();

      // Determine which direction player is facing so we can determine which
      // sprite we should use when at rest.
      if (posVector.angle() <= Direction.SOUTH_WEST
          || posVector.angle() >= Direction.NORTH_WEST) {
        direction = "west";
      } else if (posVector.angle() > Direction.SOUTH_WEST
          && posVector.angle() < Direction.SOUTH_EAST) {
        direction = "south";
      } else if (posVector.angle() >= Direction.SOUTH_EAST
          && posVector.angle() <= Direction.NORTH_EAST) {
        direction = "east";
      } else {
        direction = "north";
      }
    }
  }

  /**
   * If the player is not moving, determine their idle animation.
   */
  private void setIdle() {
    if (velocity != Vector2.Zero)
      return;

    // No other nodes to go to
    // Which direction should we idle in?
    if (direction.equalsIgnoreCase("north")) {
    	if (currentAnimation != animations.get(AnimationKey.IDLE_NORTH)) {
	      setCurrentAnimation(AnimationKey.IDLE_NORTH);
	      currentWeapon.setAnimation(AnimationKey.IDLE_NORTH);
    	}
    } else if (direction.equalsIgnoreCase("east")) {
    	if (currentAnimation != animations.get(AnimationKey.IDLE_EAST)) {
	      setCurrentAnimation(AnimationKey.IDLE_EAST);
	      currentWeapon.setAnimation(AnimationKey.IDLE_EAST);
    	}
    } else if (direction.equalsIgnoreCase("south")) {
    	if (currentAnimation != animations.get(AnimationKey.IDLE_SOUTH)) {
	      setCurrentAnimation(AnimationKey.IDLE_SOUTH);
	      currentWeapon.setAnimation(AnimationKey.IDLE_SOUTH);
    	}
    } else {
    	if (currentAnimation != animations.get(AnimationKey.IDLE_WEST)) {
	      setCurrentAnimation(AnimationKey.IDLE_WEST);
	      currentWeapon.setAnimation(AnimationKey.IDLE_WEST);
    	}
    }
  }
  
  private void attack() {
    if(target == null || targetEnemy == null || target.getType() != EntityType.ENEMY)
      return;
    
    if (direction.equalsIgnoreCase("north")) {
      if(currentAnimation != animations.get(AnimationKey.ATTACK_NORTH))
        setCurrentAnimation(AnimationKey.ATTACK_NORTH);
      currentWeapon.setAnimation(AnimationKey.ATTACK_NORTH);
    } else if (direction.equalsIgnoreCase("east")) {
      if(currentAnimation != animations.get(AnimationKey.ATTACK_EAST))
        setCurrentAnimation(AnimationKey.ATTACK_EAST);
      currentWeapon.setAnimation(AnimationKey.ATTACK_EAST);
    } else if (direction.equalsIgnoreCase("south")) {
      if(currentAnimation != animations.get(AnimationKey.ATTACK_SOUTH))
        setCurrentAnimation(AnimationKey.ATTACK_SOUTH);
      currentWeapon.setAnimation(AnimationKey.ATTACK_SOUTH);
    } else {
      if(currentAnimation != animations.get(AnimationKey.ATTACK_WEST))
        setCurrentAnimation(AnimationKey.ATTACK_WEST);
      currentWeapon.setAnimation(AnimationKey.ATTACK_WEST);
    }
  }
  
  public boolean isAlive() {
    return hc.isAlive();
  }

  public boolean isDead() {
    return hc.isDead();
  }

  public void hit(int amount, Enemy who) {
  	targetEnemy = who;
    hc.takeDamage(amount);
    
    Game.getUI().setHealth(hc.getCurrentHP());
    
    if(vitality.updateProgress(amount)) {
    	vitality.levelUp(1);
    	def++;
    	hc = new HealthComponent(hc.getMaxHP() + 1);
    	Game.getUI().setMaxHealth(hc.getMaxHP());
    }
    
    
    Vector3 posProjected = new Vector3(position.x + 0.25f, position.y + 2, 0);
    GameScreen.camera.project(posProjected);
    Game.getTweenBuilder().messageTween("-" + String.valueOf(amount), 
        posProjected.x, posProjected.y, posProjected.x, posProjected.y + 25, 
        1f, new Color(1, 0, 0, 1), new Color(1, 0, 0, 1), 1f);
    
    if(hc.isDead()) {
      currentWeapon.stopRendering();
    	currentAnimation.reset();
      currentAnimation = animations.get(AnimationKey.DEATH);
      SoundManager.getInstance().play("death.ogg", false, AudioResourceType.SFX);
    }
  }
  
  // Set player weapon
  public void setWeapon(Weapon weapon) {
  	if (currentWeapon != null)
  		currentWeapon.dispose();
  	currentWeapon = weapon;
  	
  	// Sync animation keys
  	AnimationKey k = null;
  	for (Entry<AnimationKey, Animation> entry : animations.entrySet()) {
      if (currentAnimation.equals(entry.getValue())) {
        k = entry.getKey();
      }
	  }
  	
  	currentWeapon.setAnimation(k);
  }

  /**
   * Setup player animations.
   */
  private void initializeAnimations(String armor) {

    texture = new Texture(Gdx.files.internal(armor));
    Array<TextureRegion> regions = new Array<TextureRegion>();
    // Idle South
    regions = new Array<TextureRegion>();
    regions.add(new TextureRegion(texture, 0, 256, 32, 32));
    regions.add(new TextureRegion(texture, 32, 256, 32, 32));
    addAnimation(AnimationKey.IDLE_SOUTH, new Animation(1 / 2f, regions,
        new int[] { 0, 1 }, position, scale, true));

    // Idle North
    regions = new Array<TextureRegion>();
    regions.add(new TextureRegion(texture, 0, 160, 32, 32));
    regions.add(new TextureRegion(texture, 32, 160, 32, 32));
    addAnimation(AnimationKey.IDLE_NORTH, new Animation(1 / 2f, regions,
        new int[] { 0, 1 }, position, scale, true));

    // Idle East
    regions = new Array<TextureRegion>();
    regions.add(new TextureRegion(texture, 0, 64, 32, 32));
    regions.add(new TextureRegion(texture, 32, 64, 32, 32));
    addAnimation(AnimationKey.IDLE_EAST, new Animation(1 / 2f, regions,
        new int[] { 0, 1 }, position, scale, true));

    // Idle West
    regions = new Array<TextureRegion>();
    regions.add(new TextureRegion(texture, 0, 64, 32, 32));
    regions.get(0).flip(true, false);
    regions.add(new TextureRegion(texture, 32, 64, 32, 32));
    regions.get(1).flip(true, false);
    addAnimation(AnimationKey.IDLE_WEST, new Animation(1 / 2f, regions,
        new int[] { 0, 1 }, position, scale, true));

    // East
    regions = new Array<TextureRegion>();
    regions.add(new TextureRegion(texture, 0, 32, 32, 32));
    regions.add(new TextureRegion(texture, 32, 32, 32, 32));
    regions.add(new TextureRegion(texture, 64, 32, 32, 32));
    regions.add(new TextureRegion(texture, 96, 32, 32, 32));
    addAnimation(AnimationKey.EAST, new Animation(1 / 8f, regions,
        new int[] { 0, 1, 2, 3 }, position, scale, true));

    // West
    regions = new Array<TextureRegion>();
    regions.add(new TextureRegion(texture, 0, 32, 32, 32));
    regions.get(0).flip(true, false);
    regions.add(new TextureRegion(texture, 32, 32, 32, 32));
    regions.get(1).flip(true, false);
    regions.add(new TextureRegion(texture, 64, 32, 32, 32));
    regions.get(2).flip(true, false);
    regions.add(new TextureRegion(texture, 96, 32, 32, 32));
    regions.get(3).flip(true, false);
    addAnimation(AnimationKey.WEST, new Animation(1 / 8f, regions,
        new int[] { 0, 1, 2, 3 }, position, scale, true));

    // South
    regions = new Array<TextureRegion>();
    regions.add(new TextureRegion(texture, 0, 224, 32, 32));
    regions.add(new TextureRegion(texture, 32, 224, 32, 32));
    regions.add(new TextureRegion(texture, 64, 224, 32, 32));
    regions.add(new TextureRegion(texture, 96, 224, 32, 32));
    addAnimation(AnimationKey.SOUTH, new Animation(1 / 8f, regions,
        new int[] { 0, 1, 2, 3 }, position, scale, true));

    // North
    regions = new Array<TextureRegion>();
    regions.add(new TextureRegion(texture, 0, 128, 32, 32));
    regions.add(new TextureRegion(texture, 32, 128, 32, 32));
    regions.add(new TextureRegion(texture, 64, 128, 32, 32));
    regions.add(new TextureRegion(texture, 96, 128, 32, 32));
    addAnimation(AnimationKey.NORTH, new Animation(1 / 8f, regions,
        new int[] { 0, 1, 2, 3 }, position, scale, true));
    
    // Attack North
    regions = new Array<TextureRegion>();
    regions.add(new TextureRegion(texture, 0, 96, 32, 32));
    regions.add(new TextureRegion(texture, 32, 96, 32, 32));
    regions.add(new TextureRegion(texture, 65, 96, 32, 32));
    regions.add(new TextureRegion(texture, 96, 96, 32, 32));
    regions.add(new TextureRegion(texture, 128, 96, 32, 32));
    addAnimation(AnimationKey.ATTACK_NORTH, new Animation(1 / 8f, regions,
        new int[] { 0, 1, 2, 3, 4 }, position, scale, true));
    
    // Attack East
    regions = new Array<TextureRegion>();
    regions.add(new TextureRegion(texture, 0, 0, 32, 32));
    regions.add(new TextureRegion(texture, 32, 0, 32, 32));
    regions.add(new TextureRegion(texture, 64, 0, 32, 32));
    regions.add(new TextureRegion(texture, 96, 0, 32, 32));
    regions.add(new TextureRegion(texture, 128, 0, 32, 32));
    addAnimation(AnimationKey.ATTACK_EAST, new Animation(1 / 8f, regions,
        new int[] { 0, 1, 2, 3, 4 }, position, scale, true));
    
    // Attack South
    regions = new Array<TextureRegion>();
    regions.add(new TextureRegion(texture, 0, 192, 32, 32));
    regions.add(new TextureRegion(texture, 32, 192, 32, 32));
    regions.add(new TextureRegion(texture, 65, 192, 32, 32));
    regions.add(new TextureRegion(texture, 96, 192, 32, 32));
    regions.add(new TextureRegion(texture, 128, 192, 32, 32));
    addAnimation(AnimationKey.ATTACK_SOUTH, new Animation(1 / 8f, regions,
        new int[] { 0, 1, 2, 3, 4 }, position, scale, true));
    
    // Attack West
    regions = new Array<TextureRegion>();
    regions.add(new TextureRegion(texture, 0, 0, 32, 32));
    regions.add(new TextureRegion(texture, 32, 0, 32, 32));
    regions.add(new TextureRegion(texture, 64, 0, 32, 32));
    regions.add(new TextureRegion(texture, 96, 0, 32, 32));
    regions.add(new TextureRegion(texture, 128, 0, 32, 32));
    regions.get(0).flip(true, false);
    regions.get(1).flip(true, false);
    regions.get(2).flip(true, false);
    regions.get(3).flip(true, false);
    regions.get(4).flip(true, false);
    addAnimation(AnimationKey.ATTACK_WEST, new Animation(1 / 8f, regions,
        new int[] { 0, 1, 2, 3, 4 }, position, scale, true));
    
    // Death
    texture = new Texture(Gdx.files.internal("data/img/native/death.png"));
		regions = new Array<TextureRegion>();
		regions.add(new TextureRegion(texture, 0, 0, 24, 24));
		regions.add(new TextureRegion(texture, 24, 0, 24, 24));
		regions.add(new TextureRegion(texture, 48, 0, 24, 24));
		regions.add(new TextureRegion(texture, 72, 0, 24, 24));
		regions.add(new TextureRegion(texture, 96, 0, 24, 24));
		regions.add(new TextureRegion(texture, 120, 0, 24, 24));
		addAnimation(AnimationKey.DEATH, new Animation(1 / 8f, regions, 
				new int[] { 0, 1, 2, 3, 4, 5 }, position, scale, false));
  }

	//TODO refactor
	public void initUI() {
	 	// Init the UI health
	  Game.getUI().setHealth(hc.getCurrentHP());
	  Game.getUI().setMaxHealth(hc.getMaxHP());
	}
  
  private static boolean hit = false;
  public void update(float dt) {
  	if (isAlive()) {
	    // Move player if we have not reached the destination
	    if (path != null && step != path.getLength()) {
	      // Are we close enough to snap position to destination?
	      if (Math.abs(destination.cpy().sub(position).len()) <= positionTolerance * 2) {
	        position.x = destination.x;
	        position.y = destination.y;
	
	        // Any more nodes?
	        if (++step < path.getLength()) {
	          setDesination(path.getStep(step).getX(), path.getStep(step).getY());
	          updateDirection();
	        } else {
	          // Reached our destination
	          velocity = Vector2.Zero;
	          setIdle();
	          
	          if (target != null && target.getType() == EntityType.LOOT) {
		        	// Reached item
		        	Loot loot = (Loot) target;
		        	Vector3 posProjected = new Vector3(position.x + 0.25f, position.y + 2, 0);
		          GameScreen.camera.project(posProjected);
		          Game.getTweenBuilder().messageTween("You picked up " + loot.getItemName() + "!", 
		              posProjected.x, posProjected.y, posProjected.x, posProjected.y + 25, 
		              1f, new Color(1, 1, 1, 1), new Color(1, 1, 1, 1), 1f);
		          
		          // Pickup goods
		          switch (loot.getItem().getType()) {
		          case WEAPON:
		          	setWeapon((Weapon)loot.getItem());
		          	break;
		          case ARMOR:
		          	break;
		          }
		          
		          SoundManager.getInstance().play("loot.ogg", false, AudioResourceType.SFX);
		          Game.entities.remove(loot);
		          
		          target = null;
		        }
	        }
	      } else {
	        // Move the player
	        velocity = destination.cpy().sub(position).nor().scl(maxVelocity);
	        position.add(velocity.cpy().scl(dt));
	      }
	    } else {
	    	// No path
	      path = null;
	      
	      // Look at target if there is one
	      if(target != null) {
	        lookAt(target.getPosition());
	        // If it's an enemy attack
	        if(target.getType() == EntityType.ENEMY && targetEnemy != null) {
	          if(targetEnemy.isAlive()) {
	            attack();
	            if(currentAnimation.getProgress() >= 0.5f) {
	              if(!hit) {
	              	// Calculate damage and hit enemy
	              	int damage = (int)Math.ceil(currentWeapon.getAttack() + (strength.getValue() / 2) - targetEnemy.def);
	              	
	                targetEnemy.hit(damage, this);
	                
	                // Update strength growth progress with the damage dealt
	                if(strength.updateProgress(damage)) {
	                	strength.levelUp(1);
	                	hc = new HealthComponent(hc.getMaxHP() + 1);
	                }
	                
	                int num = random.nextInt(2);
	                SoundManager.getInstance().play(
	                		num == 0 ? "hit1.ogg" : "hit2.ogg", 
	                		false, 
	                		AudioResourceType.SFX
	                	);
	              }
	              hit = true;
	            }
	            else {
	              hit = false;
	            }
	          }
	          else {
	            target = null;
	            targetEnemy = null;
	            // Reset the attack animation
	            currentAnimation.setCurrentIdx(0);
	            if (currentWeapon != null)
	            	currentWeapon.getAnimation().setCurrentIdx(0);
	          }
	        }
	      }
	      else {
	        // Idle animation
	        setIdle();
	      }
	    }
  	}

    // Give position with offsets to render the player centered
    currentAnimation.setPos(new Vector2(position.x - xOffset, position.y
        - yOffset));
    currentAnimation.update(dt);
    
    currentWeapon.setPosition(position.cpy().add(-currentWeapon.getXOffset(),  0.55f - currentWeapon.getYOffset()));
    currentWeapon.update(dt);
  }

  public void render(SpriteBatch spriteBatch) {
    currentAnimation.render(spriteBatch);
    currentWeapon.render(spriteBatch);
    
    if(isDead() && currentAnimation.getProgress() >= 1) {
    	// Reset death animation
    	currentAnimation.reset();
    	path = null;
    	target = null;
    	targetEnemy = null;
      position = spawnPoint.cpy();
      destination = position.cpy();
      currentAnimation = animations.get(AnimationKey.IDLE_SOUTH);
      hc = new HealthComponent(15);
      if (currentWeapon != null) {
      	currentWeapon.getAnimation().reset();
      	currentWeapon.setAnimation(AnimationKey.IDLE_SOUTH);
      	currentWeapon.startRendering();
      }
    }
  }
}
