package com.bitknight.bqex.managers;

import java.util.ArrayList;
import java.util.Collections;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.bitknight.bqex.entities.DrawableComparator;
import com.bitknight.bqex.entities.IDrawable;

/**
 * Wrapper to safely manage list of entities.
 * 
 * @author Jake Klassen
 *
 */
public class Entities {
	private static ArrayList<IDrawable> entities;
	// These two objects are needed for concurrency.
	// We can't just add and remove entities however we like while
	// the main game iterates over the list, so we'll use these 
	// lists to maintain groups to add and remove before updates.
	private static ArrayList<IDrawable> entitiesForAddition;
	private static ArrayList<IDrawable> entitiesForDeletion;

	public Entities() { }

	static {
		entities = new ArrayList<IDrawable>();
		entitiesForAddition = new ArrayList<IDrawable>();
		entitiesForDeletion = new ArrayList<IDrawable>();
	}

	public void dispose() {
		for(IDrawable id : entities)
			id.dispose();
		
		entities.clear();
		entitiesForAddition.clear();
		entitiesForDeletion.clear();
	}

	/**
	 * Get the underlying entities ArrayList.
	 * 
	 * @return
	 */
	public ArrayList<IDrawable> getEntities() {
		return entities;
	}

	public void add(IDrawable entity) {
		entitiesForAddition.add(entity);
	}

	public void remove(IDrawable entity) {
		entitiesForDeletion.add(entity);
	}

	private void sync() {
		entities.addAll(entitiesForAddition);
		entitiesForAddition.clear();
		entities.removeAll(entitiesForDeletion);
		entitiesForDeletion.clear();

		Collections.sort(Entities.entities, DrawableComparator.PositionComparator);
	}

	public void update(float dt) {
		sync();
		for(IDrawable id : entities)
			id.update(dt);
	}

	public void render(SpriteBatch sb) {
		for(IDrawable id : entities)
			id.render(sb);
	}
}