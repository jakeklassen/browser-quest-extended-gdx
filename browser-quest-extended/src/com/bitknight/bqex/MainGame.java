package com.bitknight.bqex;

import java.util.Stack;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.bitknight.bqex.screens.GameScreen;
import com.bitknight.bqex.screens.TitleScreen;
import com.bitknight.utils.GameState;
import com.bitknight.utils.IScreen;

/**
 * Main entry point for the game.
 * 
 * @author Jake Klassen
 *
 */
public class MainGame implements ApplicationListener {
	private GameScreen gameScreen;
	private TitleScreen titleScreen;
	// private TitleScreen titleScreen;
	private Stack<IScreen> screens;

  @Override
  public void create() {
    // Need to trigger static class early to bring it into existence.
  	Game.build();
  	// Block to load initial data
  	// TODO replace with load screen.
  	Game.assetManager.finishLoading();
  	
    screens = new Stack<IScreen>();
    gameScreen = new GameScreen();
    titleScreen = new TitleScreen();
    
    screens.push(gameScreen);
    
    Game.state = GameState.TITLE_SCREEN;
  }

  @Override
  public void dispose() {
    Game.dispose();
    for (IScreen screen : screens)
    	screen.dispose();
    
    screens.clear();
  }

  @Override
  public void render() {
  	float delta = Gdx.graphics.getDeltaTime();
  	
  	switch (Game.state) {
  	case PLAYING:
  	  Gdx.input.setCursorCatched(false);
  	  
  	  if (titleScreen != null)
  	  	titleScreen.pauseMusic();
  	  if (screens.contains(titleScreen))
  	  	screens.pop();
  	  
  	  if (GameScreen.isMusicPaused == false)
  	  	gameScreen.playMusic();
  	  else
  	  	gameScreen.pauseMusic();
  		Gdx.input.setInputProcessor(GameScreen.inputMultiplexer);
  		Game.getTweenManager().update(delta);
  		screens.peek().update(delta);
  		screens.peek().render();
  		break;
  	case PAUSED:
  		// Render in case of overlay, but don't update.
  		screens.peek().render();
  		break;
  	case TITLE_SCREEN:
  	  Gdx.input.setCursorCatched(false);
  	  Gdx.input.setInputProcessor(titleScreen.getStage());
  	  
  	  if (gameScreen !=null )
  	  	gameScreen.pauseMusic();
  	  
  		if (!screens.contains(titleScreen))
  			screens.push(titleScreen);
  		
  		if (TitleScreen.isSoundOn)
  			titleScreen.playMusic();
  		else
  			titleScreen.pauseMusic();
  		
  		screens.peek().render();
  		break;
  	case LOADING:
  	case HUD:
  		break;
  	}
  }

  @Override
  public void resize(int width, int height) {
  	for (IScreen screen : screens) {
  		screen.resize(width, height);
  	}
  }

  @Override
  public void pause() {
  	screens.peek().pause();
  }

  @Override
  public void resume() {
  	screens.peek().resume();
  }
}
