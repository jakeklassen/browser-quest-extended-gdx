package com.bitknight.bqex;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.bitknight.bqex.entities.Player;
import com.bitknight.bqex.managers.Entities;
import com.bitknight.bqex.utils.Message;
import com.bitknight.bqex.utils.MessageAccessor;
import com.bitknight.sound.SoundManager;
import com.bitknight.utils.GameState;
import com.bitknight.utils.TweenBuilder;

/**
 * Point of access class for convenience.
 * 
 * @author Jake Klassen
 *
 */
public class Game {
  public enum AppType {
    MOBILE, WEBGL, DESKTOP, APPLET, UNKNOWN
  }
  
  private static TweenManager tweenManager;
  private static TweenBuilder tweenBuilder;
  private static AppType appType;
  private static UI ui;
  
  public static Player player;
  public static Entities entities;
  public static GameState state;
  public static AssetManager assetManager;

  public static void build() {
    switch(Gdx.app.getType()) {
    case Android:
    case iOS:
      appType = AppType.MOBILE;
      break;
    case Applet:
      appType = AppType.APPLET;
      break;
    case Desktop:
      appType = AppType.DESKTOP;
      break;
    case WebGL:
      appType = AppType.WEBGL;
      break;
    default:
      appType = AppType.UNKNOWN;
      break;
    }
    
    ui = new UI();
    
    state = GameState.TITLE_SCREEN;
    entities = new Entities();

    tweenManager = new TweenManager();
    // Raise attribute limit to 4 to support (r,g,b,a) colors
    Tween.setCombinedAttributesLimit(4);
    // Register accessors
    Tween.registerAccessor(Message.class, new MessageAccessor());
    
    tweenBuilder = new TweenBuilder();

    assetManager = new AssetManager();
    loadAssets();
  }

  /**
   * Clean up
   */
  public static void dispose() {
    if (entities != null)
      entities.dispose();

    SoundManager.getInstance().dispose();
    assetManager.dispose();
    
    if (tweenBuilder != null)
      tweenBuilder.dispose();
  }

  public static UI getUI() {
  	return ui;
  }
  
  public static TweenManager getTweenManager() {
    return tweenManager;
  }
  
  public static TweenBuilder getTweenBuilder() {
    return tweenBuilder;
  }
  
  public static AppType getAppType() {
    return appType;
  }

  // Methods

  private static void loadAssets() {
    // Fonts
    assetManager.load("data/consolas.fnt", BitmapFont.class);
    assetManager.load("data/fonts/advocut-webfont-32.fnt", BitmapFont.class);
    assetManager.load("data/fonts/graphicpixel-webfont-32.fnt", BitmapFont.class);

    // Sounds
    assetManager.load("data/audio/sfx/hit1.ogg", Sound.class);
    assetManager.load("data/audio/sfx/hit2.ogg", Sound.class);
    assetManager.load("data/audio/sfx/hurt.ogg", Sound.class);
    assetManager.load("data/audio/sfx/death.ogg", Sound.class);
    assetManager.load("data/audio/sfx/kill1.ogg", Sound.class);
    assetManager.load("data/audio/sfx/kill2.ogg", Sound.class);
    assetManager.load("data/audio/sfx/death.ogg", Sound.class);
    assetManager.load("data/audio/sfx/heal.ogg", Sound.class);
    assetManager.load("data/audio/sfx/loot.ogg", Sound.class);

    // Music
    assetManager.load("data/audio/music/village.ogg", Music.class);
    assetManager.load("data/audio/music/forest.ogg", Music.class);
    
    // Textures
    assetManager.load("data/img/native/item-sword1.png", Texture.class);
    assetManager.load("data/img/native/item-darksword1.png", Texture.class);
    assetManager.load("data/img/native/item-clotharmor.png", Texture.class);
  }
}
