package com.bitknight.bqex.items;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.bitknight.bqex.entities.EntityType;
import com.bitknight.bqex.entities.IDrawable;
import com.bitknight.bqex.entities.ITarget;
import com.bitknight.graphics.Animation;

public class Loot implements IDrawable, ITarget {
	private Texture texture;
	private Vector2 position;
	private Vector2 scale;
	private Animation animation, sparks;
	private String fileName;
	private String itemName;
	private ILoot item;
	
	
	public Loot(String fileName, String itemName, Vector2 scale, Vector2 position,
			ILoot item) {
		this.itemName = itemName;
		this.fileName = fileName;
		this.scale = scale;
		this.position = position;
		this.item = item;
		
		initializeAnimation();
	}
	
	@Override
  public EntityType getType() {
		return EntityType.LOOT;
  }
	
	@Override
  public Rectangle getHitBox() {
		return new Rectangle(position.x, position.y, 1, 1);
  }
	
	public String getItemName() {
		return itemName;
	}
	
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	
	public String getAquiredItemFileName() {
		int idx = fileName.indexOf('-');
		return fileName.substring(idx + 1);
	}
	
	public ILoot getItem() {
		return item;
	}
	
	private void initializeAnimation() {
		texture = new Texture(Gdx.files.internal("data/img/native/" + fileName));
    Array<TextureRegion> regions = new Array<TextureRegion>();
    
    regions = new Array<TextureRegion>();
    regions.add(new TextureRegion(texture, 0, 0, 16, 16));
    regions.add(new TextureRegion(texture, 16, 0, 16, 16));
    regions.add(new TextureRegion(texture, 32, 0, 16, 16));
    regions.add(new TextureRegion(texture, 48, 0, 16, 16));
    regions.add(new TextureRegion(texture, 64, 0, 16, 16));
    regions.add(new TextureRegion(texture, 80, 0, 16, 16));
    animation = new Animation(1 / 6f, regions,
        new int[] { 0, 1, 2, 3, 4, 5 }, position, scale, true);
    
    // Sparks
    texture = new Texture(Gdx.files.internal("data/img/native/sparks.png"));
    regions = new Array<TextureRegion>();
    
    regions = new Array<TextureRegion>();
    regions.add(new TextureRegion(texture, 0, 0, 16, 16));
    regions.add(new TextureRegion(texture, 16, 0, 16, 16));
    regions.add(new TextureRegion(texture, 32, 0, 16, 16));
    regions.add(new TextureRegion(texture, 48, 0, 16, 16));
    regions.add(new TextureRegion(texture, 64, 0, 16, 16));
    sparks = new Animation(1 / 8f, regions,
        new int[] { 0, 1, 2, 3, 4 }, position, scale, true);
	}

	@Override
  public void dispose() {
	  texture.dispose();
  }

	@Override
  public Vector2 getPosition() {
	  return position;
  }
	
	public void setPosition(Vector2 position) {
		this.position = position;
	}

	@Override
  public void update(float dt) {
		sparks.update(dt);
		animation.update(dt);
  }

	@Override
  public void render(SpriteBatch sb) {
	  animation.render(sb);
	  sparks.render(sb);
  }
}
