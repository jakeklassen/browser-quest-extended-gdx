package com.bitknight.bqex.items;

/**
 * ILoot just looks better than IItem...
 * 
 * @author Jake Klassen
 *
 */
public interface ILoot {
	public ItemType getType();
	public String getTextureFileName();
}
