package com.bitknight.bqex.items;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.bitknight.bqex.utils.AnimationKey;
import com.bitknight.graphics.Animation;

/**
 * Contains commonly needed properties for a weapon.
 * 
 * @author Jake Klassen
 *
 */
public class Weapon implements ILoot {
	private Texture texture;
	private Animation animation;
	private int attack;
	private Vector2 position;
	private Vector2 scale;
	private boolean shouldRender = true;
	private float xOffset;
	private float yOffset;
	private String textureFileName;
	
	private Map<AnimationKey, Animation> animations;
	
	public Weapon(String textureFileName, int attack, Vector2 position) {
		animations = new HashMap<AnimationKey, Animation>();
		animation = null;
		scale = new Vector2(2, 2);
		this.texture = null;
		this.attack = attack;
		this.position = position;
		this.textureFileName = textureFileName;
		
		xOffset = 8f/16f;
		yOffset = 12f/16f;
		
		initializeAnimations(this.textureFileName);
	}
	
	public void dispose() {
		texture.dispose();
	}
	
	// Getters & Setters
	
	public Animation getAnimation() {
		return animation;
	}

	public void setAnimation(Animation animation) {
		this.animation = animation;
	}

	public int getAttack() {
		return attack;
	}

	public void setAttack(int attack) {
		this.attack = attack;
	}

	public Vector2 getPosition() {
		return position;
	}

	public void setPosition(Vector2 position) {
		this.position = position;
	}
	
	public void addAnimation(AnimationKey key, Animation animation) {
    animations.put(key, animation);
  }
	
	public void setAnimation(AnimationKey key) {
		if (animations.containsKey(key)) {
			animation = animations.get(key);
		}
	}
	
	public float getXOffset() {
		return xOffset;
	}
	
	public float getYOffset() {
		return yOffset;
	}
	
	public void stopRendering() {
		shouldRender = false;
	}
	
	public void startRendering() {
		shouldRender = true;
	}
	
	@Override
  public ItemType getType() {
	  return ItemType.WEAPON;
  }
	
	public String getTextureFileName() {
		return textureFileName;
	}
	
	public void initializeAnimations(String textureFileName) {
		texture = new Texture(Gdx.files.internal("data/img/native/" + textureFileName));
    Array<TextureRegion> regions = new Array<TextureRegion>();
    // 2d array size of weapon sheets
    final int xTiles = 5;
    final int yTiles = 9;
    final int tileWidth = texture.getWidth() / xTiles;
    final int tileHeight = texture.getHeight() / yTiles;
    
    /*
     * Typical weapon sheet
     * 
     * All West directions are gotten by flipping the East texture.
     * 
     * Idle
     *  - North
     *  - East
     *  - South
     *  
     * Attack
     *  - North
     *  - East
     *  - South
     *  
     * Static
     *  - North
     *  - East
     *  - South
     */
    
    
    // Idle South
    regions = new Array<TextureRegion>();
    regions.add(new TextureRegion(texture, 0, tileHeight * 8, tileWidth, tileHeight));
    regions.add(new TextureRegion(texture, tileWidth, tileHeight * 8, tileWidth, tileHeight));
    addAnimation(AnimationKey.IDLE_SOUTH, new Animation(1 / 2f, regions,
        new int[] { 0, 1 }, position, scale, true));

    // Idle North
    regions = new Array<TextureRegion>();
    regions.add(new TextureRegion(texture, 0, tileHeight * 5, tileWidth, tileHeight));
    regions.add(new TextureRegion(texture, tileWidth, tileHeight * 5, tileWidth, tileHeight));
    addAnimation(AnimationKey.IDLE_NORTH, new Animation(1 / 2f, regions,
        new int[] { 0, 1 }, position, scale, true));

    // Idle East
    regions = new Array<TextureRegion>();
    regions.add(new TextureRegion(texture, 0, tileHeight * 2, tileWidth, tileHeight));
    regions.add(new TextureRegion(texture, tileWidth, tileHeight * 2, tileWidth, tileHeight));
    addAnimation(AnimationKey.IDLE_EAST, new Animation(1 / 2f, regions,
        new int[] { 0, 1 }, position, scale, true));

    // Idle West
    regions = new Array<TextureRegion>();
    regions.add(new TextureRegion(texture, 0, tileHeight * 2, tileWidth, tileHeight));
    regions.get(0).flip(true, false);
    regions.add(new TextureRegion(texture, tileWidth, tileHeight * 2, tileWidth, tileHeight));
    regions.get(1).flip(true, false);
    addAnimation(AnimationKey.IDLE_WEST, new Animation(1 / 2f, regions,
        new int[] { 0, 1 }, position, scale, true));

    // East
    regions = new Array<TextureRegion>();
    regions.add(new TextureRegion(texture, 0, tileHeight, tileWidth, tileHeight));
    regions.add(new TextureRegion(texture, tileWidth, tileHeight, tileWidth, tileHeight));
    regions.add(new TextureRegion(texture, tileWidth * 2, tileHeight, tileWidth, tileHeight));
    regions.add(new TextureRegion(texture, tileWidth * 3, tileHeight, tileWidth, tileHeight));
    addAnimation(AnimationKey.EAST, new Animation(1 / 8f, regions,
        new int[] { 0, 1, 2, 3 }, position, scale, true));

    // West
    regions = new Array<TextureRegion>();
    regions.add(new TextureRegion(texture, 0, tileHeight, tileWidth, tileHeight));
    regions.get(0).flip(true, false);
    regions.add(new TextureRegion(texture, tileWidth, tileHeight, tileWidth, tileHeight));
    regions.get(1).flip(true, false);
    regions.add(new TextureRegion(texture, tileWidth * 2, tileHeight, tileWidth, tileHeight));
    regions.get(2).flip(true, false);
    regions.add(new TextureRegion(texture, tileWidth * 3, tileHeight, tileWidth, tileHeight));
    regions.get(3).flip(true, false);
    addAnimation(AnimationKey.WEST, new Animation(1 / 8f, regions,
        new int[] { 0, 1, 2, 3 }, position, scale, true));

    // South
    regions = new Array<TextureRegion>();
    regions.add(new TextureRegion(texture, 0, tileHeight * 7, tileWidth, tileHeight));
    regions.add(new TextureRegion(texture, tileWidth, tileHeight * 7, tileWidth, tileHeight));
    regions.add(new TextureRegion(texture, tileWidth * 2, tileHeight * 7, tileWidth, tileHeight));
    regions.add(new TextureRegion(texture, tileWidth * 3, tileHeight * 7, tileWidth, tileHeight));
    addAnimation(AnimationKey.SOUTH, new Animation(1 / 8f, regions,
        new int[] { 0, 1, 2, 3 }, position, scale, true));

    // North
    regions = new Array<TextureRegion>();
    regions.add(new TextureRegion(texture, 0, tileHeight * 4, tileWidth, tileHeight));
    regions.add(new TextureRegion(texture, tileWidth, tileHeight * 4, tileWidth, tileHeight));
    regions.add(new TextureRegion(texture, tileWidth * 2, tileHeight * 4, tileWidth, tileHeight));
    regions.add(new TextureRegion(texture, tileWidth * 3, tileHeight * 4, tileWidth, tileHeight));
    addAnimation(AnimationKey.NORTH, new Animation(1 / 8f, regions,
        new int[] { 0, 1, 2, 3 }, position, scale, true));
    
    // Attack North
    regions = new Array<TextureRegion>();
    regions.add(new TextureRegion(texture, 0, tileHeight * 3, tileWidth, tileHeight));
    regions.add(new TextureRegion(texture, tileWidth, tileHeight * 3, tileWidth, tileHeight));
    regions.add(new TextureRegion(texture, tileWidth * 2, tileHeight * 3, tileWidth, tileHeight));
    regions.add(new TextureRegion(texture, tileWidth * 3, tileHeight * 3, tileWidth, tileHeight));
    regions.add(new TextureRegion(texture, tileWidth * 4, tileHeight * 3, tileWidth, tileHeight));
    addAnimation(AnimationKey.ATTACK_NORTH, new Animation(1 / 8f, regions,
        new int[] { 0, 1, 2, 3, 4 }, position, scale, true));
    
    // Attack East
    regions = new Array<TextureRegion>();
    regions.add(new TextureRegion(texture, 0, 0, tileWidth, tileHeight));
    regions.add(new TextureRegion(texture, tileWidth, 0, tileWidth, tileHeight));
    regions.add(new TextureRegion(texture, tileWidth * 2, 0, tileWidth, tileHeight));
    regions.add(new TextureRegion(texture, tileWidth * 3, 0, tileWidth, tileHeight));
    regions.add(new TextureRegion(texture, tileWidth * 4, 0, tileWidth, tileHeight));
    addAnimation(AnimationKey.ATTACK_EAST, new Animation(1 / 8f, regions,
        new int[] { 0, 1, 2, 3, 4 }, position, scale, true));
    
    // Attack South
    regions = new Array<TextureRegion>();
    regions.add(new TextureRegion(texture, 0, 192, tileWidth, tileHeight));
    regions.add(new TextureRegion(texture, tileWidth, tileWidth * 6, tileWidth, tileHeight));
    regions.add(new TextureRegion(texture, tileWidth * 2, tileWidth * 6, tileWidth, tileHeight));
    regions.add(new TextureRegion(texture, tileWidth * 3, tileWidth * 6, tileWidth, tileHeight));
    regions.add(new TextureRegion(texture, tileWidth * 4, tileWidth * 6, tileWidth, tileHeight));
    addAnimation(AnimationKey.ATTACK_SOUTH, new Animation(1 / 8f, regions,
        new int[] { 0, 1, 2, 3, 4 }, position, scale, true));
    
    // Attack West
    regions = new Array<TextureRegion>();
    regions.add(new TextureRegion(texture, 0, 0, tileWidth, tileHeight));
    regions.add(new TextureRegion(texture, tileWidth, 0, tileWidth, tileHeight));
    regions.add(new TextureRegion(texture, tileWidth * 2, 0, tileWidth, tileHeight));
    regions.add(new TextureRegion(texture, tileWidth * 3, 0, tileWidth, tileHeight));
    regions.add(new TextureRegion(texture, tileWidth * 4, 0, tileWidth, tileHeight));
    regions.get(0).flip(true, false);
    regions.get(1).flip(true, false);
    regions.get(2).flip(true, false);
    regions.get(3).flip(true, false);
    regions.get(4).flip(true, false);
    addAnimation(AnimationKey.ATTACK_WEST, new Animation(1 / 8f, regions,
        new int[] { 0, 1, 2, 3, 4 }, position, scale, true));
	}

	public void update(float dt) {
		if (animation != null) {
			animation.setPos(position);
			animation.update(dt);
		}
	}
	
	public void render(SpriteBatch sb) {
		if (animation != null && shouldRender)
			animation.render(sb);
	}
}
