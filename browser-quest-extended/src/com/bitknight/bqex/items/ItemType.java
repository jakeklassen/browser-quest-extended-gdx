package com.bitknight.bqex.items;

/**
 * Used to differentiate items.
 * 
 * @author Jake Klassen
 *
 */
public enum ItemType {
	WEAPON(0), ARMOR(0);
	
	private final int value;
	
	private ItemType(int value) {
	  this.value = value;
  }
	
	public int getValue() {
		return value;
	}
}
