package com.bitknight.sound;

/**
 * Used to pass in data for audio preloading.
 * @author Jake Klassen
 *
 */
public class AudioResourceConfig {
	private String filename;
	private AudioResourceType type;
	private boolean looping;
	
	public AudioResourceConfig(String filename, AudioResourceType type,
			boolean looping) {
		this.filename = filename;
		this.type = type;
		this.looping = looping;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public AudioResourceType getType() {
		return type;
	}

	public void setType(AudioResourceType type) {
		this.type = type;
	}

	public boolean isLooping() {
		return looping;
	}

	public void setLooping(boolean looping) {
		this.looping = looping;
	}
	
	
}
