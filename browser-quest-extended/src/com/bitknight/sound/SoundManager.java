package com.bitknight.sound;

import java.util.ArrayList;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.bitknight.bqex.Game;
import com.bitknight.sound.AudioResourceType;

/**
 * Wrapper object to simplify playing music and sound effects.
 * 
 * @author John Klassen
 * 
 *
 */
public class SoundManager {
  // Private members
  private final String audioRootPath;
  private String musicPath;
  private String sfxPath;
  private boolean isPaused = false;
  private ArrayList<String> musicFiles = new ArrayList<String>();
  
  private static SoundManager instance = null;
  
  // C'tors
  private SoundManager() {
    this.audioRootPath = "data/audio/";
    this.musicPath = "music/";
    this.sfxPath = "sfx/";
  }
  
  private SoundManager(String audioPath, String musicPath, String sfxPath) {
    this.audioRootPath = audioPath;
    this.sfxPath = "sfx/";
    this.musicPath = musicPath;
  }
  
  // Methods
  /**
   * Create a default SoundManager
   * @return Return the SoundManager instance and/or create it first
   */
  public static SoundManager getInstance() {
    if( instance == null ) 
      instance = new SoundManager();
    
    return instance;
  }
  
  /**
   * Create a custom SoundManager instance
   * @param audioPath
   * @param musicPath
   * @param sfxPath
   * @return Return the custom SoundManager instance and/or create it first
   */
  public static SoundManager getInstance(String audioPath,
      String musicPath, String sfxPath) {
    if( instance == null ) {
      instance = new SoundManager( audioPath, musicPath, sfxPath );
    }
    
    return instance;
  }
  
  public boolean isPaused() {
    return isPaused;
  }
  
  /**
   * Play(): Handles the creation, storage and playback of sound effects and music
   * @param fileName with extension
   * @param isLooping
   * @param resourceType
   */
  public void play(String fileName, boolean isLooping, AudioResourceType resourceType) {
    if (isPaused)
      return;
    
    switch(resourceType) {
    case MUSIC: {
      if(!Game.assetManager.isLoaded(audioRootPath + musicPath + fileName)) {
        Game.assetManager.load(audioRootPath  + musicPath + fileName, Music.class);
        // Blocks
        Game.assetManager.finishLoading();
      }
      
      // File exists or was added, play it
      if(!Game.assetManager.get(audioRootPath + musicPath + fileName, Music.class).isPlaying()) {
      	Music m = Game.assetManager.get("data/audio/music/" + fileName, Music.class);
      	m.setLooping(isLooping);
      	m.play();
      	musicFiles.add(audioRootPath + musicPath + fileName);
      }
      break;
    }
    case SFX: {
      if( !Game.assetManager.isLoaded(audioRootPath + sfxPath + fileName) ) {
        Game.assetManager.load(audioRootPath + sfxPath + fileName, Sound.class);
        Game.assetManager.finishLoading();
      }
      
      // File exists or was added, play it
      if(isLooping)
      	Game.assetManager.get(audioRootPath + sfxPath + fileName, Sound.class).loop();
      else
      	Game.assetManager.get(audioRootPath + sfxPath + fileName, Sound.class).play();
      break;
    }
    }    
  }
  
  /**
   * Pause music resource. 
   * @param fileName
   * @param type
   */
  public void pauseMusic(String fileName) {
    if (!musicFiles.contains(audioRootPath + musicPath + fileName))
      return;
    
		Music m;
		if ((m = Game.assetManager.get(audioRootPath + musicPath + fileName, Music.class)) != null) {
			if (m.isPlaying() || m.isLooping())
				m.pause();
		}
  }
  
  public boolean isPlaying(String fileName) {
  	if (!musicFiles.contains(audioRootPath + musicPath + fileName))
  		return false;
  	
  	Music m = Game.assetManager.get(audioRootPath + musicPath + fileName, Music.class);
  	if (m != null)
  		if (m.isPlaying())
  			return true;
  		else
  			return false;
  	
  	return false;
  }
  
  public void pauseAllMusic() {
    isPaused = true;
    for (String fileName : musicFiles) {
      if (Game.assetManager.isLoaded(fileName)) {
        Music m = Game.assetManager.get(fileName, Music.class);
        m.pause();
      }
    }
  }
  
  public void unpauseAllMusic() {
    isPaused = false;
  }
  
  /**
   * Stop music resource.
   * @param fileName
   * @param type
   */
  public void stopMusic(String fileName) {
		Music m;
		if ((m = Game.assetManager.get(audioRootPath + musicPath + fileName, Music.class)) != null) {
			if (m.isPlaying())
				m.stop();
		}
  }
  
  public void dispose() {
  }
}
