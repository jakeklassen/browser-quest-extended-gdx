package com.bitknight.sound;

public enum AudioResourceType {
  MUSIC (0), SFX (1);
  
  private final int value;
  AudioResourceType(int value) {
    this.value = value;
  }
  public int getValue() {
    return value;
  }
}
