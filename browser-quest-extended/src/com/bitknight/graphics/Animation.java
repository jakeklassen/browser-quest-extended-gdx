package com.bitknight.graphics;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

/**
 * Animation utility class.
 * 
 * @author John Klassen
 *
 */
public class Animation {
  // Used to add up delta time until frameTimeLimit is reached.
  private float accumulator;
  // Image frames
  private Array<TextureRegion> texArray;
  // The order the texArray will be displayed in.
  private int[] frameDisplayOrder;
  private int currentIdx;
  // When frames should switch
  private float frameTimeLimit;
  private Vector2 pos;
  private Vector2 scale;
  private float rotation;
  private boolean repeat;
  private boolean stopRendering;

  /**
   * Animaton constructor.
   * 
   * @param frameTimeLimit In seconds
   * @param texArray Array of texture regions
   * @param frameDisplayOrder Order to draw textures
   * @param pos Position
   * @param scale Scale
   */
  public Animation(float frameTimeLimit, Array<TextureRegion> texArray, 
      int[] frameDisplayOrder, Vector2 pos, Vector2 scale, boolean repeat) {
    this.frameTimeLimit = frameTimeLimit;
    this.texArray = texArray;
    this.frameDisplayOrder = frameDisplayOrder;
    this.pos = pos;
    this.scale = scale;
    currentIdx = 0;
    this.repeat = repeat;
    stopRendering = false;
  }

  // Getters & Setters
  public Array<TextureRegion> getTexArray() {
    return texArray;
  }

  public void setTexArray(Array<TextureRegion> texArray) {
    this.texArray = texArray;
  }

  public int[] getFrameDisplayOrder() {
    return frameDisplayOrder;
  }

  public void setFrameDisplayOrder(int[] frameDisplayOrder) {
    this.frameDisplayOrder = frameDisplayOrder;
  }

  public int getCurrentIdx() {
    return currentIdx;
  }

  public void setCurrentIdx(int currentIdx) {
    this.currentIdx = currentIdx;
  }

  public float getFrameTimeLimit() {
    return frameTimeLimit;
  }

  public void setFrameTimeLimit(float frameTimeLimit) {
    this.frameTimeLimit = frameTimeLimit;
  }

  public Vector2 getPos() {
    return pos;
  }

  public void setPos(Vector2 pos) {
    this.pos = pos;
  }

  public Vector2 getScale() {
    return scale;
  }

  public void setScale(Vector2 scale) {
    this.scale = scale;
  }

  public float getRotation() {
    return rotation;
  }

  public void setRotation(float rotation) {
    this.rotation = rotation;
  }
  
  public void setRepeat(boolean repeat) {
    this.repeat = repeat;
  }
  
  public boolean getRepeat() {
    return repeat;
  }
  
  public void stop() {
    stopRendering = true;
  }
  
  public void reset() {
  	currentIdx = 0;
  	accumulator = 0f;
  	stopRendering = false;
  }
  
  /**
   * Used to return a float representing the progress through the
   * display frames.
   * 
   * @return float from 0 - 1
   */
  public float getProgress() {
    return (float)currentIdx / frameDisplayOrder.length;
  }

  // Methods
  public void update(float dt) {
    accumulator += dt;
    if( accumulator >= frameTimeLimit ) {
      accumulator = 0;
      if( ++currentIdx >= frameDisplayOrder.length ) {
        if(repeat)
          currentIdx = 0;
        stopRendering = true;
      }
    }
  }

  public void render(SpriteBatch sb) {
    // Don't render if we've played the animation once and repeat is false
    if(!repeat && stopRendering)
      return;
    
    sb.draw(texArray.get(frameDisplayOrder[currentIdx]), pos.x, pos.y, 0, 0, 1, 1, scale.x, scale.y, 0);
  }
}
