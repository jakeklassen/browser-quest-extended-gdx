package com.bitknight.utils;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector3;
import com.bitknight.bqex.Game;
import com.bitknight.bqex.entities.IDrawable;
import com.bitknight.bqex.screens.GameScreen;
import com.bitknight.sound.AudioResourceType;
import com.bitknight.sound.SoundManager;

/**
 * @author John Klassen
 * Notes on the Stat class:
 * Stat objects will keep track of their own growth and values, but a quick
 * explanation of what makes them grow is useful
 * 
 * Strength: Increases with damage dealt (50% bonus to melee damage)
 * Intelligence: Increases with magic damage dealt (50% bonus to magic damage)
 * Dexterity: Increases with ranged damage dealt (50% bonus to ranged damage)
 * Vitality: Increases with damage taken (50% bonus to defense)
 * HP: Increases by 1 whenever a stat gains a level
 */
public class Stat {
  // Members
  private int previousGoal = 0;
  private int currentGoal = 0;
  private int statProgress = 0;
  private float growthMultiplier = 0;
  private int value;
  private String shortName;
  private String fullName;
  private IDrawable owner;
  
  // C'tors
  public Stat() {
    this(0, 0, 0, 0, 0, "", "", null);
  }
  
  public Stat(float growthMultiplier) {
  	this(0, 0, 0, growthMultiplier, 0, "", "", null);
  }
  
  public Stat(int previousGoal, int currentGoal, int statProgress, float growthMultiplier, int value,
      String shortName, String fullName, IDrawable owner) {
    this.previousGoal = previousGoal;
    this.currentGoal = currentGoal;
    this.statProgress = statProgress;
    this.growthMultiplier = growthMultiplier;
    this.value = value;
    this.shortName = shortName;
    this.fullName = fullName;
    this.owner = owner;
  }
  
  // Getters and setters
  public int getPreviousGoal() {
    return previousGoal;
  }

  public void setPreviousGoal(int previousGoal) {
    this.previousGoal = previousGoal;
  }

  public int getCurrentGoal() {
    return currentGoal;
  }

  public void setCurrentGoal(int currentGoal) {
    this.currentGoal = currentGoal;
  }
  
  public int getStatProgress() {
    return this.statProgress;
  }
  
  public float getgrowthMultiplier() {
    return this.growthMultiplier;
  }
  
  public void setgrowthMultiplier(float gm) {
    this.growthMultiplier = gm;
  }
  
  public int getValue() {
    return this.value;
  }
  
  public void setValue(int value) {
    this.value = value;
  }
  
  public void setShortName(String shortName) {
    this.shortName = shortName;
  }
  
  public String getShortName() {
    return shortName;
  }
  
  public void setFullName(String fullName) {
    this.fullName = fullName;
  }
  
  public String getFullName() {
    return fullName;
  }
  
  public void setOwner(IDrawable owner) {
    this.owner = owner;
  }
  
  public IDrawable getOwner() {
    return owner;
  }
  
  //Methods
  public boolean updateProgress(int val) {
    this.statProgress += val;
    
    // Check if growth goal was reached, update goals and stats accordingly
    if(this.statProgress >= this.currentGoal) {
      return true;
    }
    
    return false;
  }
  
  public void levelUp(int val) {
    this.value += val;
    this.statProgress = 0;
    this.previousGoal = this.currentGoal;
    this.currentGoal = (int)(currentGoal * growthMultiplier);
    
    if (owner != null) {
      SoundManager.getInstance().play("heal.ogg", false, AudioResourceType.SFX);
      Vector3 posProjected = new Vector3(owner.getPosition().x + 0.25f, owner.getPosition().y + 2, 0);
      GameScreen.camera.project(posProjected);
      Game.getTweenBuilder().messageTween("++" + this.shortName, 
          posProjected.x, posProjected.y, posProjected.x, posProjected.y + 25, 
          1.25f, new Color(0, 0, 0, 1), new Color(0, 0, 0, 1), 1.25f);
    }
  }
}
