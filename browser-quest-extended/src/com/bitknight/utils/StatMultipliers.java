package com.bitknight.utils;

/**
 * @author John
 * Static multipliers for stat growth
 */
public final class StatMultipliers {
  public static final float STRENGTH_MULTIPLIER = 1.5f;
  public static final float INTELLIGENCE_MULTIPLIER = 1.5f;
  public static final float DEXTERITY_MULTIPLIER = 1.5f;
  public static final float VITALITY_MULTIPLIER = 1.5f;
}
