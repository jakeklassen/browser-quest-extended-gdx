package com.bitknight.utils;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Game;

/**
 * Methods a screen must implement.
 * 
 * @author Jake Klassen
 *
 */
public interface IScreen {
	/**
	 * Called when the screen should update itself.
	 * @param delta Time between calls.
	 */
	public void update(float delta);
	
	/** 
	 * Called when the screen should render itself. 
	 */
	public void render ();

	/** @see ApplicationListener#resize(int, int) */
	public void resize (int width, int height);

	/** Called when this screen becomes the current screen for a {@link Game}. */
	public void show ();

	/** Called when this screen is no longer the current screen for a {@link Game}. */
	public void hide ();

	/** @see ApplicationListener#pause() */
	public void pause ();

	/** @see ApplicationListener#resume() */
	public void resume ();

	/** Called when this screen should release all resources. */
	public void dispose ();
}
