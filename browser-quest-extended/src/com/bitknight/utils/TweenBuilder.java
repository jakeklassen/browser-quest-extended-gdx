package com.bitknight.utils;

import java.util.ArrayList;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenEquations;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Disposable;
import com.bitknight.bqex.Game;
import com.bitknight.bqex.utils.Message;
import com.bitknight.bqex.utils.MessageAccessor;

/**
 * Used to construct tweens.
 * 
 * @author Jake Klassen
 *
 */
public class TweenBuilder implements Disposable {
  private ArrayList<Message> messages;
  private ArrayList<Message> messagesToRemove;
  
  public TweenBuilder() {
    messages = new ArrayList<Message>();
    messagesToRemove = new ArrayList<Message>();
  }
  
  public void messageTween(String message, float startX, float startY, 
      float targetX, float targetY, float scaleXY, Color startColor, Color endColor,
      float duration) {
    //Game.getTweenManager().killTarget(msg);
    Message msg = new Message(Game.assetManager.get("data/fonts/graphicpixel-webfont-32.fnt", BitmapFont.class), 
        message, startX, startY, startColor, 1f);
    messages.add(msg);
    
    // Move message
    Tween.to(msg, MessageAccessor.POS_XY, duration)
        .target(targetX, targetY)
        .ease(TweenEquations.easeNone)
        .start(Game.getTweenManager());
    
    // Scale mesage
    Tween.to(msg, MessageAccessor.SCALE, duration)
         .target(scaleXY)
         .ease(TweenEquations.easeNone)
         .start(Game.getTweenManager());
    
    // Color message
    Tween.to(msg, MessageAccessor.COLOR, duration)
         .target(endColor.r, endColor.g, endColor.b, endColor.a)
         .ease(TweenEquations.easeNone)
         .start(Game.getTweenManager());
  }
  
  public void render(SpriteBatch spriteBatch) {
    if (!messagesToRemove.isEmpty()) {
      messages.removeAll(messagesToRemove);
      messagesToRemove.clear();
    }
    
    if (messages != null)
      for (Message m : messages) {
        if (Game.getTweenManager().containsTarget(m))
          m.render(spriteBatch);
        else
          messagesToRemove.add(m);
      }
  }

  @Override
  public void dispose() {
    for (Message m : messages)
      m.dispose();
    messages.clear();
  }
}
