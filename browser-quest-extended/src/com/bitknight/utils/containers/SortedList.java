package com.bitknight.utils.containers;

import java.util.ArrayList;
import java.util.Collections;

import com.bitknight.utils.pathfinding.Node;

/**
 * A simple sorted list
 *
 * @author Kevin Glass
 * @author Jake Klassen
 */
public class SortedList {
	/** The list of elements */
	private ArrayList<Node> list = new ArrayList<Node>();
	
	/**
	 * Retrieve the first element from the list
	 *  
	 * @return The first element from the list
	 */
	public Object first() {
		return list.get(0);
	}
	
	/**
	 * Is the list empty
	 * 
	 * @return
	 */
	public boolean isEmpty() {
		return list.isEmpty();
	}
	
	/**
	 * Empty the list
	 */
	public void clear() {
		list.clear();
	}
	
	/**
	 * Add an element to the list - causes sorting
	 * 
	 * @param o The element to add
	 */
	public void add(Object o) {
		list.add((Node)o);
		Collections.sort(list);
	}
	
	/**
	 * Sort the list
	 */
	public void sort() {
		Collections.sort(list);
	}
	
	/**
	 * Find object and update it. Resorts list afterwards.
	 * @param o Replacement object
	 */
	public void updateItem(Object o) {
		int idx = list.indexOf(o);
		if( idx == -1) // Not found
			return;
		
		list.set(idx, (Node)o);
		Collections.sort(list);
	}
	
	/**
	 * Return the first element and remove it from the list.
	 * @return
	 */
	public Node pop() {
		Node n = (Node)first();
		remove(n);
		return n;
	}
	
	/**
	 * Remove an element from the list
	 * 
	 * @param o The element to remove
	 */
	public void remove(Object o) {
		list.remove(o);
	}

	/**
	 * Get the number of elements in the list
	 * 
	 * @return The number of element in the list
		 */
	public int size() {
		return list.size();
	}
	
	/**
	 * Check if an element is in the list
	 * 
	 * @param o The element to search for
	 * @return True if the element is in the list
	 */
	public boolean contains(Object o) {
		return list.contains(o);
	}
}
