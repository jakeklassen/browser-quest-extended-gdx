package com.bitknight.utils;

/**
 * Used to represent game state.
 * @author Jake Klassen
 *
 */
public enum GameState {
	TITLE_SCREEN (0), PAUSED (1), PLAYING(2), HUD(3), LOADING(4);
  
  private final int value;
  GameState(int value) {
    this.value = value;
  }
  public int getValue() {
    return value;
  }
}
