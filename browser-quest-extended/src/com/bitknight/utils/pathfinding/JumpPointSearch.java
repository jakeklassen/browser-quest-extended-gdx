package com.bitknight.utils.pathfinding;

import java.util.ArrayList;

import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.bitknight.bqex.utils.maps.TileCollisionMap;
import com.bitknight.utils.containers.SortedList;

/**
 * Path finder using the Jump Point Search Algorithm
 * 
 * @author Jake Klassen
 * {@link https://github.com/qiao/PathFinding.js/blob/master/src/finders/JumpPointFinder.js}
 *
 */
public class JumpPointSearch implements IPathFinder {
	private SortedList open;
	// The map being searched
	private ITileBasedMap map;
	// Used with map
	Grid grid;
	
	// True if we allow diaganol movement
	private boolean allowDiagonalMovement;
	// The heuristic we're applying to determine which nodes to search first
	private IAStarHeuristic heuristic;
	
	private Node endNode;
	
	/**
	 * Create new path finder with default heuristic - closest
	 * 
	 * @param map
	 * @param allowDiagonalMovement
	 */
	public JumpPointSearch(TiledMapTileLayer layer, boolean allowDiagonalMovement) {
		this(layer, allowDiagonalMovement, new ClosestHeuristic());
	}
	
	/**
	 * Create new path finder with custom heuristic
	 * 
	 * @param map
	 * @param allowDiagonalMovement
	 * @param heuristic
	 */
	public JumpPointSearch(TiledMapTileLayer layer, boolean allowDiagonalMovement, 
			IAStarHeuristic heuristic) {
		this.map = new TileCollisionMap(layer);
		this.allowDiagonalMovement = allowDiagonalMovement;
		this.heuristic = heuristic;
		grid = new Grid(map.getWidthInTiles(), map.getHeightInTiles(), map);
	}
	
	@Override
	public Path findPath(IMover mover, int sx, int sy, int tx, int ty) {
		// Need to clear all reset all nodes.
		grid.resetGrid();
		open = new SortedList();
		Node startNode = grid.getNodeAt(sx, sy);
		endNode = grid.getNodeAt(tx, ty);
		Node node = null;
		
		// Set the `g` cost and `f` heuristic cost value of the start node to 0
		startNode.g = 0;
		startNode.f = 0;
		
		// Add to open list
		startNode.opened = true;
		open.add(startNode);
		
		// Work to do while open list has nodes
		while( !open.isEmpty() ) {
			// Get the node with the minimum `f` value
			node = (Node)open.pop();
			node.closed = true;
			
			if( node == endNode ) {
				// At this point we've definitely found a path so we can uses the parent
				// references of the nodes to find out way from the target location back
				// to the start recording the nodes on the way.
				Path path = new Path();
				while (endNode != startNode) {
					path.prependStep(endNode.x, endNode.y);
					endNode = endNode.getParent();
				}
				path.prependStep(sx,sy);
				
				return path;
			}
			
			identifySuccessors(node);
		}
		
		// Failed to find a path
		return null;
	}
	
	/**
	 * Indetify successors for the given node. Runs a jump point search in the direction
	 * of each available neighbour, adding any points found to the open list.
	 * 
	 * @param node Starting location
	 */
	private void identifySuccessors(Node node) {
		int endX = endNode.x, endY = endNode.y,
				x = node.x, y = node.y, jx, jy;
		float d, ng;
		int[] jumpPoint = null;
		Node neighbour = null, jumpNode = null;
		
		ArrayList<Node> neighbours = findNeighbours(node);
		for( int i = 0, length = neighbours.size(); i < length; ++i ) {
			neighbour = neighbours.get(i);
			jumpPoint = jump(neighbour.x, neighbour.y, x, y);
			
			if( jumpPoint != null ) {
				jx = jumpPoint[0];
				jy = jumpPoint[1];
				jumpNode = grid.getNodeAt(jx, jy);
				
				if( jumpNode.closed ) {
					continue;
				}
				
				// Include distance, as parent may not be adjacent
				//d = heuristic.getCost(map, null, x, y, jx, jy);
				d = Heuristic.euclidean(Math.abs(jx - x), Math.abs(jy - y));
				ng = node.g + d;
				
				if( !jumpNode.opened || ng < jumpNode.g ) {
					jumpNode.g = ng;
					if( !(jumpNode.h > 0) )
						jumpNode.h = Heuristic.manhattan(Math.abs(jx - endX), Math.abs(jy - endY));
					jumpNode.f = jumpNode.g + jumpNode.h;
					jumpNode.setParent(node);
					
					if( !jumpNode.opened ) {
						jumpNode.opened = true;
						open.add(jumpNode);
					}
					else {
						open.updateItem(jumpNode);
					}
				}
			}
		}
	}
	

	private int[] jump(int x, int y, int px, int py) {
		int dx = x - px, dy = y - py;
		int[] jx, jy;
		
		if( !grid.isWalkableAt(x, y) ) {
			return null;
		}
		
		if( grid.getNodeAt(x, y) == endNode )
			return new int[] {x, y};
		
		// Check for forced neighbours
		
		// Along diagonal
		if( dx != 0 && dy != 0 ) {
			if( (grid.isWalkableAt(x - dx, y + dy) && !grid.isWalkableAt(x - dx, y)) || 
					(grid.isWalkableAt(x + dx, y - dy) && !grid.isWalkableAt(x, y - dy)) ) {
				return new int[] {x, y};
			}
		}
		// Horizontally/Vertically
		else {
			if( dx != 0 ) { // Moving along x
				if( (grid.isWalkableAt(x + dx, y + 1) && !grid.isWalkableAt(x, y + 1)) ||
						(grid.isWalkableAt(x + dx, y - 1) && !grid.isWalkableAt(x, y - 1)) ) {
					return new int[] {x, y};
				}
			}
			else {
				if( (grid.isWalkableAt(x + 1, y + dy) && !grid.isWalkableAt(x + 1, y)) ||
						(grid.isWalkableAt(x - 1, y + dy) && !grid.isWalkableAt(x - 1, y)) ) {
					return new int[] {x, y};
				}
			}
		}
		
		// When moving diagonally, must check for horizontal/vertical jump points.
		if( dx != 0 && dy != 0 ) {
			jx = jump(x + dx, y, x, y);
			jy = jump(x, y + dy, x, y);
			if( jx != null || jy != null ) {
				return new int[] {x, y};
			}
		}
		
		// Moving diagonally, must make sure one of the vertical/horizontal neighbours
		// is open to allow the path.
		if( grid.isWalkableAt(x + dx, y) || grid.isWalkableAt(x, y + dy) ) {
			return jump(x + dx, y + dy, x, y);
		}
		else 
		{
			return null;
		}
	}
	
	private ArrayList<Node> findNeighbours(Node node) {
		Node parent = node.getParent();
		int x = node.x, y = node.y,
				px, py, dx, dy;
		ArrayList<Node> neighbours = new ArrayList<Node>();
		ArrayList<Node> neighbourNodes = new ArrayList<Node>();
		Node neighbourNode;
		
		// Directed pruning: can ignore most neighbours, unless forced.
		if( parent != null ) {
			px = parent.x;
			py = parent.y;
			// Get the normalized direction of travel
			dx = (x - px) / Math.max(Math.abs(x - px), 1);
			dy = (y - py) / Math.max(Math.abs(y - py), 1);
			
			// Search diagonally
			if( dx != 0 && dy != 0 ) {
				if( grid.isWalkableAt(x, y + dy) ) {
					neighbours.add(new Node(x, y + dy));
				}
			
				if( grid.isWalkableAt(x + dx, y) ) {
					neighbours.add(new Node(x + dx, y));
				}
				
				if( grid.isWalkableAt(x, y + dy) || grid.isWalkableAt(x + dx, y) ) {
					neighbours.add(new Node(x + dx, y + dy));
				}
				
				if ( !grid.isWalkableAt(x - dx, y) && grid.isWalkableAt(x, y + dy) ) {
	        neighbours.add(new Node(x - dx, y + dy));
		    }
				
		    if ( !grid.isWalkableAt(x, y - dy) && grid.isWalkableAt(x + dx, y) ) {
		        neighbours.add(new Node(x + dx, y - dy));
		    }
			}
			// Search horizontally/vertically
			else {
				if( dx == 0 ) {
					if( grid.isWalkableAt(x, y + dy) ) {
						if( grid.isWalkableAt(x, y + dy) ) {
							neighbours.add(new Node(x, y + dy));
						}
						
						if( !grid.isWalkableAt(x + 1, y) ) {
							neighbours.add(new Node(x + 1, y + dy));
						}
						
						if( !grid.isWalkableAt(x - 1, y) ) {
							neighbours.add(new Node(x - 1, y + dy));
						}
					}
				}
				else {
					if( grid.isWalkableAt(x + dx, y) ) {
						if( grid.isWalkableAt(x + dx, y) ) {
							neighbours.add(new Node(x + dx, y));
						}
						
						if( !grid.isWalkableAt(x, y + 1) ) {
							neighbours.add(new Node(x + dx, y + 1));
						}
						
						if( !grid.isWalkableAt(x, y - 1) ) {
							neighbours.add(new Node(x + dx, y - 1));
						}
					}
				}
			}
		}
		// Return all neighbours
		else {
			neighbourNodes = grid.getNeighbours(node, true, false);
			for( int i = 0, length = neighbourNodes.size(); i < length; ++i ) {
				neighbourNode = neighbourNodes.get(i);
				neighbours.add(new Node(neighbourNode.x, neighbourNode.y));
			}
		}
		
		return neighbours;
	}

	/**
	 * Helper class
	 * @author Jake Klassen
	 *
	 */
	private class Grid {
		private int width, height;
		private Node[][] nodes;
		
		public Grid(int width, int height, ITileBasedMap map) {
			this.width = width;
			this.height = height;
			this.nodes = buildNodes(width, height, map);
		}
		
		public Node[][] buildNodes(int width, int height, ITileBasedMap map) {
			nodes = new Node[map.getHeightInTiles()][map.getWidthInTiles()];
			for( int i = 0; i < height; ++i ) {
				for( int j = 0; j < width; ++j ) {
					nodes[i][j] = new Node(j, i);
				}
			}
			
			// Set walkable attribute based on the map cells
			for( int i = height - 1; i >= 0; --i ) {
				for( int j = 0; j < width; ++j ) {
					if( map.blocked(null, j, i) )
						nodes[i][j].walkable = false;
					else
						nodes[i][j].walkable = true;
				}
			}
			
			return nodes;
		}
		
		/**
		 * Reset grid Node values for new searches
		 */
		public void resetGrid() {
			for( int i = height - 1; i >= 0; --i ) {
				for( int j = 0; j < width; ++j ) {
					// Walkable?
					if( map.blocked(null, j, i) )
						nodes[i][j].walkable = false;
					else
						nodes[i][j].walkable = true;
					
					// Clear some fields
					nodes[i][j].h = 0;
					nodes[i][j].f = 0;
					nodes[i][j].g = 0;
					nodes[i][j].closed = false;
					nodes[i][j].opened = false;
					nodes[i][j].setParent(null);
				}
			}
		}
		
		public Node getNodeAt(int x, int y) {
			return nodes[y][x];
		}
		
		public boolean isWalkableAt(int x, int y) {
			return isInside(x, y) && nodes[y][x].walkable;
		}
		
		/**
		 * Bounds check with the 2d grid size.
		 * @param x
		 * @param y
		 * @return
		 */
		private boolean isInside(int x, int y) {
			return (x >= 0 && x < width) && (y >= 0 && y < height);
		}
		
		/**
		 * From https://github.com/qiao/PathFinding.js/blob/master/src/core/Grid.js
		 * 
		 * Get the neighbors of the given node.
		 *
		 *     offsets      diagonalOffsets:
		 *  +---+---+---+    +---+---+---+
		 *  |   | 0 |   |    | 0 |   | 1 |
		 *  +---+---+---+    +---+---+---+
		 *  | 3 |   | 1 |    |   |   |   |
		 *  +---+---+---+    +---+---+---+
		 *  |   | 2 |   |    | 3 |   | 2 |
		 *  +---+---+---+    +---+---+---+
		 * 
		 * @param node Node
		 * @param allowDiagonal
		 * @param dontCrossCorners
		 * @return Neighbouring nodes
		 */
		public ArrayList<Node> getNeighbours(Node node, boolean allowDiagonal, boolean dontCrossCorners) {
			int x = node.x, y = node.y;
			ArrayList<Node> neighbours = new ArrayList<Node>();
			// s - straight, d - diagonal
			boolean s0 = false, d0 = false,
							s1 = false, d1 = false,
							s2 = false, d2 = false,
							s3 = false, d3 = false;
			
			// North
			if( isWalkableAt(x, y - 1) ) {
				neighbours.add(nodes[y - 1][x]);
				s0 = true;
			}
			
			// East
			if( isWalkableAt(x + 1, y) ) {
				neighbours.add(nodes[y][x + 1]);
				s1 = true;
			}
			
			// South
			if( isWalkableAt(x, y + 1) ) {
				neighbours.add(nodes[y + 1][x]);
				s2 = true;
			}
			
			// West
			if( isWalkableAt(x - 1, y) ) {
				neighbours.add(nodes[y][x - 1]);
				s3 = true;
			}
			
			if( !allowDiagonal )
				return neighbours;
			
			if( dontCrossCorners ) {
	      d0 = s3 && s0;
	      d1 = s0 && s1;
	      d2 = s1 && s2;
	      d3 = s2 && s3;
		  } else {
	      d0 = s3 || s0;
	      d1 = s0 || s1;
	      d2 = s1 || s2;
	      d3 = s2 || s3;
		  }
			
			// North West
			if( d0 && isWalkableAt(x - 1, y - 1) ) {
				neighbours.add(nodes[y - 1][x - 1]);
			}
			
			// North East
			if( d1 && isWalkableAt(x + 1, y - 1) ) {
				neighbours.add(nodes[y - 1][x + 1]);
			}
			
			// South East
			if( d2 && isWalkableAt(x + 1, y + 1) ) {
				neighbours.add(nodes[y + 1][x + 1]);
			}
			
			// South West
			if( d3 && isWalkableAt(x - 1, y + 1) ) {
				neighbours.add(nodes[y + 1][x - 1]);
			}
				
			return neighbours;
		}
	}
	
}
