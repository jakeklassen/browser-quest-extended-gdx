package com.bitknight.utils.pathfinding;

/**
 * Static group heuristics
 * @author Jake Klassen
 *
 */
public class Heuristic {
	private Heuristic() {
		
	}
	
	/**
	 * Manhanttan distance
	 * 
	 * @param dx
	 * @param dy
	 * @return dx + dy
	 */
	public static float manhattan(float dx, float dy) {
		return dx + dy;
	}
	
	/**
	 * Manhattan distance
	 * @param x
	 * @param y
	 * @param tx target x
	 * @param ty target y
	 * @return difference x + difference y
	 */
	public static float manhattan(float x, float y, float tx, float ty) {
		return Math.abs(x - tx) + Math.abs(y - ty);
	}
	
	/**
	 * Euclidena distance
	 * 
	 * @param dx
	 * @param dy
	 * @return sqrt(dx * dx + dy * dy)
	 */
	public static float euclidean(float dx, float dy) {
		return (float)Math.sqrt(dx * dx + dy * dy);
	}
	
	/**
	 * Chebyshev distance
	 * 
	 * @param dx
	 * @param dy
	 * @return max(dx, dy)
	 */
	public static float chebyshev(float dx, float dy) {
		return Math.max(dx, dy);
	}
}
