package com.bitknight.utils.pathfinding;

/**
 * Represents a node on the map for pathfinding.
 * 
 * @author Kevin Glass, Jake Klassen
 *
 */
public class Node implements Comparable<Object> {
		//The x coordinate of the node
		public int x;
		// The y coordinate of the node
		public int y;
		// The path cost for this node
		public float cost;
		// The parent of this node, how we reached it in the search
		private Node parent;
		// The heuristic cost of this node
		public float heuristic;
		// The search depth of this node
		public int depth;
		// g(n) is the exact cost from this node to vertex n
		public float g; // g = from start
		// h(n) is the heuristic cost from this node to vertex n
		public float h; // h = to end
		// f(n) = g(n) + h(n)
		// The value we can sort by to get lowest cost points
		public float f; // f = g + h
		// Open status
		public boolean opened;
		// Closed status
		public boolean closed;
		
		public boolean walkable;
		
		/**
		 * Create a new node
		 * 
		 * @param x The x coordinate of the node
		 * @param y The y coordinate of the node
		 */
		public Node(int x, int y) {
			this.x = x;
			this.y = y;
			walkable = false;
			closed = false;
			opened = false;
		}
		
		/**
		 * Set the parent of this node
		 * 
		 * @param parent The parent node which lead us to this node
		 * @return The depth we have now reached in searching
		 */
		public int setParent(Node parent) {
			if( parent != null )
				depth = parent.depth + 1;
			
			this.parent = parent;
			
			return depth;
		}
		
		/**
		 * Parent of node
		 * 
		 * @return Parent node
		 */
		public Node getParent() {
			return parent;
		}
		
		/**
		 * @see Comparable#compareTo(Object)
		 */
		public int compareTo(Object other) {
			Node o = (Node) other;
			
			float f = heuristic + cost;
			float of = o.heuristic + o.cost;
			f = h + g;
			of = o.h + o.g;
			
			if (f < of) {
				return -1;
			} else if (f > of) {
				return 1;
			} else {
				return 0;
			}
		}

}
