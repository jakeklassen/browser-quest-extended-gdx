package com.bitknight.utils.pathfinding;

public class Manhattan implements IAStarHeuristic {

	/**
	 * @see IAStarHeuristic#getCost(ITileBasedMap, IMover, int, int, int, int)
	 */
  public float getCost(ITileBasedMap map, IMover mover, int x, int y, int tx,
      int ty) {
  	
  	return Math.abs(x - tx) + Math.abs(y - ty);
  }

}
