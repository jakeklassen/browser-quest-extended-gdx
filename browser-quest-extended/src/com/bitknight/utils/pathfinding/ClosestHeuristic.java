package com.bitknight.utils.pathfinding;

/**
 * A heuristic that uses the tile that is closest to the target
 * as the next best tile.
 * 
 * @author Kevin Glass
 * @author Jake Klassen
 */
public class ClosestHeuristic implements IAStarHeuristic {
	/**
	 * @see IAStarHeuristic#getCost(ITileBasedMap, IMover, int, int, int, int)
	 */
	public float getCost(ITileBasedMap map, IMover mover, int x, int y, int tx, int ty) {		
		float dx = tx - x;
		float dy = ty - y;

		return (float) Math.sqrt(dx*dx + dy*dy);
	}

}