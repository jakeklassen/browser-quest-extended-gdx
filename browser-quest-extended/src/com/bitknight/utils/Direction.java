package com.bitknight.utils;

/**
 * Just a helper class used to give all 45 degree angles within
 * 360 degrees.
 * @author Jake Klassen
 *
 */
public final class Direction {
	public static final int WEST = 0;
	public static final int SOUTH_WEST = 45;
	public static final int SOUTH = 90;
	public static final int SOUTH_EAST = 135;
	public static final int EAST = 180;
	public static final int NORTH_EAST = 225;
	public static final int NORTH = 270;
	public static final int NORTH_WEST = 315;
	
	// Don't need instances
	private Direction() {
		
	}
}
